google.setOnLoadCallback(function() {
    if (supportsSvg()) {
        var counter = 1
        $.each(chartData1, function(index, data) {
            drawCompareBarChart(data['difference'], counter, maxValue1, color1)
            counter = counter + 1
        })

        $.each(chartData2, function(index, data) {
            drawCompareBarChart(data['difference'], counter, maxValue2, color2)
            counter = counter + 1
        })
    }
});

function drawCompareBarChart(difference, counter, maxValue, color) 
{
  var data = new google.visualization.DataTable()
  data.addColumn('string', 'Název')
  data.addColumn('number', 'Rozdíl')
  data.addRows(1)
  data.setValue(0, 0, "")
  data.setCell(0, 1, parseFloat(difference), addSeparators(difference) + ' Kč')

  var options = {
    legend: {position:'none'},
    hAxis: {gridlines: {count: 0}, maxValue: maxValue, viewWindow: {min: 1}},
    series: {0:{color: color}},
    chartArea: {width: '100%', height: '100%', top: 0, left: 0},
    tooltip: {showColorCode: true}
  };

  var chart = new google.visualization.BarChart(document.getElementById('chart-div-'+counter))
  chart.draw(data, options)
}
$(document).ready(function(){
    $('.autocomplete').autocomplete({
      source: function(request, response){
        $.getJSON('/autocomplete', request, function(data){
            response(data)
        });
      },
      select: function(event, ui) {
        $(this).next().val(ui.item.id);
        $(this).next().change();
      }
    });
});

google.setOnLoadCallback(function(){
    if (supportsSvg()) {
        drawIncomeExpenseChart()
        drawIncomeExpenseForYearCharts()
        $(document).ready(function(){
            $('#income-expense-all-toggle').click(function(){
                $('#income-expense-all').toggle();
                return false;
            });
        });
    }
})

function drawIncomeExpenseChart() {
  if ($('#income-expense-all').length) {
    var data = new google.visualization.DataTable()
    var income;
    var expense;
    var saldo;
    data.addColumn('string', 'Rok')
    data.addColumn('number', 'Příjmy')
    data.addColumn('number', 'Výdaje')
    data.addColumn('number', 'Schodek/Přebytek')
    data.addRows(countProperties(incomeExpenseGroupedByYear))
    $.each(incomeExpenseGroupedByYear, function (index, value) {
      income = Math.round(parseFloat(value.income))
      expense = Math.round(parseFloat(value.expense))
      saldo = income - expense
      data.setValue(index, 0, value.year)
      data.setCell(index, 1, income, addSeparators(income) + " Kč")
      data.setCell(index, 2, expense, addSeparators(-1 * expense) + " Kč")
      data.setCell(index, 3, saldo, addSeparators(saldo) + " Kč")
    });

    var options = {
      width: 940, height: 500,
      legend: {position: 'right'},
      hAxis: {title: 'Rok'},
      vAxis: {title: 'Kč'},
      seriesType: "bars",
      series: {
        0: {color: '#00BA35'},
        1: {color: '#ff0000'},
        2: {type: "line", color: 'orange', pointSize: 15, lineWidth: 5}
      },
      backgroundColor: '#EEEEEF',
      focusTarget: 'category'
    };

    var chart = new google.visualization.ComboChart(document.getElementById('income-expense-all'))
    chart.draw(data, options);
  }
}

function drawIncomeExpenseForYearCharts() {
    drawChart('income',incomeSummary)
    drawChart('expense',expenseSummary)
}

function drawChart(type, group) {
  if ($("#chart_div-"+type).length) {
    var counter = 0
    var data = new google.visualization.DataTable()
    data.addColumn('string', 'Název')
    data.addColumn('number', 'Hodnota')
    data.addRows(countProperties(group))
    $.each(group, function (name, section) {
      if (section['value'] > 0) {
        data.setValue(counter, 0, name)
        data.setCell(counter, 1, parseFloat(section['value']), addSeparators(section['value']) + ' Kč')
        counter++
      }
    });

    var chart = new google.visualization.PieChart(document.getElementById('chart_div-' + type))
    chart.draw(data, {
      width: 456,
      height: 300,
      legend: {position: 'none'},
      titleTextStyle: {fontSize: 20},
      fontSize: 12,
      is3D: true,
      sliceVisibilityThreshold: 1 / 100000000,
      tooltip: {text: 'percentage'}
    })
  }
}
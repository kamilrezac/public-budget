google.setOnLoadCallback(function() {
    if (supportsSvg()) {
        drawColumnChart(quantiles)
        drawPieChart(municipalitiesNotIncludedCount, municipalitiesIncludedCount)
    }
});

function drawColumnChart(quantiles)
{
    var data = new google.visualization.DataTable()
    data.addColumn('string', 'Pásmo obcí')
    data.addColumn('number', 'Hodnota');
    data.addColumn({type: 'string', role: 'annotation'});
    data.addColumn({type: 'string', role: 'annotationText', p: {html:true}});
    data.addColumn({type: 'string', role: 'tooltip', p: {html:true}});
    
    var totalValue = Math.round(parseFloat(quantiles.totalValue)*100)/100
    var rows = new Array()
    $.each(quantiles.rows, function(index, row) {
      var quantileSum = Math.round(parseFloat(row.sum)*100)/100
      var quantileMin = Math.round(parseFloat(row.min)*100)/100
      var quantileMax = Math.round(parseFloat(row.max)*100)/100
      var percent = Math.round((quantileSum / totalValue)*10000)/100
      var row = new Array()
      row.push((index + 1) + '')
      row.push(percent)
      row.push(percent + ' %')
      row.push("<strong>Rozmezí hodnot</strong>: " + quantileMin + " Kč - " + quantileMax + " Kč<br /><strong>Podíl na celkové částce</strong>: " + addSeparators(percent) + " %")
      row.push("<strong>Rozmezí hodnot</strong>: " + quantileMin + " Kč - " + quantileMax + " Kč<br /><strong>Podíl na celkové částce</strong>: " + addSeparators(percent) + " %")
      rows.push(row)
    })
    data.addRows(rows)

    var options = {
      width: 456, height: 300,
      legend: {position: 'none'},
      hAxis: {title: 'Pásma obcí: Obce s částkou 1. největší, 2. velkou, 3. střední, 4. menší, 5. nejmenší'},
      vAxis: {title: '%: podíl na celkové částce'},
      tooltip: {isHtml: true},
      bar: { groupWidth: '99%' }
    }

    var chart = new google.visualization.ColumnChart(document.getElementById('chart-div-left'))
    chart.draw(data, options);
}

function drawPieChart(municipalitiesNotIncludedCount, municipalitiesIncludedCount) {
  var counter = 0
  var data = new google.visualization.DataTable()
  data.addColumn('string', 'Název')
  data.addColumn('number', 'Hodnota')
  data.addRows(2)
  data.setValue(0, 0, "s částkou")
  data.setCell(0, 1, municipalitiesIncludedCount)
  data.setValue(1, 0, "bez částky")
  data.setCell(1, 1, municipalitiesNotIncludedCount)

  var chart = new google.visualization.PieChart(document.getElementById('chart-div-right'))
  chart.draw(data, {width: 456, height: 300, legend: { position: 'bottom', alignment: 'start' }, titleTextStyle: {fontSize: 20}, fontSize: 12, is3D: true, sliceVisibilityThreshold: 1/100000000})
}
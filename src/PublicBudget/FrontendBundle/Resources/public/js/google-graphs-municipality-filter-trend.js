google.setOnLoadCallback(function() {
    if (supportsSvg()) {
        drawColumnChart(result)
    }
});

function drawColumnChart(result)
{
    var data = new google.visualization.DataTable()
    data.addColumn('string', 'Rok')
    if (countProperties(result) == 2) {
        data.addColumn('number', selectedMuninipalityName)
    }
    data.addColumn('number', 'Průměr obcí ve výběru')
    data.addRows(countProperties(result[0]))
    $.each(result, function(rowIndex, row) {
      $.each(row, function(index, value) {
        income = Math.round(parseFloat(value.value))
        data.setValue(index, 0, value.year)
        data.setCell(index, rowIndex + 1, income, addSeparators(income) + " Kč")
      })
    })
    
    var options = {
      width: 940, height: 500,
      legend: {position: 'right'},
      hAxis: {title: 'Rok'},
      vAxis: {title: 'Kč'},
      series: {0:{color: '#00BA35'}, 1:{color: '#ff0000'}},
      focusTarget: 'category'
    };
    
    var chart = new google.visualization.LineChart(document.getElementById('trend'))
    chart.draw(data, options);
}
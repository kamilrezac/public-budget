$(document).ready(function(){
    showSelectBoxesBy(budgetItemSelectBoxValue())
    ifUserSelectedOptionFromBudgetItemSelectBox(function(){
        showSelectBoxesBy(budgetItemSelectBoxValue())
    })

    $('#selectedMunicipalityAutocomplete').show()
    $("#filtr_obci_municipality").hide()

    if ($('#filtr_obci_municipality').val()) {
        $.get('/get-municipality-name-by-id', {municipality_id: $('#filtr_obci_municipality').val()}, function(data){
            $('#selectedMunicipalityAutocomplete').val(data)
        })
    }

    if ($('#filtr_obci_budget_item_income').val()) {
        $("#budgetItemIncomeSelect").val($('#filtr_obci_budget_item_income').val());
    }

    if ($('#filtr_obci_budget_item_expense').val()) {
        $("#budgetItemExpenseSelect").val($('#filtr_obci_budget_item_expense').val());
    }

    if ($('#filtr_obci_budget_item_purpose').val()) {
        $("#budgetItemPurposeSelect").val($('#filtr_obci_budget_item_purpose').val());
    }

    $("#budgetItemIncomeSelect").change(function(){
        $('#filtr_obci_budget_item_income').val($(this).val())
    })

    $("#budgetItemExpenseSelect").change(function(){
        $('#filtr_obci_budget_item_expense').val($(this).val())
    })

    $("#budgetItemPurposeSelect").change(function(){
        $('#filtr_obci_budget_item_purpose').val($(this).val())
    })

    $('#selectedMunicipalityAutocomplete').keyup(function(){
        if ($(this).val() == '') {
            $("#filtr_obci_municipality").val("")
        }
    })

    $('#selectedMunicipalityAutocomplete').autocomplete({
      source: function(request, response){
        $.getJSON('/autocomplete', request, function(data){
            response(data)
        });
      },
      select: function(event, ui) {
          $("#filtr_obci_municipality").val(ui.item.id)
      }
    });

    $('#expenseTypeSimpleSwitch').show();
    $('#expenseTypeSimpleSwitch a').click(function(e){
        e.preventDefault()
        var selectOptionText = $(this).text();
        if (selectOptionText == 'Vše') {
            clearSelectBox('filtr_obci_budget_item_expense');
            return;
        }
        $("#filtr_obci_budget_item_expense").find("option").filter(function(index) {
            return selectOptionText === $(this).text();
        }).prop("selected", "selected");
    })
})

function showSelectBoxesBy(value)
{
    hideSelectBoxes()
    if (value == 'Příjmy') {
        showSelectBox("filtr_obci_budget_item_income")
        clearSelectBox("filtr_obci_budget_item_purpose")
        clearSelectBox("filtr_obci_budget_item_expense")
    } else if (value == 'Výdaje') {
        showSelectBox("filtr_obci_budget_item_expense")
        showSelectBox("filtr_obci_budget_item_purpose")
        clearSelectBox("filtr_obci_budget_item_income")
    } else {
        clearSelectBoxes()
    }

    if (value != '- Vyberte položku -') {
        showSelectBox("filtr_obci_per_inhabitant")
    }
}

function hideSelectBoxes()
{
    hideSelectBox("filtr_obci_budget_item_income")
    hideSelectBox("filtr_obci_budget_item_expense")
    hideSelectBox("filtr_obci_budget_item_purpose")
    hideSelectBox("filtr_obci_per_inhabitant")
}

function clearSelectBoxes()
{
    clearSelectBox("filtr_obci_budget_item_income")
    clearSelectBox("filtr_obci_budget_item_expense")
    clearSelectBox("filtr_obci_budget_item_purpose")
}

function budgetItemSelectBoxValue()
{
    return $("#filtr_obci_budget_item").children("option").filter(":selected").text()
}

function hideSelectBox(id)
{
    $('#'+id).parent().parent().hide()
}

function showSelectBox(id)
{
    $('#'+id).parent().parent().show()
}

function ifUserSelectedOptionFromBudgetItemSelectBox(callback)
{
    $("#filtr_obci_budget_item").change(callback)
}

function clearSelectBox(id)
{
    $('#'+id).children("option").filter(":selected").removeAttr("selected");
    $('#'+id).children("option").filter(":selected")
    $('#'+id+' option[value=""]').attr("selected","selected");
}




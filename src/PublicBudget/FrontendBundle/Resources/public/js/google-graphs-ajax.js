$(document).ready(function() {
  if (supportsSvg()) {
     $('.google-graph-ajax').one('click', function() {
        var budgetItemData = $(this).data('graph-options')
        var budgetItemViewData = $(this).data('graph-header').split(';')
        var budgetItemUrl = $(this).data('graph-url')
        $(this).fancybox({
         'hideOnContentClick': true,
          onComplete: function () {
             drawAjaxChart(budgetItemData, budgetItemViewData, budgetItemUrl)
          }
        }).click()
        return false
     });
  }
})

function drawAjaxChart(budgetItemData, budgetItemViewData, budgetItemUrl) {
  $.getJSON('/ajax/google-graph-data', { data: budgetItemData }, function(result) {
    var count = result.length
    var header = '<h2>' +
      budgetItemViewData[0] + ' ' + result[0].year + ' - ' + result[count - 1].year +
    '</h2>' +
    '<div class="rel">' +
      budgetItemViewData[1] + ' - ' + budgetItemViewData[2] +
    '</div>' +
    '<div class="paragraf-polozka">' +
      budgetItemViewData[4] +
    '</div>'
    $('#google-graph-container .graph').html(header)
    var data = new google.visualization.DataTable()
    data.addColumn('string', 'Rok')
    data.addColumn('number', budgetItemViewData[0])
    data.addColumn('number', budgetItemViewData[0])
    data.addRows(count)
    $.each(result, function(index, row) {
      var value = Math.round(parseFloat(row.value)*100)/100
      data.setValue(index, 0, row.year.toString())
      if (row.year == budgetItemViewData[3]) {
        data.setCell(index, 2, value, addSeparators(value) + " Kč")
      } else {
        data.setCell(index, 1, value, addSeparators(value) + " Kč")
      }
    })

    var options = {
      width: 940, height: 500,
      legend: {position: 'none'},
      hAxis: {title: 'Rok'},
      vAxis: {title: 'Kč'},
      seriesType: "bars",
      series: {0:{color: '#3366cc'}, 1:{color: '#ff9900'}},
      backgroundColor: '#EEEEEF',
      focusTarget: 'category',
      isStacked: true
    }

    var chart = new google.visualization.ColumnChart(document.getElementById('google-graph'))
    chart.draw(data, options);

    // google.visualization.events.addListener(chart, 'select', function(e) {
      // year = result[chart.getSelection()[0].row].year
      // console.log(budgetItemUrl)
      // budgetItemUrl = budgetItemUrl.replace(/\/\d{4}/, '/'+year)
      // console.log(budgetItemUrl)

      // window.location.href = budgetItemUrl
      // self.location = budgetItemUrl
    // });
  });
}


console.log(mapData)

$(document).ready(function() {
    var myLatlng = new google.maps.LatLng(49.8037633,15.4749126);
    var mapOptions = {
        zoom: 7,
        center: myLatlng
    }
    var map = new google.maps.Map(document.getElementById("map"), mapOptions)
    
    var redCircle = {
        path: google.maps.SymbolPath.CIRCLE,
        fillColor: 'red',
        fillOpacity: .4,
        scale: 4.5,
        strokeColor: 'white',
        strokeWeight: 1
    }

    $.each(mapData, function(i, marker){
    	var marker = new google.maps.Marker({
            position: google.maps.LatLng(marker.latitude, marker.longitude),
            map: map,
            icon: circle
        })
    })
})
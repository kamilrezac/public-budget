function countProperties(obj) {
  var prop
  var propCount = 0

  for (prop in obj) {
    propCount++
  }
  return propCount
}

function addSeparators(nStr, inD, outD, sep)
{
    if(typeof(inD)==='undefined') inD = '.';
    if(typeof(outD)==='undefined') outD = ',';
    if(typeof(sep)==='undefined') sep = ' ';

  nStr += ''
  var dpos = nStr.indexOf(inD)
  var nStrEnd = ''
  if (dpos != -1) {
    nStrEnd = outD + nStr.substring(dpos + 1, nStr.length)
    nStr = nStr.substring(0, dpos)
  }
  var rgx = /(\d+)(\d{3})/
  while (rgx.test(nStr)) {
    nStr = nStr.replace(rgx, '$1' + sep + '$2')
  }
  return nStr + nStrEnd
}

function supportsSvg() {
    return (!!document.createElementNS && !!document.createElementNS('http://www.w3.org/2000/svg','svg').createSVGRect)
}
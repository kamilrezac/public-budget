_gas.setSocialActionsTrackedAlsoAsEvents(true)
_gas.push(['_setDomainName', '.rozpocetobce.cz']);
_gas.push(['_setSiteSpeedSampleRate', 5]);
_gas.create('UA-21384884-2', 'orig');

$(document).ready(function(){
  $('.write-to-your-representatives').click(function(){
    _gas.push(['_trackEvent', 'Napiste svym zastupitelum', $('h1').text()]);

    dataLayer.push({
	  'category': 'Napiste svym zastupitelum',
      'action': $('h1').text(),
      'event': 'trackEvent'
    });
  });
});
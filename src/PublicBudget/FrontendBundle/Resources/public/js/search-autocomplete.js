$(document).ready(function(){
    $('#search').autocomplete({
      source: function(request, response){
        $.getJSON('/autocomplete', request, function(data){
            response(data)
        });
      },
      select: function(event, ui){
        window.location = '/vyhledavani?id='+ui.item.id
      }
    });
});

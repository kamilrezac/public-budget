Feature: Municipality filter page
  In order to find municipality
  As an anonymous user
  I need to be able filter them by criteria

Background:
  Given I have a municipality "Brno" in kraj "Jihomoravský" and okres "Brno-město" and CSU code "666666" with population in years:
  | year | value  |
  | 2010 | 400000 |
  | 2011 | 395000 |
  And I have a municipality "Adamov" in kraj "Jihomoravský" and okres "Adamov" and CSU code "555555" with population in years:
  | year | value  |
  | 2010 | 20000 |
  | 2011 | 20500 |
  And I have a municipality "Karlovy Vary" in kraj "Karlovarský" and okres "Karlovy Vary" and CSU code "444444" with population in years:
  | year | value  |
  | 2010 | 100000 |
  | 2011 | 120000 |
  And I have a municipality "Ostrava" in kraj "Moravsko-slezský" and okres "Ostrava" and CSU code "333333" with population in years:
  | year | value  |
  | 2010 | 310000 |
  | 2011 | 315000 |
  And I have population ranges:
  |min|max|
  |10001|50000|
  |50001|100000|
  |100001|500000|
  |500001|200000000|
  And I have a sector classification budget structure:
  |category_code|category_name|section_code|section_name|subsection_code|subsection_name|paragraph_code|paragraph_name|
  |1|ZEMĚDĚLSTVÍ A LESNÍ HOSPODÁŘSTVÍ|10|Zemědělství a lesní hospodářství|103|Lesní hospodářství|1031|Pěstební činnost|
  |2|PRŮMYSLOVÁ A OSTATNÍ ODVĚTVÍ HOSPODÁŘSTVÍ|22|Doprava|221|Pozemní komunikace|2212|Silnice|
  |3|SLUŽBY PRO OBYVATELSTVO|31|Vzdělávání|311|Zařízení předškolní výchovy a základního vzdělávání|3111|Předškolní zařízení|
  |4|SOCIÁLNÍ VĚCI A POLITIKA ZAMĚSTNANOSTI|42|Politika zaměstnanosti|422|Aktivní politika zaměstnanosti|4221|Rekvalifikace|
  |5|BEZPEČNOST STÁTU A PRÁVNÍ OCHRANA|51|Obrana|511|Vojenská obrana|5111|Armáda|
  |6|VŠEOBECNÁ VEŘEJNÁ SPRÁVA A SLUŽBY|62|Jiné veřejné služby a činnosti|621|Ostatní veřejné služby|6211|Archivní činnost|
  |0|0 - nepoužívá se|0|0 - nepoužívá se|0|0 - nepoužívá se|0|Nerozlišeno|
  And I have a type classification budget structure:
  |class_code|class_name|group_code|group_name|subgroup_code|subgroup_name|item_code|item_name|
  |1|DAŇOVÉ PŘÍJMY|15|Majetkové daně|151|Daně z majetku|1511|Daň z nemovitostí|
  |2|NEDAŇOVÉ PŘÍJMY|21|Příjmy z vlastní činnosti a odvody přebytků organizací s přímým vztahem|211|Příjmy z vlastní činnosti|2114|Mýtné|
  |3|KAPITÁLOVÉ PŘÍJMY|32|Příjmy z prodeje dlouhodobého finančního majetku|320|Příjmy z prodeje dlouhodobého finančního majetku  |3201|Příjmy z prodeje akcií|
  |4|PŘIJATÉ DOTACE|41|Neinvestiční přijaté dotace|413|Převody z vlastních fondů|4132|Převody z ostatních vlastních fondů|
  |5|BĚŽNÉ VÝDAJE|54|Neinvestiční transfery obyvatelstvu|541|Sociální dávky|5410|Sociální dávky|
  |6|KAPITÁLOVÉ VÝDAJE|61|Investiční nákupy a související výdaje|613|Pozemky|6130|Pozemky|
  And I have a type classification consolidation budget structure:
  |id|class_code|class_name|group_code|group_name|subgroup_code|subgroup_name|item_code|item_name|
  |176|4|PŘIJATÉ DOTACE|41|Neinvestiční přijaté dotace|413|Převody z vlastních fondů|4134|Převody z rozpočtových účtů|
  |192|4|PŘIJATÉ DOTACE|42|nvestiční přijaté transfery|422|Investiční přijaté transfery od veřejných rozpočtů územní úrovně|4221|Investiční přijaté transfery od obcí|
  |314|5|BĚŽNÉ VÝDAJE|53|Neinvestiční transfery veřejnoprávním subjektům a mezi peněžními fondy téhož subjektu|534|Převody vlastním fondům|5349|Ostatní převody vlastním fondům|
  |411|6|KAPITÁLOVÉ VÝDAJE|63|Investiční transfery|634|Investiční transfery veřejným rozpočtům územní úrovně|6341|Investiční dotace obcím|
  And I have budget item types:
  |name|
  |Rozpočtové příjmy|
  |Rozpočtové výdaje|
  |Pevné části      |
  And I have consolidation budget items for municipality "Brno" and year "2011":
  |sector_classification|type_classification|budget_item_type|value|
  |Nerozlišeno|Převody z rozpočtových účtů|Rozpočtové příjmy|3200000|
  |Nerozlišeno|Investiční přijaté transfery od obcí|Rozpočtové příjmy|3200000|
  |Nerozlišeno|Ostatní převody vlastním fondům|Rozpočtové výdaje|3200000|
  |Nerozlišeno|Investiční dotace obcím|Rozpočtové výdaje|3200000|
  And I have consolidation budget items for municipality "Adamov" and year "2011":
  |sector_classification|type_classification|budget_item_type|value|
  |Nerozlišeno|Převody z rozpočtových účtů|Rozpočtové příjmy|3200000|
  |Nerozlišeno|Ostatní převody vlastním fondům|Rozpočtové výdaje|3200000|
  And I have budget items for municipality "Brno" and year "2011":
  |sector_classification|type_classification|budget_item_type|value|
  |Nerozlišeno|Daň z nemovitostí|Rozpočtové příjmy|1000000|
  |Nerozlišeno|Mýtné|Rozpočtové příjmy|1500000|
  |Nerozlišeno|Příjmy z prodeje akcií|Rozpočtové příjmy|2000000|
  |Nerozlišeno|Převody z ostatních vlastních fondů|Rozpočtové příjmy|2500000|
  |Pěstební činnost|Pozemky|Rozpočtové výdaje|3000000|
  |Silnice|Pozemky|Rozpočtové výdaje|3200000|
  |Předškolní zařízení|Pozemky|Rozpočtové výdaje|3200000|
  |Rekvalifikace|Sociální dávky|Rozpočtové výdaje|3200000|
  |Armáda|Sociální dávky|Rozpočtové výdaje|3200000|
  |Archivní činnost|Sociální dávky|Rozpočtové výdaje|3200000|
  And I have budget items for municipality "Brno" and year "2010":
  |sector_classification|type_classification|budget_item_type|value|
  |Nerozlišeno|Daň z nemovitostí|Rozpočtové příjmy|1000000|
  |Nerozlišeno|Mýtné|Rozpočtové příjmy|1500000|
  |Nerozlišeno|Příjmy z prodeje akcií|Rozpočtové příjmy|2000000|
  |Nerozlišeno|Převody z ostatních vlastních fondů|Rozpočtové příjmy|2500000|
  |Pěstební činnost|Pozemky|Rozpočtové výdaje|3000000|
  |Silnice|Pozemky|Rozpočtové výdaje|3200000|
  |Předškolní zařízení|Pozemky|Rozpočtové výdaje|3200000|
  |Rekvalifikace|Sociální dávky|Rozpočtové výdaje|3200000|
  |Armáda|Sociální dávky|Rozpočtové výdaje|3200000|
  |Archivní činnost|Sociální dávky|Rozpočtové výdaje|3200000|
  And I have budget items for municipality "Adamov" and year "2011":
  |sector_classification|type_classification|budget_item_type|value|
  |Nerozlišeno|Daň z nemovitostí|Rozpočtové příjmy|100000|
  |Nerozlišeno|Mýtné|Rozpočtové příjmy|150000|
  |Nerozlišeno|Příjmy z prodeje akcií|Rozpočtové příjmy|200000|
  |Nerozlišeno|Převody z ostatních vlastních fondů|Rozpočtové příjmy|250000|
  |Pěstební činnost|Pozemky|Rozpočtové výdaje|300000|
  |Silnice|Pozemky|Rozpočtové výdaje|320000|
  |Předškolní zařízení|Pozemky|Rozpočtové výdaje|320000|
  |Rekvalifikace|Sociální dávky|Rozpočtové výdaje|320000|
  |Armáda|Sociální dávky|Rozpočtové výdaje|320000|
  |Archivní činnost|Sociální dávky|Rozpočtové výdaje|320000|
  And I have budget items for municipality "Ostrava" and year "2011":
  |sector_classification|type_classification|budget_item_type|value|
  |Nerozlišeno|Daň z nemovitostí|Rozpočtové příjmy|800000|
  |Nerozlišeno|Mýtné|Rozpočtové příjmy|1000000|
  |Nerozlišeno|Příjmy z prodeje akcií|Rozpočtové příjmy|1000000|
  |Nerozlišeno|Převody z ostatních vlastních fondů|Rozpočtové příjmy|1500000|
  |Pěstební činnost|Pozemky|Rozpočtové výdaje|2000000|
  |Silnice|Pozemky|Rozpočtové výdaje|1500000|
  |Předškolní zařízení|Pozemky|Rozpočtové výdaje|1500000|
  |Rekvalifikace|Sociální dávky|Rozpočtové výdaje|1500000|
  |Armáda|Sociální dávky|Rozpočtové výdaje|1500000|
  |Archivní činnost|Sociální dávky|Rozpočtové výdaje|1500000|
  And I have budget items for municipality "Karlovy Vary" and year "2011":
  |sector_classification|type_classification|budget_item_type|value|
  |Nerozlišeno|Daň z nemovitostí|Rozpočtové příjmy|600000|
  |Nerozlišeno|Mýtné|Rozpočtové příjmy|8000000|
  |Nerozlišeno|Příjmy z prodeje akcií|Rozpočtové příjmy|1000000|
  |Nerozlišeno|Převody z ostatních vlastních fondů|Rozpočtové příjmy|1500000|
  |Pěstební činnost|Pozemky|Rozpočtové výdaje|2000000|
  |Silnice|Pozemky|Rozpočtové výdaje|1000000|
  |Předškolní zařízení|Pozemky|Rozpočtové výdaje|1000000|
  |Rekvalifikace|Sociální dávky|Rozpočtové výdaje|1000000|
  |Armáda|Sociální dávky|Rozpočtové výdaje|1500000|
  |Archivní činnost|Sociální dávky|Rozpočtové výdaje|1000000|
  And I have budget item summary configurations:
  |name|type|value|parent|code|
  |Příjmy|type_classification|1,2,3,4,7,8||1,2,3,4|
  |Daňové|type_classification|1|Příjmy|1|
  |Výdaje|type_classification|5,6,9,10||5,6|
  |Investiční|type_classification|6|Výdaje|6|
  |Přebytek/Schodek|summary_expression|1-3||1-6|
  |Služby|sector_classification|3||3|
  |Vzdělávání|sector_classification|3||31,32|
  And I am on "/filtr-obci"

Scenario: Search municipality by region
  When I select "Jihomoravský" from "Region"
  And I select "2011" from "Rok"
  And I press "Filtrovat obce"
  Then I should see "Obce ČR podle regionu Jihomoravský v roce 2011"
  And I should see "Brno"
  And I should see "Adamov"
  And I should not see "Karlovy Vary"
  And I should not see "Ostrava"

Scenario: Search municipality by size
  When I select "2011" from "Rok"
  And I select "100001 - 500000" from "Velikost obce"
  And I press "Filtrovat obce"
  Then I should see "Obce ČR podle velikosti 100001 - 500000 v roce 2011"
  And I should see "Brno"
  And I should see "Karlovy Vary"
  And I should see "Ostrava"
  And I should not see "Adamov"

Scenario: Search municipality by size and region
  When I select "Jihomoravský" from "Region"
  And I select "2011" from "Rok"
  And I select "10001 - 50000" from "Velikost obce"
  And I press "Filtrovat obce"
  Then I should see "Obce ČR podle regionu Jihomoravský, velikosti 10001 - 50000 v roce 2011"
  And I should not see "Brno"
  And I should not see "Karlovy Vary"
  And I should not see "Ostrava"
  And I should see "Adamov"

Scenario: Search municipality by income type classification summary
  When I select "Příjmy" from "Seřadit dle"
  And I select "2011" from "Rok"
  And I press "Filtrovat obce"
  # And show last response
  Then I should see "Obce ČR podle rozpočtu Příjmy v roce 2011"
  And I should see "Brno"
  And I should see value "7 000 000 Kč"

Scenario: Search municipality by income type classification summary detail
  When I select "Příjmy" from "Seřadit dle"
  And I select "2011" from "Rok"
  And I select "Daňové" from "Druh příjmů"
  And I press "Filtrovat obce"
  Then I should see "Obce ČR podle rozpočtu Příjmy - daňové v roce 2011"
  And I should see "Brno"
  And I should see value "1 000 000 Kč"

Scenario: Search municipality by expense type classification summary
  When I select "Výdaje" from "Seřadit dle"
  And I select "2011" from "Rok"
  And I press "Filtrovat obce"
  Then I should see "Obce ČR podle rozpočtu Výdaje v roce 2011"
  And I should see "Brno"
  And I should see value "19 000 000 Kč"

Scenario: Search municipality by expense type classification summary detail
  When I select "Výdaje" from "Seřadit dle"
  And I select "2011" from "Rok"
  And I select "Investiční" from "Druh výdajů"
  And I press "Filtrovat obce"
  Then I should see "Obce ČR podle rozpočtu Výdaje - investiční v roce 2011"
  And I should see "Brno"
  And I should see value "9 400 000 Kč"

Scenario: Search municipality by budget summary in ascended order
  When I select "Výdaje" from "Seřadit dle"
  And I select "2011" from "Rok"
  And I select radio button "vzestupně"
  And I press "Filtrovat obce"
  Then I should see "Obce ČR podle rozpočtu Výdaje v roce 2011"
  And I should see municipalities in order "Adamov,Karlovy Vary,Ostrava,Brno" in element "table.filtrovaneObce tr td a"

Scenario: Search municipality by budget summary in descended order
  When I select "Výdaje" from "Seřadit dle"
  And I select "2011" from "Rok"
  And I select radio button "sestupně"
  And I press "Filtrovat obce"
  Then I should see "Obce ČR podle rozpočtu Výdaje v roce 2011"
  And I should see municipalities in order "Brno,Ostrava,Karlovy Vary,Adamov" in element "table.filtrovaneObce tr td a"

Scenario: Search municipality by sector classification summary
  When I select "Výdaje" from "Seřadit dle"
  And I select "2011" from "Rok"
  And I select "Služby" from "Účel"
  And I press "Filtrovat obce"
  Then I should see "Obce ČR podle rozpočtu Výdaje na služby v roce 2011"
  And I should see "Brno"
  And I should see value "3 200 000 Kč"

Scenario: Search municipality by sector classification summary public services expection
  When I select "Výdaje" from "Seřadit dle"
  And I select "2011" from "Rok"
  And I select "Vzdělávání" from "Účel"
  And I press "Filtrovat obce"
  Then I should see "Obce ČR podle rozpočtu Výdaje na vzdělávání v roce 2011"
  And I should see "Brno"
  And I should see value "3 200 000 Kč"

Scenario: Search municipality by budget summary per inhabitant
  When I select "Výdaje" from "Seřadit dle"
  And I select "2011" from "Rok"
  And I select radio button "obyvatele"
  And I press "Filtrovat obce"
  Then I should see "Obce ČR podle rozpočtu Výdaje přepočteno na obyvatele v roce 2011"
  And I should see "Brno"
  And I should see value "48,10 Kč"

Scenario: Search municipality by budget summary expression
  When I select "Přebytek/Schodek" from "Seřadit dle"
  And I select "2011" from "Rok"
  And I press "Filtrovat obce"
  Then I should see "Obce ČR podle rozpočtu Přebytek/Schodek v roce 2011"
  And I should see "Brno"
  And I should see value "-12 000 000 Kč"

Scenario: Search municipality by budget summary expression per inhabitant
  When I select "Přebytek/Schodek" from "Seřadit dle"
  And I select radio button "obyvatele"
  And I select "2011" from "Rok"
  And I press "Filtrovat obce"
  Then I should see "Obce ČR podle rozpočtu Přebytek/Schodek přepočteno na obyvatele v roce 2011"
  And I should see "Brno"
  And I should see value "-30,38 Kč"

Scenario: Test query is saved in session on new page load
  When I select "Výdaje" from "Seřadit dle"
  And I select "2011" from "Rok"
  And I press "Filtrovat obce"
  And I am on "/filtr-obci"
  Then I should see "Obce ČR podle rozpočtu Výdaje v roce 2011"
  And I should see "Brno"
  And I should see value "19 000 000 Kč"

Scenario: Get filtered results in CSV
  When I select "Přebytek/Schodek" from "Seřadit dle"
  And I select radio button "obyvatele"
  And I select "2011" from "Rok"
  And I press "Filtrovat obce"
  And I follow "Stáhnout export v CSV"
  Then response CSV should be:
  """
"Obce ČR podle rozpočtu Přebytek/Schodek přepočteno na obyvatele v roce 2011"
"obec";"okres";"kraj";"hodnota"
"Adamov";"Adamov";"Jihomoravský";"-58,54"
"Brno";"Brno-město";"Jihomoravský";"-30,38"
"Ostrava";"Ostrava";"Moravsko-slezský";"-16,51"
"Karlovy Vary";"Karlovy Vary";"Karlovarský";"30,00"
  """









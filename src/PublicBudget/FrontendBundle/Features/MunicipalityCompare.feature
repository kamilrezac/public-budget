Feature: Municipality compare page
  In order to compare two municipalities
  As an anonymous user
  I need to select them and compare them

Background:
  Given I have a municipality "Brno" in kraj "Jihomoravský" and okres "Brno-město" and CSU code "666666" with population in years:
  | year | value  |
  | 2010 | 400000 |
  | 2011 | 395000 |
  And I have a municipality "Adamov" in kraj "Jihomoravský" and okres "Adamov" and CSU code "555555" with population in years:
  | year | value  |
  | 2010 | 20000  |
  | 2011 | 20500  |
  And I have a sector classification budget structure:
  |category_code|category_name|section_code|section_name|subsection_code|subsection_name|paragraph_code|paragraph_name|
  |1|ZEMĚDĚLSTVÍ A LESNÍ HOSPODÁŘSTVÍ|10|Zemědělství a lesní hospodářství|103|Lesní hospodářství|1031|Pěstební činnost|
  |2|PRŮMYSLOVÁ A OSTATNÍ ODVĚTVÍ HOSPODÁŘSTVÍ|22|Doprava|221|Pozemní komunikace|2212|Silnice|
  |3|SLUŽBY PRO OBYVATELSTVO|31|Vzdělávání|311|Zařízení předškolní výchovy a základního vzdělávání|3111|Předškolní zařízení|
  |4|SOCIÁLNÍ VĚCI A POLITIKA ZAMĚSTNANOSTI|42|Politika zaměstnanosti|422|Aktivní politika zaměstnanosti|4221|Rekvalifikace|
  |5|BEZPEČNOST STÁTU A PRÁVNÍ OCHRANA|51|Obrana|511|Vojenská obrana|5111|Armáda|
  |6|VŠEOBECNÁ VEŘEJNÁ SPRÁVA A SLUŽBY|62|Jiné veřejné služby a činnosti|621|Ostatní veřejné služby|6211|Archivní činnost|
  |0|0 - nepoužívá se|0|0 - nepoužívá se|0|0 - nepoužívá se|0|Nerozlišeno|
  And I have a type classification budget structure:
  |class_code|class_name|group_code|group_name|subgroup_code|subgroup_name|item_code|item_name|
  |1|DAŇOVÉ PŘÍJMY|15|Majetkové daně|151|Daně z majetku|1511|Daň z nemovitostí|
  |2|NEDAŇOVÉ PŘÍJMY|21|Příjmy z vlastní činnosti a odvody přebytků organizací s přímým vztahem|211|Příjmy z vlastní činnosti|2114|Mýtné|
  |3|KAPITÁLOVÉ PŘÍJMY|32|Příjmy z prodeje dlouhodobého finančního majetku|320|Příjmy z prodeje dlouhodobého finančního majetku  |3201|Příjmy z prodeje akcií|
  |4|PŘIJATÉ DOTACE|41|Neinvestiční přijaté dotace|413|Převody z vlastních fondů|4132|Převody z ostatních vlastních fondů|
  |5|BĚŽNÉ VÝDAJE|54|Neinvestiční transfery obyvatelstvu|541|Sociální dávky|5410|Sociální dávky|
  |6|KAPITÁLOVÉ VÝDAJE|61|Investiční nákupy a související výdaje|613|Pozemky|6130|Pozemky|
  And I have a type classification consolidation budget structure:
  |id|class_code|class_name|group_code|group_name|subgroup_code|subgroup_name|item_code|item_name|
  |176|4|PŘIJATÉ DOTACE|41|Neinvestiční přijaté dotace|413|Převody z vlastních fondů|4134|Převody z rozpočtových účtů|
  |192|4|PŘIJATÉ DOTACE|42|nvestiční přijaté transfery|422|Investiční přijaté transfery od veřejných rozpočtů územní úrovně|4221|Investiční přijaté transfery od obcí|
  |314|5|BĚŽNÉ VÝDAJE|53|Neinvestiční transfery veřejnoprávním subjektům a mezi peněžními fondy téhož subjektu|534|Převody vlastním fondům|5349|Ostatní převody vlastním fondům|
  |411|6|KAPITÁLOVÉ VÝDAJE|63|Investiční transfery|634|Investiční transfery veřejným rozpočtům územní úrovně|6341|Investiční dotace obcím|
  And I have budget item types:
  |name|
  |Rozpočtové příjmy|
  |Rozpočtové výdaje|
  |Pevné části      |
  And I have consolidation budget items for municipality "Brno" and year "2011":
  |sector_classification|type_classification|budget_item_type|value|
  |Nerozlišeno|Převody z rozpočtových účtů|Rozpočtové příjmy|3200000|
  |Nerozlišeno|Investiční přijaté transfery od obcí|Rozpočtové příjmy|3200000|
  |Nerozlišeno|Ostatní převody vlastním fondům|Rozpočtové výdaje|3200000|
  |Nerozlišeno|Investiční dotace obcím|Rozpočtové výdaje|3200000|
  And I have consolidation budget items for municipality "Adamov" and year "2011":
  |sector_classification|type_classification|budget_item_type|value|
  |Nerozlišeno|Převody z rozpočtových účtů|Rozpočtové příjmy|3200000|
  |Nerozlišeno|Ostatní převody vlastním fondům|Rozpočtové výdaje|3200000|
  And I have budget items for municipality "Brno" and year "2011":
  |sector_classification|type_classification|budget_item_type|value|
  |Nerozlišeno|Daň z nemovitostí|Rozpočtové příjmy|1000000|
  |Nerozlišeno|Mýtné|Rozpočtové příjmy|1500000|
  |Nerozlišeno|Příjmy z prodeje akcií|Rozpočtové příjmy|2000000|
  |Nerozlišeno|Převody z ostatních vlastních fondů|Rozpočtové příjmy|2500000|
  |Pěstební činnost|Pozemky|Rozpočtové výdaje|3000000|
  |Silnice|Pozemky|Rozpočtové výdaje|3200000|
  |Předškolní zařízení|Pozemky|Rozpočtové výdaje|3200000|
  |Rekvalifikace|Sociální dávky|Rozpočtové výdaje|3200000|
  |Armáda|Sociální dávky|Rozpočtové výdaje|3200000|
  |Archivní činnost|Sociální dávky|Rozpočtové výdaje|3200000|
  And I have budget items for municipality "Adamov" and year "2011":
  |sector_classification|type_classification|budget_item_type|value|
  |Nerozlišeno|Daň z nemovitostí|Rozpočtové příjmy|100000|
  |Nerozlišeno|Mýtné|Rozpočtové příjmy|150000|
  |Nerozlišeno|Příjmy z prodeje akcií|Rozpočtové příjmy|200000|
  |Nerozlišeno|Převody z ostatních vlastních fondů|Rozpočtové příjmy|250000|
  |Pěstební činnost|Pozemky|Rozpočtové výdaje|300000|
  |Silnice|Pozemky|Rozpočtové výdaje|320000|
  |Předškolní zařízení|Pozemky|Rozpočtové výdaje|320000|
  |Rekvalifikace|Sociální dávky|Rozpočtové výdaje|320000|
  |Armáda|Sociální dávky|Rozpočtové výdaje|320000|
  |Archivní činnost|Sociální dávky|Rozpočtové výdaje|320000|
  And I have budget item summary configurations:
  |name|type|column|value|parent|
  |Příjmy|type_classification|class_code|1,2,3,4||
  |Daňové|type_classification|class_code|1|Příjmy|
  |Výdaje|type_classification|class_code|5,6||
  |Investiční|type_classification|class_code|6|Výdaje|
  |Přebytek/Schodek|summary_expression||1-3||
  |Služby|sector_classification|category_code|3||
  |Vzdělávání|sector_classification|section_code|31,32|Služby|
  And I am on "/srovnani/555555-adamov/666666-brno"

  Scenario: See two municipalities are compared
    When I follow "2011"
    Then I should see "Brno"
    And I should see "Adamov"

  Scenario: See expenses are compared
    When I follow "2011"
    Then I should see "Brno"
    And I should see "Adamov"


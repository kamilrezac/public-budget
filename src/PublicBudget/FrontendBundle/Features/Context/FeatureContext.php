<?php

namespace PublicBudget\FrontendBundle\Features\Context;

use PublicBudget\FrontendBundle\Features\Context\BaseFeatureContext;

use Behat\Behat\Exception\PendingException;
use Behat\Gherkin\Node\PyStringNode,
    Behat\Gherkin\Node\TableNode;

/**
 * Feature context.
 */
class FeatureContext extends BaseFeatureContext
{
    /**
     * @Given /^I have a municipality "([^"]*)" in kraj "([^"]*)" and okres "([^"]*)" and CSU code "([^"]*)" with population in years:$/
     */
    public function iHaveAMunicipalityInKrajAndOkresWithPopulationInYears($municipalityName, $krajName, $okresName, $csuCode, TableNode $population)
    {
        $em = $this->kernel->getContainer()->get('doctrine')->getManager();

        if (isset($this->krajIdentityMap[$krajName])) {
            $kraj = $this->krajIdentityMap[$krajName];
        } else {
            $kraj = new \PublicBudget\FrontendBundle\Entity\Kraj();
            $kraj->setName($krajName);
            $kraj->setCode(1);
            $kraj->setPopulation(1);
            $em->persist($kraj);
            $this->krajIdentityMap[$krajName] = $kraj;
        }

        if (isset($this->okresIdentityMap[$okresName])) {
            $okres = $this->okresIdentityMap[$okresName];
        } else {
            $okres = new \PublicBudget\FrontendBundle\Entity\Okres();
            $okres->setName($okresName);
            $okres->setCode(1);
            $em->persist($okres);

            $this->okresIdentityMap[$okresName] = $okres;
        }

        $municipality = new \PublicBudget\FrontendBundle\Entity\Municipality();
        $municipality->setName($municipalityName);
        $municipality->setCsuCode($csuCode);
        $municipality->setOrganizationName('');
        $municipality->setOrganizationStreet('');
        $municipality->setOrganizationZip('');
        $municipality->setOrganizationIco('');
        $em->persist($municipality);

        $this->municipalityIdentityMap[$municipalityName] = $municipality;

        $municipality->setKraj($kraj);
        $municipality->setOkres($okres);

        $em->flush();

        foreach ($population->getHash() as $row) {
            $population = new \PublicBudget\FrontendBundle\Entity\Population();
            $population->setYear($row['year']);
            $population->setCount($row['value']);
            $population->setMunicipality($municipality);
            $em->persist($population);
        }

        $em->flush();
    }

    /**
     * @Given /^I have population ranges:$/
     */
    public function iHavePopulationRanges(TableNode $table)
    {
        $em = $this->kernel->getContainer()->get('doctrine')->getManager();

        foreach ($table->getHash() as $row) {
            $populationRange = new \PublicBudget\FrontendBundle\Entity\PopulationRange();
            $populationRange->setMin($row['min']);
            $populationRange->setMax($row['max']);
            $em->persist($populationRange);
        }

        $em->flush();
    }

    /**
     * @Given /^I have a sector classification budget structure:$/
     */
    public function iHaveASectorClassificationBudgetStructure(TableNode $table)
    {
        $em = $this->kernel->getContainer()->get('doctrine')->getManager();

        foreach ($table->getHash() as $row) {
            $sectorClassification = new \PublicBudget\FrontendBundle\Entity\SectorClassification();
            $sectorClassification->setCategoryCode($row['category_code']);
            $sectorClassification->setCategoryName($row['category_name']);
            $sectorClassification->setSectionCode($row['section_code']);
            $sectorClassification->setSectionName($row['section_name']);
            $sectorClassification->setSubsectionCode($row['subsection_code']);
            $sectorClassification->setSubsectionName($row['subsection_name']);
            $sectorClassification->setParagraphCode($row['paragraph_code']);
            $sectorClassification->setParagraphName($row['paragraph_name']);
            $em->persist($sectorClassification);

            $this->sectorClassificationIdentityMap[$row['paragraph_name']] = $sectorClassification;
        }

        $em->flush();
    }

    /**
     * @Given /^I have a type classification consolidation budget structure:$/
     */
    public function iHaveATypeClassificationConsolidationBudgetStructure(TableNode $table)
    {
        $this->iHaveATypeClassificationBudgetStructure($table);
    }

    /**
     * @Given /^I have a type classification budget structure:$/
     */
    public function iHaveATypeClassificationBudgetStructure(TableNode $table)
    {
        $em = $this->kernel->getContainer()->get('doctrine')->getManager();

        foreach ($table->getHash() as $row) {
            $typeClassification = new \PublicBudget\FrontendBundle\Entity\TypeClassification();
            if (isset($row['id'])) {
                $typeClassification->setId($row['id']);
            }
            $typeClassification->setClassCode($row['class_code']);
            $typeClassification->setClassName($row['class_name']);
            $typeClassification->setGroupCode($row['group_code']);
            $typeClassification->setGroupName($row['group_name']);
            $typeClassification->setSubgroupCode($row['subgroup_code']);
            $typeClassification->setSubgroupName($row['subgroup_name']);
            $typeClassification->setItemCode($row['item_code']);
            $typeClassification->setItemName($row['item_name']);
            $em->persist($typeClassification);
            if (isset($row['id'])) {
                $metadata = $em->getClassMetaData(get_class($typeClassification));
                $metadata->setIdGeneratorType(\Doctrine\ORM\Mapping\ClassMetadata::GENERATOR_TYPE_NONE);
            } else {
                $metadata = $em->getClassMetaData(get_class($typeClassification));
                $metadata->setIdGeneratorType(\Doctrine\ORM\Mapping\ClassMetadata::GENERATOR_TYPE_SEQUENCE);
            }
            $em->flush();
            $this->typeClassificationIdentityMap[$row['item_name']] = $typeClassification;
        }
    }

    /**
     * @Given /^I have budget item types:$/
     */
    public function iHaveBudgetItemTypes(TableNode $table)
    {
        $em = $this->kernel->getContainer()->get('doctrine')->getManager();

        foreach ($table->getHash() as $row) {
            $budgetItemType = new \PublicBudget\FrontendBundle\Entity\BudgetItemType();
            $budgetItemType->setName($row['name']);
            $em->persist($budgetItemType);

            $this->budgetItemTypeIdentityMap[$row['name']] = $budgetItemType;
        }

        $em->flush();
    }


    /**
     * @Given /^I have consolidation budget items for municipality "([^"]*)" and year "(\d+)":$/
     */
    public function iHaveConsolidationBudgetItemsForMunicipalityAndYear($municipalityName, $year, TableNode $table)
    {
        $this->iHaveBudgetItemsForMunicipalityAndYear($municipalityName, $year, $table);
    }

    /**
     * @Given /^I have budget items for municipality "([^"]*)" and year "(\d+)":$/
     */
    public function iHaveBudgetItemsForMunicipalityAndYear($municipalityName, $year, TableNode $table)
    {
        $em = $this->kernel->getContainer()->get('doctrine')->getManager();

        foreach ($table->getHash() as $key => $row) {
            $budgetItem = new \PublicBudget\FrontendBundle\Entity\BudgetItem();
            $budgetItem->setId($key);
            $budgetItem->setMunicipality($this->getMunicipality($municipalityName));
            $budgetItem->setType($this->getBudgetItemType($row['budget_item_type']));
            $budgetItem->setSectorClassification($this->getSectorClassification($row['sector_classification']));
            $budgetItem->setTypeClassification($this->getTypeClassification($row['type_classification']));
            $budgetItem->setYear($year);
            $budgetItem->setValueApproved(0);
            $budgetItem->setValueAfterChanges(0);
            $budgetItem->setValueFinal($row['value']);
            $em->persist($budgetItem);
        }

        $em->flush();
    }

    /**
     * @Given /^I have budget item summary configurations:$/
     */
    public function iHaveBudgetItemSummaryConfigurations(TableNode $table)
    {
        $em = $this->kernel->getContainer()->get('doctrine')->getManager();

        foreach ($table->getHash() as $row) {
            $budgetItemSummaryConfiguration = new \PublicBudget\FrontendBundle\Entity\BudgetItemSummaryConfiguration();
            $budgetItemSummaryConfiguration->setType($row['type']);
            $budgetItemSummaryConfiguration->setValue($row['value']);
            $budgetItemSummaryConfiguration->setName($row['name']);
            $budgetItemSummaryConfiguration->setCode($row['code']);
            if ($row['parent']) {
                $budgetItemSummaryConfiguration->setParent($this->getBudgetItemSummaryConfiguration($row['parent']));
            } else {
                $budgetItemSummaryConfiguration->setParent(null);
            }
            $em->persist($budgetItemSummaryConfiguration);


            $this->budgetItemSummaryConfigurationIdentityMap[$row['name']] = $budgetItemSummaryConfiguration;
        }

        $em->flush();
    }


    /**
     * @Then /^I should see municipalities in order "([^"]*)" in element "([^"]*)"$/
     */
    public function iShouldSeeMunicipalitiesInOrderInElement($municipalityOrder, $cssQuery)
    {
        $expectedMunicipalityOrder = explode(',', $municipalityOrder);

        $municipalityOrder = array_map(
            function ($element) {
                return $element->getText();
            },
            $this->getSession()->getPage()->findAll('css', $cssQuery)
        );

        assertEquals($expectedMunicipalityOrder, $municipalityOrder);
    }

    /**
     * @Given /^I should see value "([^"]*)"$/
     */
    public function iShouldSeeValue($value)
    {
        $values = array_map(
            function ($element) {
                return str_replace("\xc2\xa0"," ",$element->getText());
            },
            $this->getSession()->getPage()->findAll('css', 'td.schodek')
        );

        assertContains($value, $values);
    }

    /**
     * @Then /^response CSV should be:$/
     */
    public function responseCsvShouldBe(PyStringNode $string)
    {
        $responseBody = $this->getSession()->getPage()->getContent();
        $string = iconv('utf-8', 'windows-1250', $string);
        assertEquals((string)$string, $responseBody);
    }

}


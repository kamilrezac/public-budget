<?php

namespace PublicBudget\FrontendBundle\Features\Context;

use Symfony\Component\HttpKernel\KernelInterface;
use Behat\Symfony2Extension\Context\KernelAwareInterface;
use Behat\MinkExtension\Context\MinkContext;

require_once 'PHPUnit/Autoload.php';
require_once 'PHPUnit/Framework/Assert/Functions.php';

/**
 * Feature context.
 */
class BaseFeatureContext extends MinkContext
                  implements KernelAwareInterface
{
    protected $kernel;
    protected $parameters;

    protected $okresIdentityMap = array();
    protected $krajIdentityMap = array();
    protected $municipalityIdentityMap = array();
    protected $sectorClassificationIdentityMap = array();
    protected $typeClassificationIdentityMap = array();
    protected $budgetItemTypeMap = array();
    protected $budgetItemSummaryConfigurationIdentityMap = array();

    /**
     * Initializes context with parameters from behat.yml.
     *
     * @param array $parameters
     */
    public function __construct(array $parameters)
    {
        $this->parameters = $parameters;
    }

    /**
     * Sets HttpKernel instance.
     * This method will be automatically called by Symfony2Extension ContextInitializer.
     *
     * @param KernelInterface $kernel
     */
    public function setKernel(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    /**
     * @BeforeScenario
     */
    public function cleanDatabase()
    {
        $em = $this->kernel->getContainer()->get('doctrine')->getManager();
        $entities = array('Municipality', 'Kraj', 'Okres', 'Population', 'PopulationRange', 'SectorClassification','TypeClassification', 'BudgetItemType', 'BudgetItem', 'budgetItemSummaryConfiguration');
        foreach ($entities as $entity) {
            $cmd = $em->getClassMetadata('PublicBudgetFrontendBundle:'.$entity);
            $connection = $em->getConnection();
            $dbPlatform = $connection->getDatabasePlatform();
            $connection->beginTransaction();
            try {
                $connection->query('SET FOREIGN_KEY_CHECKS=0');
                $q = $dbPlatform->getTruncateTableSql($cmd->getTableName());
                $connection->executeUpdate($q);
                $connection->query('SET FOREIGN_KEY_CHECKS=1');
                $connection->commit();
            }
            catch (\Exception $e) {
                $connection->rollback();
            }
        }
    }

    protected function getMunicipality($name)
    {
        if (isset($this->municipalityIdentityMap[$name])) {
            return $this->municipalityIdentityMap[$name];
        }

        throw new Exception("Object not found");
    }

    protected function getKraj($name)
    {
        if (isset($this->krajIdentityMap[$name])) {
            return $this->krajIdentityMap[$name];
        }

        throw new Exception("Object not found");
    }

    protected function getOkres($name)
    {
        if (isset($this->okresIdentityMap[$name])) {
            return $this->okresIdentityMap[$name];
        }

        throw new Exception("Object not found");
    }

    protected function getSectorClassification($name)
    {
        if (isset($this->sectorClassificationIdentityMap[$name])) {
            return $this->sectorClassificationIdentityMap[$name];
        }

        throw new Exception("Object not found");
    }

    protected function getTypeClassification($name)
    {
        if (isset($this->typeClassificationIdentityMap[$name])) {
            return $this->typeClassificationIdentityMap[$name];
        }

        throw new Exception("Object not found");
    }

    protected function getBudgetItemType($name)
    {
        if (isset($this->budgetItemTypeIdentityMap[$name])) {
            return $this->budgetItemTypeIdentityMap[$name];
        }

        throw new Exception("Object not found");
    }

    protected function getBudgetItemSummaryConfiguration($name)
    {
        if (isset($this->budgetItemSummaryConfigurationIdentityMap[$name])) {
            return $this->budgetItemSummaryConfigurationIdentityMap[$name];
        }

        throw new Exception("Object not found");
    }

    /**
     * @Given /^I select radio button "([^"]*)"$/
     */
    public function iSelectRadioButton($radio_label) {
      $radio_button = $this->getSession()->getPage()->findField($radio_label);
      if (null === $radio_button) {
        throw new ElementNotFoundException(
          $this->getSession(), 'form field', 'id|name|label|value', $field
        );
      }
      $value = $radio_button->getAttribute('value');
      $this->fillField($radio_label, $value);
    }


    /**
     * @When /^I start typing "([^"]*)" and select "([^"]*)" from autocomplete field "([^"]*)"$/
     */
    public function iStartTypingAndSelectFromAutocompleteField($municipalitySubstring, $municipalityName, $autocompleteField)
    {
        // $this->getSession()->executeScript("$('#".$autocompleteField."').trigger('focus')");
        $this->fillField($autocompleteField, $municipalitySubstring);
        // $this->getSession()->executeScript("$('#".$autocompleteField."').trigger('keydown')");
        $this->getSession()->wait(1000,
            "$('.ui-autocomplete').children().length > 0"
        );
        $this->getSession()->executeScript("$('.ui-menu-item a:contains(\'^".$municipalityName."$\')').trigger('mouseenter').trigger('click')");
    }

}




<?php

namespace PublicBudget\FrontendBundle\Repository;

use Doctrine\ORM\EntityRepository;
use PublicBudget\FrontendBundle\Municipality\Common\BudgetSlugConverter;

class AdministrationUnitExpenseRepository extends AdministrationUnitAbstractRepository
{
    public function findAjaxData($code)
    {
        $this->qb->select(array('SUM(b.valueFinal) AS value', 'b.year'))
            ->from($this->administrationUnitHelperData->budgetItemClass, 'b')
            ->innerJoin('b.typeClassification', 'tc');

        $this->qb->innerJoin('b.sectorClassification', 'sc');

        $this->qb->where($this->qb->expr()->andx(
                $this->qb->expr()->eq('b.'.$this->administrationUnitHelperData->name, ':'.$this->administrationUnitHelperData->name),
                $this->qb->expr()->notIn('tc.itemCode', ':expense_consolidation'),
                $this->qb->expr()->gte('b.year', 2000)
                ))
            ->setParameter($this->administrationUnitHelperData->name, $this->administrationUnit)
            ->setParameter('expense_consolidation', $this->budgetStructureLogic->expenseConsolidation);

        if ($code) {
            $code = explode('|', $code);
            foreach ($code as $codePart) {
                $codePart = explode(',', $codePart);
                $codeType = array_shift($codePart);
                if ($codeType == 'tc') {
                    $column = 'tc.'.self::$typeClassificationCodeLengthToColumnMapping[strlen($codePart[0])];
                } else if ($codeType == 'sc') {
                    $column = 'sc.'.self::$sectorClassificationCodeLengthToColumnMapping[strlen($codePart[0])];
                } else {
                    throw new Exception('There is no code type with name '. $codeType);
                }

                $this->qb->andWhere($this->qb->expr()->in($column, $codePart));
            }
        }

        $this->qb->orderBy('b.year');

        $this->qb->groupBy('b.year')
            ->having($this->qb->expr()->neq('value', '0'));

        return $this->qb->getQuery()->execute();
    }

    public function findSectionDetail($year, $sectionSlug, $sectionFilter)
    {
        $this->qb->select(array('SUM(b.valueFinal) AS value', 'sc.paragraphName AS itemName', 'sc.paragraphCode AS itemCode', 'sc.subsectionCode AS subgroupCode', 'sc.subsectionName AS subgroupName'))
                ->from($this->administrationUnitHelperData->budgetItemClass, 'b')
                ->innerJoin('b.typeClassification', 'tc')
                ->innerJoin('b.sectorClassification', 'sc')
                ->groupBy('sc.paragraphName', 'sc.paragraphCode', 'sc.subsectionCode', 'sc.subsectionName')
                ->having($this->qb->expr()->neq('value', '0'));

        $code = BudgetSlugConverter::$toSectionCode[$sectionSlug];
        $code = explode(',', $code);
        $column = 'sc.'.self::$sectorClassificationCodeLengthToColumnMapping[strlen(current($code))];

        $this->qb->where($this->qb->expr()->andx(
                $this->qb->expr()->eq('b.'.$this->administrationUnitHelperData->name, ':'.$this->administrationUnitHelperData->name),
                $this->qb->expr()->eq('b.year', ':year'),
                $this->qb->expr()->notIn('tc.itemCode', ':expense_consolidation'),
                $this->qb->expr()->in($column, ':code')))
            ->setParameter($this->administrationUnitHelperData->name, $this->administrationUnit)
            ->setParameter('year', $year)
            ->setParameter('expense_consolidation', $this->budgetStructureLogic->expenseConsolidation)
            ->setParameter('code', $code);

        if ($sectionFilter == 'provozni') {
            $this->qb->andWhere($this->qb->expr()->eq('tc.classCode', '5'));
        } else if ($sectionFilter == 'investicni') {
            $this->qb->andWhere($this->qb->expr()->eq('tc.classCode', '6'));
        } else {
            $this->qb->andWhere($this->qb->expr()->orx(
                  $this->qb->expr()->eq('tc.classCode', '5'),
                  $this->qb->expr()->eq('tc.classCode', '6')
                ));
        }

        $this->qb->orderBy('sc.subsectionCode, sc.paragraphCode');

        return $this->qb->getQuery()->execute();
    }
}
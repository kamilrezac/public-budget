<?php

namespace PublicBudget\FrontendBundle\Repository;

use Doctrine\ORM\EntityRepository;

class MunicipalityRepository extends EntityRepository
{
    public function findAllByFirstLetter($firstLetter)
    {
        if ($firstLetter == 'C') {
            $whereClause = "WHERE m.name LIKE 'C%' AND m.name NOT LIKE 'Ch%'";
        } else {
            $whereClause = "WHERE m.name LIKE '".$firstLetter."%'";
        }

        return $this->getEntityManager()
            ->createQuery("SELECT m FROM PublicBudgetFrontendBundle:Municipality m ".$whereClause." ORDER BY m.name ASC")
            ->getResult();
    }

    public function searchAllBySubstring($substring)
    {
        return $this->getEntityManager()
            ->createQuery("SELECT m FROM PublicBudgetFrontendBundle:Municipality m WHERE m.name LIKE :substring ORDER BY m.name ASC")
            ->setParameter('substring','%'.$substring.'%')
            ->getResult();
    }

    // public function whisperSimilarMunicipalities($substring)
    // {
    //     return $this->getEntityManager()
    //         ->createQuery("SELECT m.id, m.name, o.name as okres_name
    //          FROM PublicBudgetFrontendBundle:Municipality m
    //          INNER JOIN m.okres o
    //          INNER JOIN m.kraj k
    //          INNER JOIN m.population p
    //          WHERE p.year=2013
    //          WHERE m.name LIKE :substring
    //          ORDER BY m.name ASC")
    //         ->setParameter('substring','%'.$substring.'%')
    //         ->getResult();
    // }

    public function searchAllByName($name)
    {
        return $this->getEntityManager()
            ->createQuery("SELECT m FROM PublicBudgetFrontendBundle:Municipality m WHERE m.name = :name ORDER BY m.name ASC")
            ->setParameter('name',$name)
            ->getResult();
    }

    public function getMunicipalityByNameAndGeographicLocationFrom($userLocationData)
    {
        if (!$userLocationData) {
            return false;
        }

        $result = $this->getEntityManager()
             ->getConnection()
             ->fetchAll("SELECT ((ACOS(SIN(:lat * PI() / 180) * SIN(latitude * PI() / 180) + COS(:lat * PI() / 180) * COS(latitude * PI() / 180) * COS((:lon - longitude) * PI() / 180)) * 180 / PI()) * 60 * 1.1515) AS distance, name, slug
                         FROM municipality
                         WHERE name LIKE '%".$userLocationData->city->name."%'
                         ORDER BY distance ASC", array(':lat' => $userLocationData->location->latitude, ':lon' => $userLocationData->location->longitude));

        if ($result) {
            return $result[0];
        }

        $result = $this->getEntityManager()
             ->getConnection()
             ->fetchAll("SELECT ((ACOS(SIN(:lat * PI() / 180) * SIN(latitude * PI() / 180) + COS(:lat * PI() / 180) * COS(latitude * PI() / 180) * COS((:lon - longitude) * PI() / 180)) * 180 / PI()) * 60 * 1.1515) AS distance, name, slug
                         FROM municipality
                         ORDER BY distance ASC", array(':lat' => $userLocationData->location->latitude, ':lon' => $userLocationData->location->longitude));

        if ($result) {
            return $result[0];
        }

        return false;
    }

    public function findAllAsChoices()
    {
        return $this->getEntityManager()
            ->createQuery("SELECT m.id, m.name FROM PublicBudgetFrontendBundle:Municipality m ORDER BY m.name ASC")
            ->getArrayResult();
    }
}

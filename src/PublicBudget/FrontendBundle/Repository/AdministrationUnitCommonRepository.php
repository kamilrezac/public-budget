<?php

namespace PublicBudget\FrontendBundle\Repository;

use Doctrine\ORM\EntityRepository;
use PublicBudget\FrontendBundle\Municipality as Municipality;

class AdministrationUnitCommonRepository extends AdministrationUnitAbstractRepository
{
    public function findTotalBalanceFor()
    {
        $result = $this->entityManager
             ->getConnection()
             ->fetchColumn("SELECT income - expense AS totalBalance
             FROM (SELECT
               SUM(IF(t.id=1,b.value_final,0)) AS income,
               SUM(IF(t.id=2,b.value_final,0)) AS expense
               FROM " . $this->administrationUnitHelperData->budgetItemTable . " b
               JOIN budget_item_type t
               ON t.id=b.budget_item_type_id
               JOIN type_classification tc
               ON tc.id=b.type_classification_id
               WHERE b." . $this->administrationUnitHelperData->name . "_id=:" . $this->administrationUnitHelperData->name . "_id
               AND b.year >= 2000
               AND tc.item_code NOT IN (".implode(',',array_merge($this->budgetStructureLogic->incomeConsolidation, $this->budgetStructureLogic->expenseConsolidation)).")
             ) AS b", array($this->administrationUnitHelperData->name . '_id'=>$this->administrationUnit->getId()));

        return $result;
    }

    public function findIncomeAndExpenseGroupedByYearFor()
    {
        return $this->entityManager
             ->getConnection()
             ->fetchAll("SELECT b.year,
             SUM(IF(t.id = 1, b.value_final, 0)) AS income,
             SUM(IF(t.id = 2, b.value_final, 0)) AS expense
             FROM " . $this->administrationUnitHelperData->budgetItemTable . " b
             JOIN budget_item_type t
             ON t.id=b.budget_item_type_id
             JOIN type_classification tc
             ON tc.id=b.type_classification_id
             WHERE b." . $this->administrationUnitHelperData->name . "_id=:" . $this->administrationUnitHelperData->name . "_id AND b.year >= 2000 AND tc.item_code NOT IN (".implode(',',array_merge($this->budgetStructureLogic->incomeConsolidation, $this->budgetStructureLogic->expenseConsolidation)).")
             GROUP BY b.year", array($this->administrationUnitHelperData->name.'_id'=>$this->administrationUnit->getId()));
    }

    public function findIncomeSectionsFor($year)
    {
        $result = $this->entityManager
             ->getConnection()
             ->fetchAll("SELECT SUM(b.value_final) AS value,
             tc.class_code AS code
             FROM " . $this->administrationUnitHelperData->budgetItemTable . " b
             JOIN type_classification tc
             ON tc.id=b.type_classification_id
             WHERE tc.class_code IN (1,2,3,4) AND b." . $this->administrationUnitHelperData->name . "_id=:" . $this->administrationUnitHelperData->name . "_id AND b.year=:year AND tc.item_code NOT IN (".implode(',',$this->budgetStructureLogic->incomeConsolidation).")
             GROUP BY code", array($this->administrationUnitHelperData->name.'_id'=>$this->administrationUnit->getId(), 'year' => $year));

        return $result;
    }

    public function findExpenseByPurposeSectionsFor($year, $expensesSectionFilter)
    {
        $this->qb->select(array('SUM(b.valueFinal) AS value', 'sc.categoryCode AS code', 'sc.sectionCode'))
        ->from($this->administrationUnitHelperData->budgetItemClass, 'b')
        ->innerJoin('b.typeClassification', 'tc')
        ->innerJoin('b.sectorClassification', 'sc')
        ->where($this->qb->expr()->andx(
                 $this->qb->expr()->eq('b.'.$this->administrationUnitHelperData->name, ':'.$this->administrationUnitHelperData->name),
                 $this->qb->expr()->eq('b.year', ':year'),
                 $this->qb->expr()->notIn('tc.itemCode', ':expense_consolidation'),
                 $this->qb->expr()->in('tc.classCode', ':class_code')
                 ))
        ->setParameter($this->administrationUnitHelperData->name, $this->administrationUnit)
        ->setParameter('year', $year)
        ->setParameter('expense_consolidation', $this->budgetStructureLogic->expenseConsolidation);

        if ($expensesSectionFilter == 'provozni') {
            $this->qb->setParameter('class_code', array(5));
        } else if ($expensesSectionFilter == 'investicni') {
            $this->qb->setParameter('class_code', array(6));
        } else {
            $this->qb->setParameter('class_code', array(5,6));
        }

        $this->qb->groupBy('sc.categoryCode', 'sc.sectionCode');

        $result = $this->qb->getQuery()->execute();

        $correctResult = array();

        foreach ($result as &$row) {
            if ($row['code'] == 3) {
                if ($row['sectionCode'] == 31 || $row['sectionCode'] == 32) {
                    $row['code'] = '31,32';
                } else {
                    $row['code'] = $row['sectionCode'];
                }
            }
            unset($row['sectionCode']);
            if (!isset($correctResult[$row['code']])) {
                $correctResult[$row['code']] = $row;
            } else {
                $correctResult[$row['code']]['value'] += $row['value'];
            }
        }

        return $correctResult;
    }

    public function findFinancingFor($year)
    {
        $result = $this->entityManager
             ->getConnection()
             ->fetchAll("SELECT -1*SUM(b.value_final) AS value,
             CASE
             WHEN tc.item_code IN (".implode(',',$this->budgetStructureLogic->changeOfAssets).") then 'Změna prostředků na účtech'
             WHEN tc.item_code IN (".implode(',',$this->budgetStructureLogic->changeOfDebt).") then 'Změna zadlužení'
             END budget_section
             FROM " . $this->administrationUnitHelperData->budgetItemTable . " b
             JOIN type_classification tc
             ON tc.id=b.type_classification_id
             WHERE b." . $this->administrationUnitHelperData->name . "_id=:" . $this->administrationUnitHelperData->name . "_id AND b.year=:year
             GROUP BY budget_section
             HAVING budget_section IS NOT NULL", array($this->administrationUnitHelperData->name.'_id'=>$this->administrationUnit->getId(), 'year' => $year));

        return $result;
    }

    public function findTransfersFor($year)
    {
        $result = $this->entityManager
             ->getConnection()
             ->fetchAll("SELECT SUM(b.value_final) AS value,
             CASE
             WHEN tc.item_code IN (".implode(',',$this->budgetStructureLogic->incomeConsolidation).") then 'Příjmy'
             WHEN tc.item_code IN (".implode(',',$this->budgetStructureLogic->expenseConsolidation) .") then 'Výdaje'
             END budget_section
             FROM " . $this->administrationUnitHelperData->budgetItemTable . " b
             JOIN type_classification tc
             ON tc.id=b.type_classification_id
             WHERE b." . $this->administrationUnitHelperData->name . "_id=:" . $this->administrationUnitHelperData->name . "_id AND b.year=:year
             GROUP BY budget_section
             HAVING budget_section IS NOT NULL", array($this->administrationUnitHelperData->name.'_id'=>$this->administrationUnit->getId(), 'year' => $year));

        return $result;
    }
}
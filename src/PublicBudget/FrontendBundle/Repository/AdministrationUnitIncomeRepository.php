<?php

namespace PublicBudget\FrontendBundle\Repository;

use Doctrine\ORM\EntityRepository;
use PublicBudget\FrontendBundle\Municipality\Common\BudgetSlugConverter;

class AdministrationUnitIncomeRepository extends AdministrationUnitAbstractRepository
{
    public function findAjaxData($code)
    {
        $this->qb->select(array('SUM(b.valueFinal) AS value', 'b.year'))
            ->from($this->administrationUnitHelperData->budgetItemClass, 'b')
            ->innerJoin('b.typeClassification', 'tc');

        $this->qb->where($this->qb->expr()->andx(
                 $this->qb->expr()->eq('b.'.$this->administrationUnitHelperData->name, ':'.$this->administrationUnitHelperData->name),
                 $this->qb->expr()->notIn('tc.itemCode', ':income_consolidation'),
                 $this->qb->expr()->gte('b.year', 2000)
                 ))
            ->setParameter($this->administrationUnitHelperData->name, $this->administrationUnit)
            ->setParameter('income_consolidation', $this->budgetStructureLogic->incomeConsolidation);

        if ($code) {
            $code = explode(',', $code);
            $column = 'tc.'.self::$typeClassificationCodeLengthToColumnMapping[strlen(current($code))];
            $this->qb->andWhere($this->qb->expr()->in($column, $code));
        }

        $this->qb->orderBy('b.year');

        $this->qb->groupBy('b.year')
            ->having($this->qb->expr()->neq('value', '0'));

        return $this->qb->getQuery()->execute();
    }

    public function findSectionDetail($year, $sectionSlug, $sectionFilter)
    {
        $this->qb->select(array('b.valueFinal AS value', 'tc.itemName', 'tc.itemCode', 'tc.subgroupCode', 'tc.subgroupName'))
                ->from($this->administrationUnitHelperData->budgetItemClass, 'b')
                ->innerJoin('b.typeClassification', 'tc');

        $this->qb->where($this->qb->expr()->andx(
                 $this->qb->expr()->eq('b.'.$this->administrationUnitHelperData->name, ':'.$this->administrationUnitHelperData->name),
                 $this->qb->expr()->eq('b.year', ':year'),
                 $this->qb->expr()->notIn('tc.itemCode', ':income_consolidation'),
                 $this->qb->expr()->eq('tc.classCode', ':class_code'),
                 $this->qb->expr()->neq('b.valueFinal', '0')))
            ->setParameter($this->administrationUnitHelperData->name, $this->administrationUnit)
            ->setParameter('year', $year)
            ->setParameter('income_consolidation', $this->budgetStructureLogic->incomeConsolidation)
            ->setParameter('class_code', BudgetSlugConverter::$toSectionCode[$sectionSlug]);

        $this->qb->orderBy('tc.subgroupCode, tc.itemCode');

        return $this->qb->getQuery()->execute();
    }
}
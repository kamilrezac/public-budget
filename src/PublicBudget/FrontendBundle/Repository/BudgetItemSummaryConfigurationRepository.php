<?php

namespace PublicBudget\FrontendBundle\Repository;

use Doctrine\ORM\EntityRepository;

class BudgetItemSummaryConfigurationRepository extends EntityRepository
{
	public function findSectorClassificationSummaries()
	{
		return $this->createQueryBuilder('c')
        ->where('c.level IN (2,3,4)')
        ->andWhere("c.type = 'sector_classification'")->getQuery()->getArrayResult();
	}

	public function findIncomeTypeClassificationSummaries()
	{
		return $this->createQueryBuilder('c')
        ->where("c.type = 'type_classification'")
        ->andWhere("c.id > 1 AND c.id < 282")->getQuery()->getArrayResult();
	}

	public function findExpenseTypeClassificationSummaries()
	{
		return $this->createQueryBuilder('c')
        ->where("c.type = 'type_classification'")
        ->andWhere("c.id > 282 AND c.id < 647")->getQuery()->getArrayResult();
	}
}
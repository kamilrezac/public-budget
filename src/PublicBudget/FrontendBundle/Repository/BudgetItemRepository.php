<?php

namespace PublicBudget\FrontendBundle\Repository;

use Doctrine\ORM\EntityRepository;
use PublicBudget\FrontendBundle\Municipality\Common\BudgetStructureLogicFactory;

class BudgetItemRepository extends EntityRepository
{
    public function findByMunicipalityForExport($municipality, $budgetStructureLogic)
    {
        $consolidation = array_merge($budgetStructureLogic->incomeConsolidation, $budgetStructureLogic->expenseConsolidation);

        return $this->getEntityManager()
             ->getConnection()
             ->fetchAll("SELECT o.name AS okres_name, m.name, m.csu_code,
             m.organization_ico, b.year,
             tc.class_code, CONCAT(tc.class_code,'. ',tc.class_name) AS class_name,
             tc.item_code, CONCAT(tc.item_code,'. ',tc.item_name) AS item_name,
             sc.category_code, CONCAT(sc.category_code,'. ',sc.category_name) AS category_name,
             sc.paragraph_code, CONCAT(sc.paragraph_code,'. ',sc.paragraph_name) AS paragraph_name,
             IF (tc.item_code IN (".implode(',',$consolidation)."),'Nekonsolidovaný','Konsolidovaný') AS consolidation,
             b.value_final
             FROM budget_item b
             JOIN type_classification tc
             ON tc.id=b.type_classification_id
             JOIN sector_classification sc
             ON sc.id=b.sector_classification_id
             JOIN municipality m
             ON m.id=b.municipality_id
             JOIN okres o
             ON o.id=m.okres_id
             WHERE b.municipality_id=:municipality_id AND b.year>=2000
             ORDER BY b.year,tc.class_code",
             array(':municipality_id' => $municipality->getId()));
    }
}
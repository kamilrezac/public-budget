<?php

namespace PublicBudget\FrontendBundle\Repository;

use Doctrine\ORM\EntityRepository;
use PublicBudget\FrontendBundle\Municipality as Municipality;

class AdministrationUnitAbstractRepository
{
    protected $qb;

    protected $administrationUnit;

    protected $budgetStructureLogic;

    protected $entityManager;

    protected $administrationUnitHelperData;

    static protected $typeClassificationCodeLengthToColumnMapping = array(
        1 => 'classCode',
        2 => 'groupCode',
        3 => 'subgroupCode',
        4 => 'itemCode'
    );

    static protected $sectorClassificationCodeLengthToColumnMapping = array(
        1 => 'categoryCode',
        2 => 'sectionCode',
        3 => 'subsectionCode',
        4 => 'paragraphCode'
    );

    public function __construct($entityManager, $administrationUnit, $budgetStructureLogic, $administrationUnitHelperData)
    {
        $this->entityManager = $entityManager;
        $this->qb = $entityManager->createQueryBuilder();
        $this->administrationUnit = $administrationUnit;
        $this->budgetStructureLogic = $budgetStructureLogic;
        $this->administrationUnitHelperData = $administrationUnitHelperData;
    }
}
<?php

namespace PublicBudget\FrontendBundle\Repository;

use Doctrine\ORM\EntityRepository;
use PublicBudget\FrontendBundle\Municipality as Municipality;

class AdministrationUnitTransfersRepository extends AdministrationUnitAbstractRepository
{
    public function findSectionDetail($year, $sectionSlug, $sectionFilter)
    {
        $this->qb->select(array('b.valueFinal AS value', 'tc.itemName', 'tc.itemCode', 'tc.subgroupCode', 'tc.subgroupName'))
                ->from($this->administrationUnitHelperData->budgetItemClass, 'b')
                ->innerJoin('b.typeClassification', 'tc');

        $this->qb->where($this->qb->expr()->andx(
                $this->qb->expr()->eq('b.'.$this->administrationUnitHelperData->name, ':'.$this->administrationUnitHelperData->name),
                $this->qb->expr()->eq('b.year', ':year'),
                $this->qb->expr()->in('tc.itemCode', ':item_code'),
                $this->qb->expr()->neq('b.valueFinal', '0')))
            ->setParameter($this->administrationUnitHelperData->name, $this->administrationUnit)
            ->setParameter('year', $year);

        if ($sectionSlug == 'prijmy') {
            $this->qb->setParameter('item_code', $this->budgetStructureLogic->incomeConsolidation);
        } else if ($sectionSlug == 'vydaje') {
            $this->qb->setParameter('item_code', $this->budgetStructureLogic->expenseConsolidation);
        } else {
            throw new Exception('There is no transfer section with name '. $sectionSlug);
        }

        $this->qb->orderBy('tc.subgroupCode, tc.itemCode');

        return $this->qb->getQuery()->execute();
    }
}
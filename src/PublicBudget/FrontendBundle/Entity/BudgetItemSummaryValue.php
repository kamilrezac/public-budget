<?php

namespace PublicBudget\FrontendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PublicBudget\FrontendBundle\Entity\BudgetItemSummaryValue
 *
 * @ORM\Table(name="budget_item_summary_value", indexes={@ORM\Index(name="value_idx", columns={"value"})})
 * @ORM\Entity
 */
class BudgetItemSummaryValue
{
    /**
     * @ORM\OneToOne(targetEntity="Municipality")
     * @ORM\Id
     * @ORM\JoinColumn(name="municipality_id", referencedColumnName="id")
     */
    private $municipality;


    /**
     * @var decimal $value
     *
     * @ORM\Column(name="value", type="decimal", precision=14, scale=2)
     */
    private $value;


    /**
     * Set value
     *
     * @param float $value
     * @return BudgetItemSummaryValue
     */
    public function setValue($value)
    {
        $this->value = $value;
    
        return $this;
    }

    /**
     * Get value
     *
     * @return float 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set municipality
     *
     * @param \PublicBudget\FrontendBundle\Entity\Municipality $municipality
     * @return BudgetItemSummaryValue
     */
    public function setMunicipality(\PublicBudget\FrontendBundle\Entity\Municipality $municipality)
    {
        $this->municipality = $municipality;
    
        return $this;
    }

    /**
     * Get municipality
     *
     * @return \PublicBudget\FrontendBundle\Entity\Municipality 
     */
    public function getMunicipality()
    {
        return $this->municipality;
    }
}
<?php

namespace PublicBudget\FrontendBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;

/**
 * PublicBudget\FrontendBundle\Entity\BudgetItemSummaryConfiguration
 *
 * @ORM\Table(name="budget_item_summary_configuration")
 * @ORM\Entity(repositoryClass="PublicBudget\FrontendBundle\Repository\BudgetItemSummaryConfigurationRepository")
 */
class BudgetItemSummaryConfiguration
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Gedmo\Slug(separator="-", updatable=true, unique=true, fields={"name"})
     * @ORM\Column(name="slug", type="string", length=255)
     */
    private $slug;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string $value
     *
     * @ORM\Column(name="value", type="text")
     */
    private $value;

    /**
     * @var string $code
     *
     * @ORM\Column(name="code", type="string", length=255)
     */
    private $code;

    /**
     * @var string $type
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;

    /**
     * @var integer $level
     *
     * @ORM\Column(name="level", type="integer", nullable=true)
     */
    private $level;

    /**
     * @ORM\ManyToOne(targetEntity="BudgetItemSummaryConfiguration", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity="BudgetItemSummaryConfiguration", mappedBy="BudgetItemSummaryConfiguration")
     */
    private $children;

    public function __toString()
    {
        return $this->getName();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return BudgetItemSummaryConfiguration
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return BudgetItemSummaryConfiguration
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return BudgetItemSummaryConfiguration
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return BudgetItemSummaryConfiguration
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->children = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set parent
     *
     * @param \PublicBudget\FrontendBundle\Entity\BudgetItemSummaryConfiguration $parent
     * @return BudgetItemSummaryConfiguration
     */
    public function setParent(\PublicBudget\FrontendBundle\Entity\BudgetItemSummaryConfiguration $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \PublicBudget\FrontendBundle\Entity\BudgetItemSummaryConfiguration
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Add children
     *
     * @param \PublicBudget\FrontendBundle\Entity\BudgetItemSummaryConfiguration $children
     * @return BudgetItemSummaryConfiguration
     */
    public function addChildren(\PublicBudget\FrontendBundle\Entity\BudgetItemSummaryConfiguration $children)
    {
        $this->children[] = $children;

        return $this;
    }

    /**
     * Remove children
     *
     * @param \PublicBudget\FrontendBundle\Entity\BudgetItemSummaryConfiguration $children
     */
    public function removeChildren(\PublicBudget\FrontendBundle\Entity\BudgetItemSummaryConfiguration $children)
    {
        $this->children->removeElement($children);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return BudgetItemSummaryConfiguration
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set level
     *
     * @param integer $level
     * @return BudgetItemSummaryConfiguration
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Get level
     *
     * @return integer
     */
    public function getLevel()
    {
        return $this->level;
    }


    public function getParentName()
    {

        if ($this->getParent() === null) {
            return null;
        }

        return $this->getParent()->getName();
    }
}
<?php

namespace PublicBudget\FrontendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * PublicBudget\FrontendBundle\Entity\SectorClassification
 *
 * @ORM\Table(name="sector_classification")
 * @ORM\Entity
 */
class SectorClassification
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer $categoryCode
     *
     * @ORM\Column(name="category_code", type="integer")
     */
    private $categoryCode;

    /**
     * @var string $categoryName
     *
     * @ORM\Column(name="category_name", type="string", length=255)
     */
    private $categoryName;

    /**
     * @var integer $sectionCode
     *
     * @ORM\Column(name="section_code", type="integer")
     */
    private $sectionCode;

    /**
     * @var string $sectionName
     *
     * @ORM\Column(name="section_name", type="string", length=255)
     */
    private $sectionName;

    /**
     * @var integer $subsectionCode
     *
     * @ORM\Column(name="subsection_code", type="integer")
     */
    private $subsectionCode;

    /**
     * @var string $subsectionName
     *
     * @ORM\Column(name="subsection_name", type="string", length=255)
     */
    private $subsectionName;

    /**
     * @var integer $paragraphCode
     *
     * @ORM\Column(name="paragraph_code", type="integer")
     */
    private $paragraphCode;

    /**
     * @var string $paragraphName
     *
     * @ORM\Column(name="paragraph_name", type="string", length=255)
     */
    private $paragraphName;

    /**
     * @ORM\OneToMany(targetEntity="BudgetItemKrajSummary", mappedBy="sectorClassification")
     */
    private $budgetItemKrajSummaries;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set categoryCode
     *
     * @param integer $categoryCode
     */
    public function setCategoryCode($categoryCode)
    {
        $this->categoryCode = $categoryCode;
    }

    /**
     * Get categoryCode
     *
     * @return integer
     */
    public function getCategoryCode()
    {
        return $this->categoryCode;
    }

    /**
     * Set categoryName
     *
     * @param string $categoryName
     */
    public function setCategoryName($categoryName)
    {
        $this->categoryName = $categoryName;
    }

    /**
     * Get categoryName
     *
     * @return string
     */
    public function getCategoryName()
    {
        return $this->categoryName;
    }

    /**
     * Set sectionCode
     *
     * @param integer $sectionCode
     */
    public function setSectionCode($sectionCode)
    {
        $this->sectionCode = $sectionCode;
    }

    /**
     * Get sectionCode
     *
     * @return integer
     */
    public function getSectionCode()
    {
        return $this->sectionCode;
    }

    /**
     * Set sectionName
     *
     * @param string $sectionName
     */
    public function setSectionName($sectionName)
    {
        $this->sectionName = $sectionName;
    }

    /**
     * Get sectionName
     *
     * @return string
     */
    public function getSectionName()
    {
        return $this->sectionName;
    }

    /**
     * Set subsectionCode
     *
     * @param integer $subsectionCode
     */
    public function setSubsectionCode($subsectionCode)
    {
        $this->subsectionCode = $subsectionCode;
    }

    /**
     * Get subsectionCode
     *
     * @return integer
     */
    public function getSubsectionCode()
    {
        return $this->subsectionCode;
    }

    /**
     * Set subsectionName
     *
     * @param string $subsectionName
     */
    public function setSubsectionName($subsectionName)
    {
        $this->subsectionName = $subsectionName;
    }

    /**
     * Get subsectionName
     *
     * @return string
     */
    public function getSubsectionName()
    {
        return $this->subsectionName;
    }

    /**
     * Set paragraphCode
     *
     * @param integer $paragraphCode
     */
    public function setParagraphCode($paragraphCode)
    {
        $this->paragraphCode = $paragraphCode;
    }

    /**
     * Get paragraphCode
     *
     * @return integer
     */
    public function getParagraphCode()
    {
        return $this->paragraphCode;
    }

    /**
     * Set paragraphName
     *
     * @param string $paragraphName
     */
    public function setParagraphName($paragraphName)
    {
        $this->paragraphName = $paragraphName;
    }

    /**
     * Get paragraphName
     *
     * @return string
     */
    public function getParagraphName()
    {
        return $this->paragraphName;
    }



    /**
     * Add budgetItemKrajSummaries
     *
     * @param PublicBudget\FrontendBundle\Entity\BudgetItemKrajSummary $budgetItemKrajSummaries
     */
    public function addBudgetItemKrajSummary(\PublicBudget\FrontendBundle\Entity\BudgetItemKrajSummary $budgetItemKrajSummaries)
    {
        $this->budgetItemKrajSummaries[] = $budgetItemKrajSummaries;
    }

    /**
     * Get budgetItemKrajSummaries
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getBudgetItemKrajSummaries()
    {
        return $this->budgetItemKrajSummaries;
    }




}
<?php

namespace PublicBudget\FrontendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PublicBudget\FrontendBundle\Entity\BudgetItemKrajSummary
 *
 * @ORM\Table(name="budget_item_kraj_summary")
 * @ORM\Entity
 */
class BudgetItemKrajSummary
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="SectorClassification", inversedBy="budgetItems")
     * @ORM\JoinColumn(name="sector_classification_id", referencedColumnName="id")
     */
    private $sectorClassification;

    /**
     * @ORM\ManyToOne(targetEntity="TypeClassification", inversedBy="budgetItems")
     * @ORM\JoinColumn(name="type_classification_id", referencedColumnName="id")
     */
    private $typeClassification;

    /**
     * @ORM\ManyToOne(targetEntity="Kraj", inversedBy="budgetItems")
     * @ORM\JoinColumn(name="kraj_id", referencedColumnName="id")
     */
    private $kraj;

    /**
     * @ORM\ManyToOne(targetEntity="BudgetItemType", inversedBy="budgetItems")
     * @ORM\JoinColumn(name="budget_item_type_id", referencedColumnName="id")
     */
    private $type;

    /**
     * @var decimal $valueApproved
     *
     * @ORM\Column(name="value_approved", type="decimal", precision=14, scale=2)
     */
    private $valueApproved;

    /**
     * @var decimal $valueAfterChanges
     *
     * @ORM\Column(name="value_after_changes", type="decimal", precision=14, scale=2)
     */
    private $valueAfterChanges;

    /**
     * @var decimal $valueFinal
     *
     * @ORM\Column(name="value_final", type="decimal", precision=14, scale=2)
     */
    private $valueFinal;

    /**
     * @var integer $year
     *
     * @ORM\Column(name="year", type="integer")
     */
    private $year;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set value
     *
     * @param decimal $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * Get value
     *
     * @return decimal
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set budgetItemType
     *
     * @param PublicBudget\FrontendBundle\Entity\BudgetItemType $budgetItemType
     */
    public function setBudgetItemType(\PublicBudget\FrontendBundle\Entity\BudgetItemType $budgetItemType)
    {
        $this->budgetItemType = $budgetItemType;
    }

    /**
     * Get budgetItemType
     *
     * @return PublicBudget\FrontendBundle\Entity\BudgetItemType
     */
    public function getBudgetItemType()
    {
        return $this->budgetItemType;
    }

    /**
     * Set valueApproved
     *
     * @param decimal $valueApproved
     */
    public function setValueApproved($valueApproved)
    {
        $this->valueApproved = $valueApproved;
    }

    /**
     * Get valueApproved
     *
     * @return decimal
     */
    public function getValueApproved()
    {
        return $this->valueApproved;
    }

    /**
     * Set valueAfterChanges
     *
     * @param decimal $valueAfterChanges
     */
    public function setValueAfterChanges($valueAfterChanges)
    {
        $this->valueAfterChanges = $valueAfterChanges;
    }

    /**
     * Get valueAfterChanges
     *
     * @return decimal
     */
    public function getValueAfterChanges()
    {
        return $this->valueAfterChanges;
    }

    /**
     * Set valueFinal
     *
     * @param decimal $valueFinal
     */
    public function setValueFinal($valueFinal)
    {
        $this->valueFinal = $valueFinal;
    }

    /**
     * Get valueFinal
     *
     * @return decimal
     */
    public function getValueFinal()
    {
        return $this->valueFinal;
    }

    /**
     * Set year
     *
     * @param integer $year
     */
    public function setYear($year)
    {
        $this->year = $year;
    }

    /**
     * Get year
     *
     * @return integer
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set sectorClassification
     *
     * @param PublicBudget\FrontendBundle\Entity\SectorClassification $sectorClassification
     */
    public function setSectorClassification(\PublicBudget\FrontendBundle\Entity\SectorClassification $sectorClassification)
    {
        $this->sectorClassification = $sectorClassification;
    }

    /**
     * Get sectorClassification
     *
     * @return PublicBudget\FrontendBundle\Entity\SectorClassification
     */
    public function getSectorClassification()
    {
        return $this->sectorClassification;
    }

    /**
     * Set typeClassification
     *
     * @param PublicBudget\FrontendBundle\Entity\TypeClassification $typeClassification
     */
    public function setTypeClassification(\PublicBudget\FrontendBundle\Entity\TypeClassification $typeClassification)
    {
        $this->typeClassification = $typeClassification;
    }

    /**
     * Get typeClassification
     *
     * @return PublicBudget\FrontendBundle\Entity\TypeClassification
     */
    public function getTypeClassification()
    {
        return $this->typeClassification;
    }

    /**
     * Set kraj
     *
     * @param PublicBudget\FrontendBundle\Entity\Kraj $kraj
     */
    public function setKraj(\PublicBudget\FrontendBundle\Entity\Kraj $kraj)
    {
        $this->kraj = $kraj;
    }

    /**
     * Get kraj
     *
     * @return PublicBudget\FrontendBundle\Entity\Kraj
     */
    public function getKraj()
    {
        return $this->kraj;
    }

    /**
     * Set type
     *
     * @param PublicBudget\FrontendBundle\Entity\BudgetItemType $type
     */
    public function setType(\PublicBudget\FrontendBundle\Entity\BudgetItemType $type)
    {
        $this->type = $type;
    }

    /**
     * Get type
     *
     * @return PublicBudget\FrontendBundle\Entity\BudgetItemType
     */
    public function getType()
    {
        return $this->type;
    }
}
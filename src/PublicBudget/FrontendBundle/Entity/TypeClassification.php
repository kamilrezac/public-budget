<?php

namespace PublicBudget\FrontendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * PublicBudget\FrontendBundle\Entity\TypeClassification
 *
 * @ORM\Table(name="type_classification")
 * @ORM\Entity
 */
class TypeClassification
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer $classCode
     *
     * @ORM\Column(name="class_code", type="integer")
     */
    private $classCode;

    /**
     * @var string $className
     *
     * @ORM\Column(name="class_name", type="string", length=255)
     */
    private $className;

    /**
     * @var integer $groupCode
     *
     * @ORM\Column(name="group_code", type="integer")
     */
    private $groupCode;

    /**
     * @var string $groupName
     *
     * @ORM\Column(name="group_name", type="string", length=255)
     */
    private $groupName;

    /**
     * @var integer $subgroupCode
     *
     * @ORM\Column(name="subgroup_code", type="integer")
     */
    private $subgroupCode;

    /**
     * @var string $subgroupName
     *
     * @ORM\Column(name="subgroup_name", type="string", length=255)
     */
    private $subgroupName;

    /**
     * @var integer $itemCode
     *
     * @ORM\Column(name="item_code", type="integer")
     */
    private $itemCode;

    /**
     * @var string $itemName
     *
     * @ORM\Column(name="item_name", type="string", length=255)
     */
    private $itemName;

    /**
     * @ORM\OneToMany(targetEntity="BudgetItem", mappedBy="typeClassification")
     */
    private $budgetItems;

    /**
     * @ORM\OneToMany(targetEntity="BudgetItemKrajSummary", mappedBy="typeClassification")
     */
    private $budgetItemKrajSummaries;

    public function __construct()
    {
        $this->budgetItems = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Set classCode
     *
     * @param integer $classCode
     */
    public function setClassCode($classCode)
    {
        $this->classCode = $classCode;
    }

    /**
     * Get classCode
     *
     * @return integer
     */
    public function getClassCode()
    {
        return $this->classCode;
    }

    /**
     * Set className
     *
     * @param string $className
     */
    public function setClassName($className)
    {
        $this->className = $className;
    }

    /**
     * Get className
     *
     * @return string
     */
    public function getClassName()
    {
        return $this->className;
    }

    /**
     * Set groupCode
     *
     * @param integer $groupCode
     */
    public function setGroupCode($groupCode)
    {
        $this->groupCode = $groupCode;
    }

    /**
     * Get groupCode
     *
     * @return integer
     */
    public function getGroupCode()
    {
        return $this->groupCode;
    }

    /**
     * Set groupName
     *
     * @param string $groupName
     */
    public function setGroupName($groupName)
    {
        $this->groupName = $groupName;
    }

    /**
     * Get groupName
     *
     * @return string
     */
    public function getGroupName()
    {
        return $this->groupName;
    }

    /**
     * Set subgroupCode
     *
     * @param integer $subgroupCode
     */
    public function setSubgroupCode($subgroupCode)
    {
        $this->subgroupCode = $subgroupCode;
    }

    /**
     * Get subgroupCode
     *
     * @return integer
     */
    public function getSubgroupCode()
    {
        return $this->subgroupCode;
    }

    /**
     * Set subgroupName
     *
     * @param string $subgroupName
     */
    public function setSubgroupName($subgroupName)
    {
        $this->subgroupName = $subgroupName;
    }

    /**
     * Get subgroupName
     *
     * @return string
     */
    public function getSubgroupName()
    {
        return $this->subgroupName;
    }

    /**
     * Set itemCode
     *
     * @param integer $itemCode
     */
    public function setItemCode($itemCode)
    {
        $this->itemCode = $itemCode;
    }

    /**
     * Get itemCode
     *
     * @return integer
     */
    public function getItemCode()
    {
        return $this->itemCode;
    }

    /**
     * Set itemName
     *
     * @param string $itemName
     */
    public function setItemName($itemName)
    {
        $this->itemName = $itemName;
    }

    /**
     * Get itemName
     *
     * @return string
     */
    public function getItemName()
    {
        return $this->itemName;
    }

    /**
     * Add budgetItems
     *
     * @param PublicBudget\FrontendBundle\Entity\BudgetItem $budgetItems
     */
    public function addBudgetItem(\PublicBudget\FrontendBundle\Entity\BudgetItem $budgetItems)
    {
        $this->budgetItems[] = $budgetItems;
    }

    /**
     * Get budgetItems
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getBudgetItems()
    {
        return $this->budgetItems;
    }

    /**
     * Add budgetItemKrajSummaries
     *
     * @param PublicBudget\FrontendBundle\Entity\BudgetItemKrajSummary $budgetItemKrajSummaries
     */
    public function addBudgetItemKrajSummary(\PublicBudget\FrontendBundle\Entity\BudgetItemKrajSummary $budgetItemKrajSummaries)
    {
        $this->budgetItemKrajSummaries[] = $budgetItemKrajSummaries;
    }

    /**
     * Get budgetItemKrajSummaries
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getBudgetItemKrajSummaries()
    {
        return $this->budgetItemKrajSummaries;
    }

    /**
     * Remove budgetItems
     *
     * @param \PublicBudget\FrontendBundle\Entity\BudgetItem $budgetItems
     */
    public function removeBudgetItem(\PublicBudget\FrontendBundle\Entity\BudgetItem $budgetItems)
    {
        $this->budgetItems->removeElement($budgetItems);
    }

    /**
     * Add budgetItemKrajSummaries
     *
     * @param \PublicBudget\FrontendBundle\Entity\BudgetItemKrajSummary $budgetItemKrajSummaries
     * @return TypeClassification
     */
    public function addBudgetItemKrajSummarie(\PublicBudget\FrontendBundle\Entity\BudgetItemKrajSummary $budgetItemKrajSummaries)
    {
        $this->budgetItemKrajSummaries[] = $budgetItemKrajSummaries;

        return $this;
    }

    /**
     * Remove budgetItemKrajSummaries
     *
     * @param \PublicBudget\FrontendBundle\Entity\BudgetItemKrajSummary $budgetItemKrajSummaries
     */
    public function removeBudgetItemKrajSummarie(\PublicBudget\FrontendBundle\Entity\BudgetItemKrajSummary $budgetItemKrajSummaries)
    {
        $this->budgetItemKrajSummaries->removeElement($budgetItemKrajSummaries);
    }
}
<?php

namespace PublicBudget\FrontendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PublicBudget\FrontendBundle\Entity\BudgetMonitoringInformation
 *
 * @ORM\Table(name="budget_monitoring_information")
 * @ORM\Entity
 */
class BudgetMonitoringInformation
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Municipality", inversedBy="BudgetMonitoringInformation")
     * @ORM\JoinColumn(name="municipality_id", referencedColumnName="id")
     */
    private $municipality;

    /**
     * @var integer $year
     *
     * @ORM\Column(name="year", type="integer")
     */
    private $year;

    /**
     * @var integer $population
     *
     * @ORM\Column(name="population", type="integer")
     */
    // private $population;

    /**
     * @var decimal $value
     *
     * @ORM\Column(name="total_income", type="decimal", precision=14, scale=2)
     */
    // private $totalIncome;

    /**
     * @var decimal $value
     *
     * @ORM\Column(name="interests", type="decimal", precision=14, scale=2)
     */
    // private $interests;

    /**
     * @var decimal $value
     *
     * @ORM\Column(name="paid_debts", type="decimal", precision=14, scale=2)
     */
    // private $paidDebts;

    /**
     * @var decimal $value
     *
     * @ORM\Column(name="total_debt_service", type="decimal", precision=14, scale=2)
     */
    // private $totalDebtService;

    /**
     * @var decimal $value
     *
     * @ORM\Column(name="debt_service_pointer", type="decimal", precision=5, scale=4)
     */
    // private $debtServicePointer;

    /**
     * @var decimal $value
     *
     * @ORM\Column(name="total_assets", type="decimal", precision=14, scale=2)
     */
    // private $totalAssets;

    /**
     * @var decimal $value
     *
     * @ORM\Column(name="liabilities", type="decimal", precision=14, scale=2)
     */
    // private $liabilities;

    /**
     * @var decimal $value
     *
     * @ORM\Column(name="total_money_on_bank_accounts", type="decimal", precision=14, scale=2)
     */
    // private $totalMoneyOnBankAccounts;

    /**
     * @var decimal $value
     *
     * @ORM\Column(name="loans_and_municipal_bonds", type="decimal", precision=14, scale=2)
     */
    // private $loansAndMunicipalBonds;

    /**
     * @var decimal $value
     *
     * @ORM\Column(name="received_repayable_financial_aid_and_other_debts", type="decimal", precision=14, scale=2)
     */
    // private $receivedRepayableFinancialAidAndOtherDebts;

    /**
     * @var decimal $value
     *
     * @ORM\Column(name="total_debt", type="decimal", precision=14, scale=2)
     */
    private $totalDebt;

    /**
     * @var decimal $value
     *
     * @ORM\Column(name="share_of_liabilities_to_total_assets", type="decimal", precision=5, scale=4)
     */
    // private $shareOfLiabilitiesToTotalAssets;

    /**
     * @var decimal $value
     *
     * @ORM\Column(name="share_of_debt_to_liabilities", type="decimal", precision=5, scale=4)
     */
    // private $shareOfDebtToLiabilities;

    /**
     * @var decimal $value
     *
     * @ORM\Column(name="liabilities_per_inhabitant", type="decimal", precision=14, scale=2)
     */
    // private $liabilitiesPerInhabitant;

    /**
     * @var decimal $value
     *
     * @ORM\Column(name="current_assets", type="decimal", precision=14, scale=2)
     */
    // private $currentAssets;

    /**
     * @var decimal $value
     *
     * @ORM\Column(name="short_term_liabilities", type="decimal", precision=14, scale=2)
     */
    // private $shortTermLiabilities;

    /**
     * @var decimal $value
     *
     * @ORM\Column(name="total_liquidity", type="decimal", precision=3, scale=2)
     */
    // private $totalLiquidity;

    /**
     * Set year
     *
     * @param integer $year
     */
    public function setYear($year)
    {
        $this->year = $year;
    }

    /**
     * Get year
     *
     * @return integer
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set totalDebt
     *
     * @param decimal $totalDebt
     */
    public function setTotalDebt($totalDebt)
    {
        $this->totalDebt = $totalDebt;
    }

    /**
     * Get totalDebt
     *
     * @return decimal
     */
    public function getTotalDebt()
    {
        return $this->totalDebt;
    }

    /**
     * Set municipality
     *
     * @param PublicBudget\FrontendBundle\Entity\Municipality $municipality
     */
    public function setMunicipality(\PublicBudget\FrontendBundle\Entity\Municipality $municipality)
    {
        $this->municipality = $municipality;
    }

    /**
     * Get municipality
     *
     * @return PublicBudget\FrontendBundle\Entity\Municipality
     */
    public function getMunicipality()
    {
        return $this->municipality;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}
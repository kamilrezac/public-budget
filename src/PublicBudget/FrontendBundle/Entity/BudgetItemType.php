<?php

namespace PublicBudget\FrontendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * PublicBudget\FrontendBundle\Entity\BudgetItemType
 *
 * @ORM\Table(name="budget_item_type")
 * @ORM\Entity
 */
class BudgetItemType
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="BudgetItem", mappedBy="type")
     */
    private $budgetItems;

    /**
     * @ORM\OneToMany(targetEntity="BudgetItemKrajSummary", mappedBy="budgetItemType")
     */
    private $budgetItemKrajSummaries;

    public function __construct()
    {
        $this->budgetItems = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add budgetItems
     *
     * @param PublicBudget\FrontendBundle\Entity\BudgetItem $budgetItems
     */
    public function addBudgetItem(\PublicBudget\FrontendBundle\Entity\BudgetItem $budgetItems)
    {
        $this->budgetItems[] = $budgetItems;
    }

    /**
     * Get budgetItems
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getBudgetItems()
    {
        return $this->budgetItems;
    }

    /**
     * Add budgetItemKrajSummaries
     *
     * @param PublicBudget\FrontendBundle\Entity\BudgetItemKrajSummary $budgetItemKrajSummaries
     */
    public function addBudgetItemKrajSummary(\PublicBudget\FrontendBundle\Entity\BudgetItemKrajSummary $budgetItemKrajSummaries)
    {
        $this->budgetItemKrajSummaries[] = $budgetItemKrajSummaries;
    }

    /**
     * Get budgetItemKrajSummaries
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getBudgetItemKrajSummaries()
    {
        return $this->budgetItemKrajSummaries;
    }

    /**
     * Remove budgetItems
     *
     * @param \PublicBudget\FrontendBundle\Entity\BudgetItem $budgetItems
     */
    public function removeBudgetItem(\PublicBudget\FrontendBundle\Entity\BudgetItem $budgetItems)
    {
        $this->budgetItems->removeElement($budgetItems);
    }

    /**
     * Add budgetItemKrajSummaries
     *
     * @param \PublicBudget\FrontendBundle\Entity\BudgetItemKrajSummary $budgetItemKrajSummaries
     * @return BudgetItemType
     */
    public function addBudgetItemKrajSummarie(\PublicBudget\FrontendBundle\Entity\BudgetItemKrajSummary $budgetItemKrajSummaries)
    {
        $this->budgetItemKrajSummaries[] = $budgetItemKrajSummaries;

        return $this;
    }

    /**
     * Remove budgetItemKrajSummaries
     *
     * @param \PublicBudget\FrontendBundle\Entity\BudgetItemKrajSummary $budgetItemKrajSummaries
     */
    public function removeBudgetItemKrajSummarie(\PublicBudget\FrontendBundle\Entity\BudgetItemKrajSummary $budgetItemKrajSummaries)
    {
        $this->budgetItemKrajSummaries->removeElement($budgetItemKrajSummaries);
    }
}
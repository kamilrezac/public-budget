<?php

namespace PublicBudget\FrontendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PublicBudget\FrontendBundle\Entity\BudgetItem
 *
 * @ORM\Table(name="budget_item")
 * @ORM\Entity(repositoryClass="PublicBudget\FrontendBundle\Repository\BudgetItemRepository")
 */
class BudgetItem
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="SectorClassification", inversedBy="budgetItems")
     * @ORM\Id
     * @ORM\JoinColumn(name="sector_classification_id", referencedColumnName="id")
     */
    private $sectorClassification;

    /**
     * @ORM\ManyToOne(targetEntity="TypeClassification", inversedBy="budgetItems")
     * @ORM\Id
     * @ORM\JoinColumn(name="type_classification_id", referencedColumnName="id")
     */
    private $typeClassification;

    /**
     * @ORM\ManyToOne(targetEntity="Municipality", inversedBy="budgetItems")
     * @ORM\Id
     * @ORM\JoinColumn(name="municipality_id", referencedColumnName="id")
     */
    private $municipality;

    /**
     * @ORM\ManyToOne(targetEntity="BudgetItemType", inversedBy="budgetItems")
     * @ORM\JoinColumn(name="budget_item_type_id", referencedColumnName="id")
     */
    private $type;

    /**
     * @var decimal $valueApproved
     *
     * @ORM\Column(name="value_approved", type="decimal", precision=14, scale=2)
     */
    private $valueApproved;

    /**
     * @var decimal $valueAfterChanges
     *
     * @ORM\Column(name="value_after_changes", type="decimal", precision=14, scale=2)
     */
    private $valueAfterChanges;

    /**
     * @var decimal $valueFinal
     *
     * @ORM\Column(name="value_final", type="decimal", precision=14, scale=2)
     */
    private $valueFinal;

    /**
     * @var integer $year
     * @ORM\Id
     * @ORM\Column(name="year", type="integer")
     */
    private $year;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Set valueApproved
     *
     * @param decimal $valueApproved
     */
    public function setValueApproved($valueApproved)
    {
        $this->valueApproved = $valueApproved;
    }

    /**
     * Get valueApproved
     *
     * @return decimal
     */
    public function getValueApproved()
    {
        return $this->valueApproved;
    }

    /**
     * Set valueAfterChange
     *
     * @param decimal $valueAfterChange
     */
    public function setValueAfterChange($valueAfterChange)
    {
        $this->valueAfterChange = $valueAfterChange;
    }

    /**
     * Get valueAfterChange
     *
     * @return decimal
     */
    public function getValueAfterChange()
    {
        return $this->valueAfterChange;
    }

    /**
     * Set valueFinal
     *
     * @param decimal $valueFinal
     */
    public function setValueFinal($valueFinal)
    {
        $this->valueFinal = $valueFinal;
    }

    /**
     * Get valueFinal
     *
     * @return decimal
     */
    public function getValueFinal()
    {
        return $this->valueFinal;
    }

    /**
     * Set year
     *
     * @param integer $year
     */
    public function setYear($year)
    {
        $this->year = $year;
    }

    /**
     * Get year
     *
     * @return integer
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set month
     *
     * @param integer $month
     */
    public function setMonth($month)
    {
        $this->month = $month;
    }

    /**
     * Get month
     *
     * @return integer
     */
    public function getMonth()
    {
        return $this->month;
    }

    /**
     * Set quarter
     *
     * @param integer $quarter
     */
    public function setQuarter($quarter)
    {
        $this->quarter = $quarter;
    }

    /**
     * Get quarter
     *
     * @return integer
     */
    public function getQuarter()
    {
        return $this->quarter;
    }

    /**
     * Set sectorClassification
     *
     * @param PublicBudget\FrontendBundle\Entity\SectorClassification $sectorClassification
     */
    public function setSectorClassification(\PublicBudget\FrontendBundle\Entity\SectorClassification $sectorClassification)
    {
        $this->sectorClassification = $sectorClassification;
    }

    /**
     * Get sectorClassification
     *
     * @return PublicBudget\FrontendBundle\Entity\SectorClassification
     */
    public function getSectorClassification()
    {
        return $this->sectorClassification;
    }

    /**
     * Set typeClassification
     *
     * @param PublicBudget\FrontendBundle\Entity\TypeClassification $typeClassification
     */
    public function setTypeClassification(\PublicBudget\FrontendBundle\Entity\TypeClassification $typeClassification)
    {
        $this->typeClassification = $typeClassification;
    }

    /**
     * Get typeClassification
     *
     * @return PublicBudget\FrontendBundle\Entity\TypeClassification
     */
    public function getTypeClassification()
    {
        return $this->typeClassification;
    }

    /**
     * Set municipality
     *
     * @param PublicBudget\FrontendBundle\Entity\Municipality $municipality
     */
    public function setMunicipality(\PublicBudget\FrontendBundle\Entity\Municipality $municipality)
    {
        $this->municipality = $municipality;
    }

    /**
     * Get municipality
     *
     * @return PublicBudget\FrontendBundle\Entity\Municipality
     */
    public function getMunicipality()
    {
        return $this->municipality;
    }

    /**
     * Set type
     *
     * @param PublicBudget\FrontendBundle\Entity\BudgetItemType $type
     */
    public function setType(\PublicBudget\FrontendBundle\Entity\BudgetItemType $type)
    {
        $this->type = $type;
    }

    /**
     * Get type
     *
     * @return PublicBudget\FrontendBundle\Entity\BudgetItemType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set valueAfterChanges
     *
     * @param decimal $valueAfterChanges
     */
    public function setValueAfterChanges($valueAfterChanges)
    {
        $this->valueAfterChanges = $valueAfterChanges;
    }

    /**
     * Get valueAfterChanges
     *
     * @return decimal
     */
    public function getValueAfterChanges()
    {
        return $this->valueAfterChanges;
    }
}
<?php

namespace PublicBudget\FrontendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PublicBudget\FrontendBundle\Entity\PopulationRange
 *
 * @ORM\Table(name="population_range")
 * @ORM\Entity(repositoryClass="PublicBudget\FrontendBundle\Repository\PopulationRangeRepository")
 */
class PopulationRange
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer $min
     *
     * @ORM\Column(name="min", type="integer")
     */
    private $min;

    /**
     * @var integer $max
     *
     * @ORM\Column(name="max", type="integer")
     */
    private $max;

    public function __toString()
    {
        return $this->min . ' - ' . $this->max;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set min
     *
     * @param integer $min
     * @return PopulationRange
     */
    public function setMin($min)
    {
        $this->min = $min;

        return $this;
    }

    /**
     * Get min
     *
     * @return integer
     */
    public function getMin()
    {
        return $this->min;
    }

    /**
     * Set max
     *
     * @param integer $max
     * @return PopulationRange
     */
    public function setMax($max)
    {
        $this->max = $max;

        return $this;
    }

    /**
     * Get max
     *
     * @return integer
     */
    public function getMax()
    {
        return $this->max;
    }
}
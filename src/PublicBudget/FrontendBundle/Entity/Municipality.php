<?php

namespace PublicBudget\FrontendBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection as ArrayCollection;

/**
 * PublicBudget\FrontendBundle\Entity\Municipality
 *
 * @ORM\Table(name="municipality")
 * @ORM\Entity(repositoryClass="PublicBudget\FrontendBundle\Repository\MunicipalityRepository")
 */
class Municipality
{
    const COMPETENCE_ORP = 'orp';
    const COMPETENCE_POU = 'pou';

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @Gedmo\Slug(separator="-", updatable=true, unique=true, fields={"csuCode", "name"})
     * @ORM\Column(name="slug", type="string", length=255)
     */
    private $slug;

    /**
     * @var integer $csuCode
     *
     * @ORM\Column(name="csu_code", type="integer")
     */
    private $csuCode;

    /**
     * @var string $organizationName
     *
     * @ORM\Column(name="organization_name", type="string", length=255)
     */
    private $organizationName;

    /**
     * @var string $organizationStreet
     *
     * @ORM\Column(name="organization_street", type="string", length=255)
     */
    private $organizationStreet;

    /**
     * @var integer $organizationZip
     *
     * @ORM\Column(name="organization_zip", type="string", length=5)
     */
    private $organizationZip;

    /**
     * @var string $organizationIco
     *
     * @ORM\Column(name="organization_ico", type="string", length=8)
     */
    private $organizationIco;

    /**
     * @var string $wikiPage
     *
     * @ORM\Column(name="wiki_page", type="string", length=255, nullable=true)
     */
    private $wikiPage;

    /**
     * @var string $webPage
     *
     * @ORM\Column(name="web_page", type="string", length=255, nullable=true)
     */
    private $webPage;

    /**
     * @var string $email
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var string $longitude
     *
     * @ORM\Column(name="longitude", type="decimal", precision=8, scale=6, nullable=true)
     */
    private $longitude;

   /**
    * @var string $latitude
    *
    * @ORM\Column(name="latitude", type="decimal", precision=8, scale=6, nullable=true)
    */
    private $latitude;

    /**
    * @var string $competence
    *
    * @ORM\Column(name="competence", type="string", length=3, nullable=true)
    */
    private $competence;

    /**
     * @ORM\ManyToOne(targetEntity="Kraj", inversedBy="municipalities")
     * @ORM\JoinColumn(name="kraj_id", referencedColumnName="id")
     */
    private $kraj;

    /**
     * @ORM\ManyToOne(targetEntity="Okres", inversedBy="municipalities")
     * @ORM\JoinColumn(name="okres_id", referencedColumnName="id")
     */
    private $okres;

    /**
     * @ORM\OneToMany(targetEntity="Population", mappedBy="municipality")
     */
    private $population;

    public function __construct()
    {
        $this->budgetItems = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getUniqueName();
    }

    public function getUniqueName()
    {
        return $this->getName() . ' (okres ' . $this->getOkres()->getName() . ')';
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set csu_code
     *
     * @param integer $csuCode
     */
    public function setCsuCode($csuCode)
    {
        $this->csuCode = $csuCode;
    }

    /**
     * Get csu_code
     *
     * @return integer
     */
    public function getCsuCode()
    {
        return $this->csuCode;
    }

    /**
     * Set organization_name
     *
     * @param string $organizationName
     */
    public function setOrganizationName($organizationName)
    {
        $this->organizationName = $organizationName;
    }

    /**
     * Get organization_name
     *
     * @return string
     */
    public function getOrganizationName()
    {
        return $this->organizationName;
    }

    /**
     * Set organization_street
     *
     * @param string $organizationStreet
     */
    public function setOrganizationStreet($organizationStreet)
    {
        $this->organizationStreet = $organizationStreet;
    }

    /**
     * Get organization_street
     *
     * @return string
     */
    public function getOrganizationStreet()
    {
        return $this->organizationStreet;
    }

    /**
     * Set organization_zip
     *
     * @param integer $organizationZip
     */
    public function setOrganizationZip($organizationZip)
    {
        $this->organizationZip = $organizationZip;
    }

    /**
     * Get organization_zip
     *
     * @return integer
     */
    public function getOrganizationZip()
    {
        return $this->organizationZip;
    }

    /**
     * Set organization_ico
     *
     * @param integer $organizationIco
     */
    public function setOrganizationIco($organizationIco)
    {
        $this->organizationIco = $organizationIco;
    }

    /**
     * Get organization_ico
     *
     * @return integer
     */
    public function getOrganizationIco()
    {
        return $this->organizationIco;
    }

    /**
     * Set wiki_page
     *
     * @param string $wikiPage
     */
    public function setWikiPage($wikiPage)
    {
        $this->wikiPage = $wikiPage;
    }

    /**
     * Get wiki_page
     *
     * @return string
     */
    public function getWikiPage()
    {
        return $this->wikiPage;
    }

    /**
     * Set web_page
     *
     * @param string $webPage
     */
    public function setWebPage($webPage)
    {
        $this->webPage = $webPage;
    }

    /**
     * Get web_page
     *
     * @return string
     */
    public function getWebPage()
    {
        return $this->webPage;
    }

    /**
     * Set kraj
     *
     * @param PublicBudget\FrontendBundle\Entity\Kraj $kraj
     */
    public function setKraj(\PublicBudget\FrontendBundle\Entity\Kraj $kraj)
    {
        $this->kraj = $kraj;
    }

    /**
     * Get kraj
     *
     * @return PublicBudget\FrontendBundle\Entity\Kraj
     */
    public function getKraj()
    {
        return $this->kraj;
    }

    /**
     * Set okres
     *
     * @param PublicBudget\FrontendBundle\Entity\Okres $okres
     */
    public function setOkres(\PublicBudget\FrontendBundle\Entity\Okres $okres)
    {
        $this->okres = $okres;
    }

    /**
     * Get okres
     *
     * @return PublicBudget\FrontendBundle\Entity\Okres
     */
    public function getOkres()
    {
        return $this->okres;
    }

    /**
     * Set slug
     *
     * @param string $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set longitude
     *
     * @param decimal $longitude
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
    }

    /**
     * Get longitude
     *
     * @return decimal
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set latitude
     *
     * @param decimal $latitude
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    }

    /**
     * Get latitude
     *
     * @return decimal
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set email
     *
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Add population
     *
     * @param PublicBudget\FrontendBundle\Entity\Population $population
     */
    public function addPopulation(\PublicBudget\FrontendBundle\Entity\Population $population)
    {
        $this->population[] = $population;
    }

    /**
     * Get population
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getPopulation()
    {
        return $this->population;
    }

    public function getPopulationFor($year)
    {
        foreach ($this->population as $population) {
            if ($population->getYear() == $year) {
                return $population->getCount();
            }
        }

        return 0;
    }

    public function getPopulationRangeFor($year)
    {
        foreach ($this->population as $population) {
            $populationRange = $population->getPopulationRange();
            if ($population->getYear() == $year) {
                return $populationRange;
            }
        }

        return $populationRange;
    }

    /**
     * Set competence
     *
     * @param string $competence
     */
    public function setCompetence($competence)
    {
        if (!in_array($competence, array(self::COMPETENCE_ORP, self::COMPETENCE_POU))) {
            throw new \InvalidArgumentException("Invalid competence");
        }
        $this->competence = $competence;
    }

    /**
     * Get competence
     *
     * @return string
     */
    public function getCompetence()
    {
        return $this->competence;
    }


    /**
     * Remove population
     *
     * @param \PublicBudget\FrontendBundle\Entity\Population $population
     */
    public function removePopulation(\PublicBudget\FrontendBundle\Entity\Population $population)
    {
        $this->population->removeElement($population);
    }
}
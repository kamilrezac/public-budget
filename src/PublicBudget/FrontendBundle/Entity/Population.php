<?php

namespace PublicBudget\FrontendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PublicBudget\FrontendBundle\Entity\Population
 *
 * @ORM\Table(name="population")
 * @ORM\Entity(repositoryClass="PublicBudget\FrontendBundle\Repository\PopulationRepository")
 */
class Population
{
    /**
     * @ORM\ManyToOne(targetEntity="Municipality", inversedBy="population")
     * @ORM\Id
     * @ORM\JoinColumn(name="municipality_id", referencedColumnName="id")
     */
    private $municipality;

    /**
     * @ORM\ManyToOne(targetEntity="PopulationRange")
     * @ORM\JoinColumn(name="population_range_id", referencedColumnName="id")
     */
    private $populationRange;

    /**
     * @var integer $year
     * @ORM\Id
     * @ORM\Column(name="year", type="integer")
     */
    private $year;

    /**
     * @var integer $count
     *
     * @ORM\Column(name="count", type="integer")
     */
    private $count;

    /**
     * Set year
     *
     * @param integer $year
     */
    public function setYear($year)
    {
        $this->year = $year;
    }

    /**
     * Get year
     *
     * @return integer
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set count
     *
     * @param integer $count
     */
    public function setCount($count)
    {
        $this->count = $count;
    }

    /**
     * Get count
     *
     * @return integer
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * Set municipality
     *
     * @param PublicBudget\FrontendBundle\Entity\Municipality $municipality
     */
    public function setMunicipality(\PublicBudget\FrontendBundle\Entity\Municipality $municipality)
    {
        $this->municipality = $municipality;
    }

    /**
     * Get municipality
     *
     * @return PublicBudget\FrontendBundle\Entity\Municipality
     */
    public function getMunicipality()
    {
        return $this->municipality;
    }

    /**
     * Set populationRange
     *
     * @param \PublicBudget\FrontendBundle\Entity\PopulationRange $populationRange
     * @return Population
     */
    public function setPopulationRange(\PublicBudget\FrontendBundle\Entity\PopulationRange $populationRange = null)
    {
        $this->populationRange = $populationRange;
    
        return $this;
    }

    /**
     * Get populationRange
     *
     * @return \PublicBudget\FrontendBundle\Entity\PopulationRange 
     */
    public function getPopulationRange()
    {
        return $this->populationRange;
    }
}
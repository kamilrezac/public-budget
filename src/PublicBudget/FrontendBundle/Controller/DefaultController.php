<?php

namespace PublicBudget\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Response;

use PublicBudget\FrontendBundle\Repository as Repository;


class DefaultController extends Controller
{

    public function indexAction()
    {
        $clientIpLocationFinder = $this->get('client_ip_location_finder');

        $userLocationData = $clientIpLocationFinder->getUserLocationData();

        $municipality = $this->getDoctrine()
        ->getRepository('PublicBudgetFrontendBundle:Municipality')->getMunicipalityByNameAndGeographicLocationFrom($userLocationData);

        return $this->render('PublicBudgetFrontendBundle:Default:index.html.twig', array(
            'municipality' => $municipality
        ));
    }

    public function autocompleteAction()
    {
        $term = $this->get('request')->query->get('term');

        $municipalities = $this->getDoctrine()
        ->getRepository('PublicBudgetFrontendBundle:Municipality')->searchAllBySubstring($term);

        $result = array();
        foreach ($municipalities as $municipality) {
            $result[] = array(
                'value' => (string)$municipality,
                'id' => $municipality->getId()
            );
        }

        return new Response(json_encode($result));
    }

    public function searchAction()
    {
        $term = $this->get('request')->query->get('q');

        $id = $this->get('request')->query->get('id'); 

        if ($id) {
            $municipality = $this->getDoctrine()->getRepository('PublicBudgetFrontendBundle:Municipality')->find($id);
            return $this->redirect($this->get('router')->generate('PublicBudgetFrontendBundle_municipality_timeline', array('slug' => $municipality->getSlug())));
        }

        $municipalities = $this->getDoctrine()
        ->getRepository('PublicBudgetFrontendBundle:Municipality')->searchAllByName($term);

        return $this->render('PublicBudgetFrontendBundle:Default:search.html.twig', array('municipalities' => $municipalities));
    }

    public function staticPageAction($template)
    {
        return $this->render('PublicBudgetFrontendBundle:Default:'.$template.'.html.twig');
    }
}

<?php

namespace PublicBudget\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use PublicBudget\FrontendBundle\Municipality as Municipality;
use PublicBudget\FrontendBundle\Repository as Repository;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

use PublicBudget\FrontendBundle\Municipality\Common\BudgetStructureLogicFactory;

use PublicBudget\FrontendBundle\Form\Data\MunicipalityFilterData;
use PublicBudget\FrontendBundle\Form\Type\MunicipalityFilterType;

use PublicBudget\FrontendBundle\Form\Data\MunicipalityCompareData;
use PublicBudget\FrontendBundle\Form\Type\MunicipalityCompareType;

use PublicBudget\FrontendBundle\Form\Data\MunicipalityTimelineData;
use PublicBudget\FrontendBundle\Form\Type\MunicipalityTimelineType;

use PublicBudget\FrontendBundle\Helper\NumberHelper;
use PublicBudget\FrontendBundle\Municipality\Detail\AdministrationUnitHelperDataFactory;
use PublicBudget\FrontendBundle\Repository\AdministrationUnitCommonRepository;
use PublicBudget\FrontendBundle\Municipality\Detail\AdministrationUnitBudgetDataCollectionAssembler;

use Symfony\Component\Stopwatch\Stopwatch;

class MunicipalityController extends Controller
{
    public function indexAction()
    {
        $letter = $this->get('request')->query->get('pismeno', 'A');
        $municipalities = $this->getDoctrine()
        ->getRepository('PublicBudgetFrontendBundle:Municipality')
        ->findAllByFirstLetter($letter);

        return $this->render('PublicBudgetFrontendBundle:Municipality:index.html.twig', array('municipalities' => $municipalities, 'activeLetter' => $letter));
    }

    public function filterAction($tab)
    {

        $budgetItemSummaryConfigurationRepository = $this->getDoctrine()
        ->getRepository('PublicBudgetFrontendBundle:BudgetItemSummaryConfiguration'); 

        $municipalityFilterData = new MunicipalityFilterData();
        $municipalityFilterData->setBudgetItem($budgetItemSummaryConfigurationRepository->findOneByName('Výdaje'));

        $municipalityFilterForm = $this->get('form.factory')->create(
           $this->get('publicbudget.form.type.municipality_filter'),
           $municipalityFilterData
        );

        $request = $this->get('request');
        $params = $request->query->all();

        if (isset($params['filtr_obci'])) {
            $municipalityFilterForm->bind($request);
            $filterData = $municipalityFilterForm->getData();
        } else {
            $filterData = $municipalityFilterData;
        }

        $titleBuilder = $this->get('municipality_filter_title_builder');
        $title = $titleBuilder->buildFrom($filterData);

        if ($filterData->getBudgetItem()) {
            if ($filterData->getBudgetItem()->getType() == 'summary_expression') {
                $summaryValuesInsertQuery = $this->get('municipality_filter_budget_expression_summary_values_insert_query');
            } else {
                $summaryValuesInsertQuery = $this->get('municipality_filter_budget_item_summary_values_insert_query');
            }
            $summaryValuesInsertQuery->insertBudgetItemSummaryValues($filterData);
            $filterQuery = $this->get('municipality_filter_budget_query');
        } else {
            $filterQuery = $this->get('municipality_filter_basic_query');
        }

        $filterQuery->createQueryFromFilterData($filterData);

        if ($request->query->get('csv_export')) {
            $municipalities = $filterQuery->execute();

            $csvExporterFactory = $this->get('csv_exporter_factory');
            $csvExporter = $csvExporterFactory->getCsvExporter('cz');

            $headerData = array(
                'name' => 'obec',
                'okres_name' => 'okres',
                'kraj_name' => 'kraj',
                'value' => 'hodnota',
                'population_count' => 'počet obyvatel'
            );

            $csvOutput = $csvExporter->smartProcess($headerData, $municipalities, array($title));

            $response = new Response();
            $response->headers->set('Content-Type', "text/csv");
            $response->headers->set('Content-Disposition', 'attachment; filename="obce-export.csv"');
            $response->setContent($csvOutput);
        } else {
            $page = $request->query->get('strana', 1);

            if ($filterData->getBudgetItem()) {
                if ($filterData->getBudgetItem()->getType() == 'summary_expression') {
                    $resultBuilder = $this->get('municipality_filter_budget_expression_result_builder');
                } else {
                    $resultBuilder = $this->get('municipality_filter_budget_item_result_builder');
                }
            } else {
                $resultBuilder = $this->get('municipality_filter_basic_result_builder');
            }

            $data = array(
                'filterData' => $filterData, 
                'filterQuery' => $filterQuery, 
                'page' => $page
            );
            $result = $resultBuilder->getResult($data, $tab);

            $response = $this->render('PublicBudgetFrontendBundle:Municipality:'.$result->template.'.html.twig', array(
                    'page' => $page,
                    'title' => $title,
                    'form' => $municipalityFilterForm->createView(),
                    'result' => $result
            ));
        }

        return $response;
    }

    public function filterTrendAction()
    {
        $budgetItemSummaryConfigurationRepository = $this->getDoctrine()
        ->getRepository('PublicBudgetFrontendBundle:BudgetItemSummaryConfiguration'); 

        $municipalityFilterData = new MunicipalityFilterData();
        $municipalityFilterData->setBudgetItem($budgetItemSummaryConfigurationRepository->findOneByName('Výdaje'));

        $municipalityFilterForm = $this->get('form.factory')->create(
           $this->get('publicbudget.form.type.municipality_filter'),
           $municipalityFilterData
        );

        $request = $this->get('request');
        $params = $request->query->all();

        if (isset($params['filtr_obci'])) {
            $municipalityFilterForm->bind($request);
            $filterData = $municipalityFilterForm->getData();
        }  else {
            $filterData = $municipalityFilterData;
        }

        $titleBuilder = $this->get('municipality_filter_title_builder');
        $title = $titleBuilder->buildFrom($filterData);

        $municipalitiesTrendQuery = $this->get('municipality_filter_municipalities_trend_query');
        $selectedMunicipalityTrendQuery = $this->get('municipality_filter_selected_municipality_trend_query');

        $result = array();

        if ($filterData->getMunicipality()) {
           $result[] = $selectedMunicipalityTrendQuery->getResult($filterData);
        }

        $result[] = $municipalitiesTrendQuery->getResult($filterData);

        return $this->render('PublicBudgetFrontendBundle:Municipality:filter-budget-item-trend.html.twig', array(
            'title' => $title,
            'form' => $municipalityFilterForm->createView(),
            'result' => $result,
            'selectedMunicipality' => $filterData->getMunicipality()
        ));
    }

    public function compareIndexAction()
    {
        $municipalityRepository = $this->getDoctrine()
        ->getRepository('PublicBudgetFrontendBundle:Municipality');

        $request = $this->get('request');

        if ($request->getMethod() == "POST") {
            $params = $request->request->all();
            if (!empty($params['municipality1']) && !empty($params['municipality2'])) {
                $municipality1 = $municipalityRepository->find($params['municipality1']);
                $municipality2 = $municipalityRepository->find($params['municipality2']);

                return $this->redirect(
                    $this->generateUrl('PublicBudgetFrontendBundle_municipality_compare',
                        array(
                            'slug1' => $municipality1->getSlug(),
                            'slug2' => $municipality2->getSlug()
                        )
                    )
                );
            } else {
                $this->get('session')->setFlash('error', 'Použijte prosím automatické doplňování k vyběru obcí.');
            }
        }

        return $this->render('PublicBudgetFrontendBundle:Municipality:compare-index.html.twig');
    }

    public function compareAction($slug1, $slug2)
    {
        $request = $this->get('request');
        $params = $request->query->all();

        $municipalityRepository = $this->getDoctrine()
        ->getRepository('PublicBudgetFrontendBundle:Municipality');

        $municipality1 = $municipalityRepository->findOneBySlug($slug1);
        $municipality2 = $municipalityRepository->findOneBySlug($slug2);

        $municipalityCompareData = new MunicipalityCompareData();

        $municipality1CompareQuery = $this->get('municipality_compare_query');
        $municipality2CompareQuery = $this->get('municipality_compare_query');

        $municipalityCompareForm = $this->get('form.factory')->create(
           $this->get('publicbudget.form.type.municipality_compare'),
           $municipalityCompareData
        );

        if (isset($params['porovnani_obci'])) {
            $municipalityCompareForm->bind($request);
        }

        $classificationQuery = $this->get('municipality_compare_classification_query');

        $classificationIds = $classificationQuery->createQueryFromData($municipalityCompareData);

        $municipality1Result = $municipality1CompareQuery->createQueryFromData(
            $municipality1,
            $municipalityCompareData,
            $classificationIds,
            BudgetStructureLogicFactory::getBudgetStructureLogicBy($municipality1)
        )->fetchAssoc();
        $municipality2Result = $municipality2CompareQuery->createQueryFromData(
            $municipality2,
            $municipalityCompareData,
            $classificationIds,
            BudgetStructureLogicFactory::getBudgetStructureLogicBy($municipality2)
        )->fetchAssoc();

        $compareProcessor = $this->get('municipality_compare_processor');

        $result = $compareProcessor->compare($municipality1->getName(), $municipality2->getName(), $municipality1Result, $municipality2Result, $classificationIds);

        return $this->render('PublicBudgetFrontendBundle:Municipality:compare.html.twig',
            array(
              'municipality1' => $municipality1,
              'municipality2' => $municipality2,
              'activeYear' => $municipalityCompareData->getYear(),
              'perInhabitant' => $municipalityCompareData->getPerInhabitant(),
              'result' => $result,
              'form' => $municipalityCompareForm->createView()
        ));
    }

    public function timelineAction($slug, $administrationUnitClass)
    {
        $administrationUnit = $this->getDoctrine()
          ->getRepository('PublicBudgetFrontendBundle:Municipality')->findOneBySlug($slug);

        if (!$administrationUnit) {
            throw $this->createNotFoundException();
        }

        $activeYear = null;

        $budgetStructureLogic = BudgetStructureLogicFactory::getBudgetStructureLogicBy($administrationUnit);
        $administrationUnitHelperData = AdministrationUnitHelperDataFactory::getBy($administrationUnit);

        $administrationUnitCommonRepository = new AdministrationUnitCommonRepository($this->getDoctrine()->getManager(), $administrationUnit, $budgetStructureLogic, $administrationUnitHelperData);

        $administrationUnitBudgetDataCollectionAssembler = new AdministrationUnitBudgetDataCollectionAssembler(
          $this->get('detail_output_modifier'),
          $this->get('router'),
          $this->get('request'),
          $administrationUnitCommonRepository
        );

        $budgetDataCollection = $administrationUnitBudgetDataCollectionAssembler->getDetailPageData(
          $this->container->getParameter('public_budget_frontend.current_year'), $administrationUnit
        );

        $budgetDataCollection->activeYear = null;
        $budgetDataCollection->administrationUnit = $administrationUnit;
        $budgetDataCollection->administrationUnitClass = $administrationUnitClass;
        $budgetDataCollection->administrationUnitDetailRoute = 'PublicBudgetFrontendBundle_municipality_detail';
        $budgetDataCollection->administrationUnitSectionDetailRoute = 'PublicBudgetFrontendBundle_municipality_section_detail';

        $request = $this->get('request');
        $params = $request->query->all();

        $municipalityRepository = $this->getDoctrine()
        ->getRepository('PublicBudgetFrontendBundle:Municipality');
        $municipality = $municipalityRepository->findOneBySlug($slug);

        $municipalityTimelineData = new MunicipalityTimelineData();

        $municipalityTimelineForm = $this->get('form.factory')->create(
           $this->get('publicbudget.form.type.municipality_timeline'),
           $municipalityTimelineData
        );

        if (isset($params['timeline_obce'])) {
            $municipalityTimelineForm->bind($request);
        }

        return $this->render('PublicBudgetFrontendBundle:Municipality:timeline.html.twig',
            array(
                'municipality' => $municipality,
                'form' => $municipalityTimelineForm->createView(),
                'budgetDataCollection' => $budgetDataCollection
            )
        );
    }

    public function timelineDataAction($slug)
    {
        $request = $this->get('request');
        $params = $request->query->all();

        $municipalityRepository = $this->getDoctrine()
        ->getRepository('PublicBudgetFrontendBundle:Municipality');
        $municipality = $municipalityRepository->findOneBySlug($slug);

        $municipalityTimelineData = new MunicipalityTimelineData();

        $municipalityTimelineForm = $this->get('form.factory')->create(
           $this->get('publicbudget.form.type.municipality_timeline'),
           $municipalityTimelineData
        );

        if (isset($params['timeline_obce'])) {
            $municipalityTimelineForm->bind($request);
        }

        $significantExpensesQuery = $this->get('municipality_timeline_significant_expenses_query');
        $significantExpenses = $significantExpensesQuery->getResult($municipality, $municipalityTimelineData->getPercentSignificance());

        $significantIncomesQuery = $this->get('municipality_timeline_significant_incomes_query');
        $significantIncomes = $significantIncomesQuery->getResult($municipality, $municipalityTimelineData->getPercentSignificance());
        
        $numberHelper = $this->get('number_helper');

        $result = array();
        foreach ($significantIncomes as $significantIncome) {
            $result[] = array(
                "startDate" => $significantIncome['year'],
                "endDate" => $significantIncome['year'],
                "headline" => $significantIncome['name'],
                "text" => "<p>Tato položka má hodnotu ".$numberHelper->number_format($significantIncome['value'])." Kč, což je ".$numberHelper->percent_format($significantIncome['percent_of_total'])." z celkových příjmů rozpočtu obce<br/>
                <a href='".$this->get('router')->generate('PublicBudgetFrontendBundle_municipality_detail', array('slug' => $municipality->getSlug(), 'rok' => $significantIncome['year']))."'>Do rozpočtu obce >></a><br/>
                <a href='".$this->get('router')->generate('PublicBudgetFrontendBundle_municipality_filter_trend', array('filtr_obci[municipality]' => $municipality->getId(), 'filtr_obci[year]' => $significantIncome['year'], 'filtr_obci[budget_item]' => 1))."'>Porovnat o ostatními obcemi >></a></p>",
                "tag" => "Významný příjem"
            );
        }

        foreach ($significantExpenses as $significantExpense) {
            $result[] = array(
                "startDate" => $significantExpense['year'],
                "endDate" => $significantExpense['year'],
                "headline" => $significantExpense['name'],
                "text" => "<p>Tato položka má hodnotu ".$numberHelper->number_format($significantExpense['value'])." Kč, což je ".$numberHelper->percent_format($significantExpense['percent_of_total'])." z celkových výdajů rozpočtu obce<br/>
                <a href='".$this->get('router')->generate('PublicBudgetFrontendBundle_municipality_detail', array('slug' => $municipality->getSlug(), 'rok' => $significantExpense['year']))."'>Do rozpočtu obce >></a><br/>
                <a href='".$this->get('router')->generate('PublicBudgetFrontendBundle_municipality_filter_trend', array('filtr_obci[municipality]' => $municipality->getId(), 'filtr_obci[year]' => $significantExpense['year'], 'filtr_obci[budget_item]' => 282))."'>Porovnat o ostatními obcemi >></a></p>",
                "tag" => "Významný výdaj"
            );
        }
    
        $timeline = array(
            "timeline" => array(
                 "headline" => "Timeline obce ".$municipality->getName(),
                 "type" => "default",
                 "text" =>"<p>časová osa znazorňující nejvyznamější položky rozpočtu v letech</p>",
                 "date" => $result,
                 'era' => array(array(
                    "startDate" => "2000",
                    "endDate" => "2013"
                 ))
            )
        );

        $response = new JsonResponse();
        $response->setData($timeline);

        return $response;
    }

    public function exportBudgetItemsAction($ico, $type)
    {
        $municipality = $this->getDoctrine()->getRepository('PublicBudgetFrontendBundle:Municipality')->findOneByOrganizationIco($ico);
        $budgetStructureLogic = BudgetStructureLogicFactory::getBudgetStructureLogicBy($municipality);
        $budgetItems = $this->getDoctrine()->getRepository('PublicBudgetFrontendBundle:BudgetItem')->findByMunicipalityForExport($municipality, $budgetStructureLogic);

        $csvExporterFactory = $this->get('csv_exporter_factory');
        $csvExporter = $csvExporterFactory->getCsvExporter($type);

        $headerData = array('okres', 'obec', 'csu_kod', 'ico_organizace', 'rok', 'trida_kod', 'trida_nazev',
                            'polozka_kod', 'polozka_nazev', 'skupina_kod', 'skupina_nazev', 'paragraf_kod',
                            'paragraf_nazev', 'konsolidace', 'hodnota');

        $csvOutput = $csvExporter->process($headerData, $budgetItems);

        $response = new Response();
        $response->headers->set('Content-Type', "text/csv");
        $response->headers->set('Content-Disposition', 'attachment; filename="'.$municipality->getName().'-'.$municipality->getOrganizationIco().'-'.$type.'.csv"');
        $response->setContent($csvOutput);

        return $response;
    }

    public function redirectToMunicipalityDetailPageAction($csuCode)
    {
        $ico = $this->get('request')->query->get('ico');

        if (!$csuCode) {
            $csuCode = $this->get('request')->query->get('csu');
        }

        if (!$ico && !$csuCode) {
            throw $this->createNotFoundException();
        }

        if ($ico) {
            $municipality = $this->getDoctrine()->getRepository('PublicBudgetFrontendBundle:Municipality')->findOneByOrganizationIco($ico);
        } else {
            $municipality = $this->getDoctrine()->getRepository('PublicBudgetFrontendBundle:Municipality')->findOneByCsuCode($csuCode);
        }

        if (!$municipality) {
            throw $this->createNotFoundException();
        }

        return $this->redirect($this->generateUrl('PublicBudgetFrontendBundle_municipality_timeline', array('slug' => $municipality->getSlug())));
    }

    public function getNameByIdAction()
    {
        $municipalityId = $this->get('request')->query->get('municipality_id');

        $municipalityRepository = $this->getDoctrine()
            ->getRepository('PublicBudgetFrontendBundle:Municipality');
        $municipality = $municipalityRepository->findOneById($municipalityId);

        $response = new JsonResponse();
        $response->setData($municipality->getName() . ' (Okres ' . $municipality->getOkres()->getName() . ')');

        return $response;
    }
}
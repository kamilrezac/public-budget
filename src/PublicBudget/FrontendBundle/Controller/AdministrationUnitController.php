<?php

namespace PublicBudget\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use PublicBudget\FrontendBundle\Repository as Repository;
use Symfony\Component\HttpFoundation\Response;

use PublicBudget\FrontendBundle\Municipality\Common\BudgetStructureLogicFactory;
use PublicBudget\FrontendBundle\Municipality\Detail\AdministrationUnitHelperDataFactory;
use PublicBudget\FrontendBundle\Municipality\Detail\AdministrationUnitSectionRepositoryFactory;
use PublicBudget\FrontendBundle\Municipality\Detail\AdministrationUnitBudgetDataCollectionAssembler;
use PublicBudget\FrontendBundle\Municipality\Detail\AjaxOutputModifier;
use PublicBudget\FrontendBundle\Repository\AdministrationUnitCommonRepository;
use PublicBudget\FrontendBundle\Entity\Municipality;

use PublicBudget\FrontendBundle\Municipality\Common\BudgetSlugConverter;

class AdministrationUnitController extends Controller
{
    public function detailAction($slug, $administrationUnitClass)
    {
        $administrationUnit = $this->getDoctrine()
        ->getRepository('PublicBudgetFrontendBundle:'.$administrationUnitClass)->findOneBySlug($slug);

        if (!$administrationUnit) {
            throw $this->createNotFoundException();
        }

        $activeYear = $this->get('request')->query->get('rok');

        if (!$activeYear) {
            $activeYear = $this->container->getParameter('public_budget_frontend.current_year');
        }

        $budgetStructureLogic = BudgetStructureLogicFactory::getBudgetStructureLogicBy($administrationUnit);
        $administrationUnitHelperData = AdministrationUnitHelperDataFactory::getBy($administrationUnit);

        $administrationUnitCommonRepository = new AdministrationUnitCommonRepository($this->getDoctrine()->getManager(), $administrationUnit, $budgetStructureLogic, $administrationUnitHelperData);

        $administrationUnitBudgetDataCollectionAssembler = new AdministrationUnitBudgetDataCollectionAssembler(
            $this->get('detail_output_modifier'),
            $this->get('router'),
            $this->get('request'),
            $administrationUnitCommonRepository
        );

        $budgetDataCollection = $administrationUnitBudgetDataCollectionAssembler->getDetailPageData(
            $activeYear, $administrationUnit
        );

        $budgetDataCollection->administrationUnit = $administrationUnit;
        $budgetDataCollection->administrationUnitClass = $administrationUnitClass;
        $budgetDataCollection->administrationUnitDetailRoute = 'PublicBudgetFrontendBundle_'.strtolower($administrationUnitClass).'_detail';
        $budgetDataCollection->administrationUnitSectionDetailRoute = 'PublicBudgetFrontendBundle_'.strtolower($administrationUnitClass).'_section_detail';

        return $this->render('PublicBudgetFrontendBundle:'.$administrationUnitClass.':detail.html.twig', array(
            'budgetDataCollection' => $budgetDataCollection
        ));
    }

    public function sectionDetailAction($slug, $type, $section, $activeYear, $administrationUnitClass)
    {
        if (!isset(BudgetSlugConverter::$toSectionName[$section])) {
            throw $this->createNotFoundException();
        }

        if (!isset(BudgetSlugConverter::$toTypeName[$type])) {
            throw $this->createNotFoundException();
        }

        $administrationUnit = $this->getDoctrine()->getRepository('PublicBudgetFrontendBundle:'.$administrationUnitClass)->findOneBySlug($slug);
        $budgetStructureLogic = BudgetStructureLogicFactory::getBudgetStructureLogicBy($administrationUnit);
        $administrationUnitHelperData = AdministrationUnitHelperDataFactory::getBy($administrationUnit);

        $repository = AdministrationUnitSectionRepositoryFactory::getSectionRepositoryByType($type, $this->getDoctrine()->getManager(), $administrationUnit, $budgetStructureLogic, $administrationUnitHelperData);

        $administrationUnitCommonRepository = new AdministrationUnitCommonRepository($this->getDoctrine()->getManager(), $administrationUnit, $budgetStructureLogic, $administrationUnitHelperData);

        $administrationUnitBudgetDataCollectionAssembler = new AdministrationUnitBudgetDataCollectionAssembler(
            $this->get('section_detail_output_modifier'),
            $this->get('router'),
            $this->get('request'),
            $administrationUnitCommonRepository
        );

        $budgetDataCollection = $administrationUnitBudgetDataCollectionAssembler->getSectionDetailPageData(
            $activeYear, $section, $type, $administrationUnit,
            $repository);

        $budgetDataCollection->administrationUnit = $administrationUnit;
        $budgetDataCollection->administrationUnitClass = $administrationUnitClass;
        $budgetDataCollection->administrationUnitDetailRoute = 'PublicBudgetFrontendBundle_'.strtolower($administrationUnitClass).'_detail';
        $budgetDataCollection->administrationUnitSectionDetailRoute = 'PublicBudgetFrontendBundle_'.strtolower($administrationUnitClass).'_section_detail';

        if ($administrationUnit instanceof Municipality && !in_array($type, array('financovani', 'presuny'))) {
            $municipalityFilterUrlBuilder = $this->get('municipality_filter_url_builder');
            $BudgetItemSummaryConfigurationRepository = $this->getDoctrine()->getRepository('PublicBudgetFrontendBundle:BudgetItemSummaryConfiguration');
            $display = $this->get('request')->query->get('zobrazeni');
            $expenseType = $this->get('request')->query->get('vydaje');
            $type = $BudgetItemSummaryConfigurationRepository->findOneBySlug($type);
            $section = $BudgetItemSummaryConfigurationRepository->findOneBySlug($section);
            $expenseType = $BudgetItemSummaryConfigurationRepository->findOneBySlug($expenseType);
            $populationRange = $administrationUnit->getPopulationRangeFor($activeYear)->getId();
            $budgetDataCollection->municipalityFilterUrl = $municipalityFilterUrlBuilder->buildFrom($activeYear, $section, $type, $expenseType, $display, $populationRange);
        }

        return $this->render('PublicBudgetFrontendBundle:'.$administrationUnitClass.':section-detail.html.twig', array(
            'budgetDataCollection' => $budgetDataCollection
        ));
    }

    public function numberDetailAction($municipalitySlug, $budgetItemType, $parentSection, $numberSlug)
    {
        return $this->render('PublicBudgetFrontendBundle:Municipality:number-detail.html.twig');
    }

    public function GoogleGraphDataAjaxAction()
    {
        list($code, $budgetType, $administrationUnitSlug, $administrationUnitClass, $display) = explode(' ', $this->get('request')->query->get('data'));
        $administrationUnit = $this->getDoctrine()
            ->getRepository('PublicBudgetFrontendBundle:'.$administrationUnitClass)
            ->findOneBySlug($administrationUnitSlug);

        $budgetStructureLogic = BudgetStructureLogicFactory::getBudgetStructureLogicBy($administrationUnit);
        $administrationUnitHelperData = AdministrationUnitHelperDataFactory::getBy($administrationUnit);
        $repository = AdministrationUnitSectionRepositoryFactory::getSectionRepositoryByType(
            $budgetType,
            $this->getDoctrine()->getManager(),
            $administrationUnit,
            $budgetStructureLogic,
            $administrationUnitHelperData
        );
        $data = $repository->findAjaxData($code);

        if ($display == 'obyvatel') {
            $ajaxOutputModifier = new AjaxOutputModifier();
            $data = $ajaxOutputModifier->alterResultBasedOnOptions($data, $administrationUnit);
        }

        return new Response(json_encode($data));
    }
}
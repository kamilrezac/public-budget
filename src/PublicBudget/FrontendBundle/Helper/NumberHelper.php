<?php

namespace PublicBudget\FrontendBundle\Helper;

class NumberHelper
{
	public function number_format($number, $decimals = 0, $dec_point = ',', $thousands_sep = ' ')
    {
        $number = number_format($number, $decimals, $dec_point, $thousands_sep);
        return str_replace(" ", utf8_encode("\xA0"), $number);
    }


    public function percent_format($value)
    {
        return $this->number_format($value, 2) . utf8_encode("\xA0") . "%";
    }
}
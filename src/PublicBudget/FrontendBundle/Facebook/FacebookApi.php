<?php

namespace PublicBudget\FrontendBundle\Facebook;

class FacebookApi
{
    private $facebookSdk;

    public function __construct($facebookSdk)
    {
        $this->facebookSdk = $facebookSdk;
    }

    public function userIsConnected()
    {
        return $this->facebookSdk->getUser() === 0 ? false:true;
    }

    public function getRedirectUrlToLoginPage()
    {
        return $this->facebookSdk->getLoginUrl();
    }
}

?>
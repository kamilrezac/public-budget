<?php

namespace PublicBudget\FrontendBundle\Twig\Extension;

class NumberExtension extends \Twig_Extension
{
    protected $numberHelper;

    public function __construct($numberHelper) 
    {
        $this->numberHelper = $numberHelper;
    }

    public function getFunctions()
    {
        return array(
            'twig_number_format' => new \Twig_Function_Method($this, 'twig_number_format'),
            'twig_percent_format' => new \Twig_Function_Method($this, 'twig_percent_format'),
            'twig_number_format_for_display' => new \Twig_Function_Method($this, 'twig_number_format_for_display'));
    }

    function twig_number_format($number, $decimals = 0, $dec_point = ',', $thousands_sep = ' ')
    {
        return $this->numberHelper->number_format($number, $decimals, $dec_point, $thousands_sep);
    }

    function twig_number_format_for_display($value, $display)
    {
        if ($display == "obyvatel") {
            return $this->numberHelper->number_format($value, 2) . utf8_encode("\xA0") . "Kč";
        }

        return $this->numberHelper->number_format($value) . utf8_encode("\xA0") . "Kč";
    }

    function twig_percent_format($value)
    {
        return $this->numberHelper->percent_format($value);
    }

    public function getName()
    {
        return 'number';
    }
}




<?php

namespace PublicBudget\FrontendBundle\Twig\Extension;

class ArrayExtension extends \Twig_Extension
{
    public function getFunctions()
    {
        return array('twig_array_sum' => new \Twig_Function_Method($this, 'twig_array_sum'));
    }

    function twig_array_sum($array)
    {
        return array_sum($array);
    }

    public function getName()
    {
        return 'array';
    }
}
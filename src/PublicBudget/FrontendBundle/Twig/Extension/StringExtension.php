<?php

namespace PublicBudget\FrontendBundle\Twig\Extension;

class StringExtension extends \Twig_Extension
{
    public function getFunctions()
    {
        return array('twig_slugify' => new \Twig_Function_Method($this, 'twig_slugify'));
    }

    function twig_slugify($text)
    {
        return \Gedmo\Sluggable\Util\Urlizer::urlize($text);
    }

    public function getName()
    {
        return 'string';
    }
}

// replace non letter or digits by -

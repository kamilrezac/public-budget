<?php

namespace PublicBudget\FrontendBundle\Twig\Extension;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\HttpKernel;

class PathExtension extends \Twig_Extension
{
    private $request;
    private $router;

    public function __construct(ContainerInterface $container) {
        $this->router = $container->get('router');
    }

    public function getFunctions()
    {
        return array(
            'smart_path' => new \Twig_Function_Method($this, 'smart_path')
        );
    }

    public function onKernelRequest(GetResponseEvent $event) {
        if ($event->getRequestType() === HttpKernel::MASTER_REQUEST) {
            $this->request = $event->getRequest();
        }
    }

    function smart_path($additionalAttributes = array(), $route = '', $filterQuery = array())
    {
        $route = $route ? $route : $this->request->attributes->get('_route');
        $attributes = array_merge($this->getQuery($filterQuery), $this->getAttributes($route));
        $attributes = array_merge($attributes, $additionalAttributes);

        return $this->router->generate($route, $attributes);
    }

    private function getQuery($filterQuery)
    {
        $query = $this->request->query->all();
        return array_diff_key($query, array_flip($filterQuery));
    }

    private function getAttributes($route)
    {
        $attributes = $this->request->attributes->all();
        unset($attributes['_route']);
        $routeVariables = $this->router->getRouteCollection()->get($route)->compile()->getVariables();
        return array_intersect_key($attributes, array_flip($routeVariables));
    }

    public function getName()
    {
        return 'path';
    }
}
<?php

namespace PublicBudget\FrontendBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class BudgetItemSummaryConfigurationEntityTransformer implements DataTransformerInterface
{

    private $budgetItemSummaryConfigurationRepository;

    public function __construct($budgetItemSummaryConfigurationRepository)
    {
        $this->budgetItemSummaryConfigurationRepository = $budgetItemSummaryConfigurationRepository;
    }

    public function transform($budgetItemSummaryConfiguration)
    {
        if (null === $budgetItemSummaryConfiguration) {
            return "";
        }

        return $budgetItemSummaryConfiguration->getId();
    }

    public function reverseTransform($budgetItemSummaryConfigurationId)
    {
        if (!$budgetItemSummaryConfigurationId) {
            return null;
        }

        $budgetItemSummaryConfiguration = $this->budgetItemSummaryConfigurationRepository
            ->findOneById($budgetItemSummaryConfigurationId)
        ;

        if (null === $budgetItemSummaryConfiguration) {
            throw new TransformationFailedException(sprintf(
                'An budget item summary configuration with id "%s" does not exist!',
                $budgetItemSummaryConfigurationId
            ));
        }

        return $budgetItemSummaryConfiguration;
    }
}
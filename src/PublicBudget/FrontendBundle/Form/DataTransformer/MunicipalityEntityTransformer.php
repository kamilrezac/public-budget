<?php

namespace PublicBudget\FrontendBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class MunicipalityEntityTransformer implements DataTransformerInterface
{

    private $municipalityRepository;

    public function __construct($municipalityRepository)
    {
        $this->municipalityRepository = $municipalityRepository;
    }

    public function transform($okres)
    {
        if (null === $okres) {
            return "";
        }

        return $okres->getId();
    }

    public function reverseTransform($okresId)
    {
        if (!$okresId) {
            return null;
        }

        $municipality = $this->municipalityRepository
            ->findOneById($okresId)
        ;

        if (null === $municipality) {
            throw new TransformationFailedException(sprintf(
                'An municipality with id "%s" does not exist!',
                $okresId
            ));
        }

        return $municipality;
    }
}
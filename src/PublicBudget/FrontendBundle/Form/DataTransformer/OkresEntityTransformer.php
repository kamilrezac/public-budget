<?php

namespace PublicBudget\FrontendBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class OkresEntityTransformer implements DataTransformerInterface
{

    private $okresRepository;

    public function __construct($okresRepository)
    {
        $this->okresRepository = $okresRepository;
    }

    public function transform($okres)
    {
        if (null === $okres) {
            return "";
        }

        return $okres->getId();
    }

    public function reverseTransform($okresId)
    {
        if (!$okresId) {
            return null;
        }

        $okres = $this->okresRepository
            ->findOneById($okresId)
        ;

        if (null === $okres) {
            throw new TransformationFailedException(sprintf(
                'An okres with id "%s" does not exist!',
                $okresId
            ));
        }

        return $okres;
    }
}
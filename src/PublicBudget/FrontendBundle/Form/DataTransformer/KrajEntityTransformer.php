<?php

namespace PublicBudget\FrontendBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class KrajEntityTransformer implements DataTransformerInterface
{

    private $krajRepository;

    public function __construct($krajRepository)
    {
        $this->krajRepository = $krajRepository;
    }

    public function transform($kraj)
    {
        if (null === $kraj) {
            return "";
        }

        return $kraj->getId();
    }

    public function reverseTransform($krajId)
    {
        if (!$krajId) {
            return null;
        }

        $kraj = $this->krajRepository
            ->findOneById($krajId)
        ;

        if (null === $kraj) {
            throw new TransformationFailedException(sprintf(
                'An kraj with id "%s" does not exist!',
                $krajId
            ));
        }

        return $kraj;
    }
}
<?php

namespace PublicBudget\FrontendBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use PublicBudget\FrontendBundle\Form\DataTransformer\BudgetItemSummaryConfigurationEntityTransformer;
use PublicBudget\FrontendBundle\Form\DataTransformer\KrajEntityTransformer;
use PublicBudget\FrontendBundle\Form\DataTransformer\OkresEntityTransformer;
use PublicBudget\FrontendBundle\Form\DataTransformer\MunicipalityEntityTransformer;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MunicipalityFilterType extends AbstractType {

    private $doctrine;

    public function __construct($doctrine)
    {
        $this->doctrine = $doctrine;
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {

        $krajEntityTransformer = new KrajEntityTransformer($this->doctrine->getRepository('PublicBudgetFrontendBundle:Kraj'));

        $builder->add($builder->create('kraj', 'choice', array(
            'label' => 'Kraj',
            'choices' => $this->getKrajChoices(),
            'required' => false,
            'empty_value' => '--- Všechny ---'
        ))->addModelTransformer($krajEntityTransformer));

        $okresEntityTransformer = new OkresEntityTransformer($this->doctrine->getRepository('PublicBudgetFrontendBundle:Okres'));

        $builder->add($builder->create('okres', 'choice', array(
            'label' => 'Okres',
            'choices' => $this->getOkresChoices(),
            'required' => false,
            'empty_value' => '--- Všechny ---'
        ))->addModelTransformer($okresEntityTransformer));


        $builder->add('population_range', 'entity',
            array(
                'label' => 'Velikost obce',
                'class' => 'PublicBudget\FrontendBundle\Entity\PopulationRange',
                'query_builder' => function ($repository) use($options) {
                    return $repository
                        ->createQueryBuilder('p');
                },
                'required' => false,
                'empty_value' => '--- Všechny ---'
        ));

        $municipalityEntityTransformer = new MunicipalityEntityTransformer($this->doctrine->getRepository('PublicBudgetFrontendBundle:Municipality'));

        $builder->add($builder->create('municipality', 'hidden', array(
            'label' => 'Obec k porovnání',
            'required' => false,
        ))->addModelTransformer($municipalityEntityTransformer));

        $builder->add('year', 'choice',
            array(
                'label' => 'Rok',
                'choices' => array_combine(range(2000, 2013),range(2000, 2013)),
                'required' => false,
                'empty_value' => false
        ));

        $builder->add('budget_item', 'entity',
            array(
                'label' => 'Seřadit dle',
                'class' => 'PublicBudget\FrontendBundle\Entity\BudgetItemSummaryConfiguration',
                'query_builder' => function ($repository) use($options) {
                    return $repository
                        ->createQueryBuilder('c')
                        ->where("c.type IN ('type_classification','summary_expression')")
                        ->andWhere('c.parent IS NULL');
                },
                'required' => false,
                'empty_value' => '- Vyberte položku -'
        ));

        $budgetItemSummaryConfigurationEntityTransformer = new BudgetItemSummaryConfigurationEntityTransformer($this->doctrine->getRepository('PublicBudgetFrontendBundle:BudgetItemSummaryConfiguration'));

//        $builder->add($builder->create('budget_item_income', 'choice', array(
//            'label' => 'Druh příjmů',
//            'choices' => $this->getIncomeSummaryConfigurationChoices(),
//            'required' => false,
//            'empty_value' => '- Vyberte položku -'
//        ))->addModelTransformer($budgetItemSummaryConfigurationEntityTransformer));
//
//        $builder->add($builder->create('budget_item_expense', 'choice', array(
//            'label' => 'Druh výdajů',
//            'choices' => $this->getExpenseSummaryConfigurationChoices(),
//            'required' => false,
//            'empty_value' => '- Vyberte položku -'
//        ))->addModelTransformer($budgetItemSummaryConfigurationEntityTransformer));
//
//        $builder->add($builder->create('budget_item_purpose', 'choice', array(
//            'label' => 'Účel',
//            'choices' => $this->getPurposeSummaryConfigurationChoices(),
//            'required' => false,
//            'empty_value' => '- Vyberte položku -'
//        ))->addModelTransformer($budgetItemSummaryConfigurationEntityTransformer));

        $builder->add($builder->create('budget_item_income', 'hidden', array(
            'label' => 'Druh příjmů',
            'required' => false,
        ))->addModelTransformer($budgetItemSummaryConfigurationEntityTransformer));

        $builder->add($builder->create('budget_item_expense', 'hidden', array(
            'label' => 'Druh výdajů',
            'required' => false,
        ))->addModelTransformer($budgetItemSummaryConfigurationEntityTransformer));

        $builder->add($builder->create('budget_item_purpose', 'hidden', array(
            'label' => 'Účel',
            'required' => false,
        ))->addModelTransformer($budgetItemSummaryConfigurationEntityTransformer));

        $builder->add('sort', 'choice',
            array(
                'label' => 'Řazení',
                'choices' => array('asc' => 'vzestupně', 'desc' => 'sestupně'),
                'required' => false,
                'expanded' => true
        ));

        $builder->add('per_inhabitant', 'choice',
            array(
                'label' => 'Za',
                'choices' => array('0' => 'za celou obec', '1' => 'na obyvatele'),
                'required' => false,
                'expanded' => true
        ));
    }

    private function getPurposeSummaryConfigurationChoices()
    {
        $choices = array();
        $summaryConfigurations = $this->doctrine->getRepository('PublicBudgetFrontendBundle:BudgetItemSummaryConfiguration')->findSectorClassificationSummaries();

        foreach ($summaryConfigurations as $summaryConfiguration) {
            $intendation = ($summaryConfiguration['level'] - 2) * 4;
            $name = html_entity_decode(str_repeat("&nbsp;", $intendation).$summaryConfiguration['name']);
            $choices[$summaryConfiguration['id']] = $name;
        }
    
        return $choices;
    }

    private function getIncomeSummaryConfigurationChoices()
    {
        $choices = array();
        $summaryConfigurations = $this->doctrine->getRepository('PublicBudgetFrontendBundle:BudgetItemSummaryConfiguration')->findIncomeTypeClassificationSummaries();

        foreach ($summaryConfigurations as $summaryConfiguration) {
            $intendation = ($summaryConfiguration['level'] - 2) * 4;
            $name = html_entity_decode(str_repeat("&nbsp;", $intendation).$summaryConfiguration['name']);
            $choices[$summaryConfiguration['id']] = $name;
        }
    
        return $choices;
    }

    private function getExpenseSummaryConfigurationChoices()
    {
        $choices = array();
        $summaryConfigurations = $this->doctrine->getRepository('PublicBudgetFrontendBundle:BudgetItemSummaryConfiguration')->findExpenseTypeClassificationSummaries();

        foreach ($summaryConfigurations as $summaryConfiguration) {
            $intendation = ($summaryConfiguration['level'] - 2) * 4;
            $name = html_entity_decode(str_repeat("&nbsp;", $intendation).$summaryConfiguration['name']);
            $choices[$summaryConfiguration['id']] = $name;
        }
    
        return $choices;
    }

    public function getKrajChoices()
    {
        $krajList = $this->doctrine->getRepository('PublicBudgetFrontendBundle:Kraj')->findAllAsChoices();

        $choices = array();
        foreach ($krajList as $kraj) {
            $choices[$kraj['id']] = $kraj['name'];
        }

        return $choices;
    }

    public function getOkresChoices()
    {
        $okresList = $this->doctrine->getRepository('PublicBudgetFrontendBundle:Okres')->findAllAsChoices();

        $choices = array();
        foreach ($okresList as $okres) {
            $choices[$okres['id']] = $okres['name'];
        }

        return $choices;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'csrf_protection' => false,
        ));
    }

    public function getName()
    {
        return 'filtr_obci';
    }
}


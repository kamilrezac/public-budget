<?php

namespace PublicBudget\FrontendBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MunicipalityTimelineType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('percent_significance', 'choice',
            array(
                'label' => 'Významnost položky v rozpočtu obce',
                'choices' => array_combine(range(0.01,1,0.01), range(1,100)),
                'required' => true,
                'empty_value' => false
        ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'csrf_protection' => false,
        ));
    }

    public function getName()
    {
        return 'timeline_obce';
    }
}
<?php

namespace PublicBudget\FrontendBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MunicipalityCompareType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('budget_item_expense', 'entity',
            array(
                'label' => 'Výdaje',
                'class' => 'PublicBudget\FrontendBundle\Entity\BudgetItemSummaryConfiguration',
                'query_builder' => function ($repository) use($options) {
                    return $repository
                        ->createQueryBuilder('c')
                        ->innerJoin('c.parent', 'p')
                        ->where("c.type = 'type_classification'")
                        ->andWhere("p.name='Výdaje'");
                },
                'required' => false,
                'empty_value' => '--- Všechny ---'
        ));

        $builder->add('budget_item_parent', 'entity',
            array(
                'label' => 'Oblast rozpočtu',
                'class' => 'PublicBudget\FrontendBundle\Entity\BudgetItemSummaryConfiguration',
                'query_builder' => function ($repository) use($options) {
                    return $repository
                        ->createQueryBuilder('c')
                        ->where('c.level IN (2,3)')
                        ->andWhere("c.type = 'sector_classification'");
                },
                'required' => false,
                'group_by' => 'parentName',
                'empty_value' => '- Vyberte položku -'
        ));

        $builder->add('budget_item_level', 'choice',
            array(
                'label' => 'Úroveň detailu',
                'choices' => array(
                    2 => "Nejnižší",
                    3 => "Střední",
                    4 => "Nejvyšší",
                ),
                'required' => true,
                'empty_value' => false
        ));

        $builder->add('per_inhabitant', 'choice',
            array(
                'label' => 'Za',
                'choices' => array('0' => 'za celou obec', '1' => 'na obyvatele'),
                'required' => true,
                'expanded' => true
        ));

        $builder->add('year', 'choice',
            array(
                'label' => 'Rok',
                'choices' => array_combine(range(2000, 2013),range(2000, 2013)),
                'required' => true,
                'empty_value' => false
        ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'csrf_protection' => false,
        ));
    }

    public function getName()
    {
        return 'porovnani_obci';
    }
}
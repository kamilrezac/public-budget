<?php

namespace PublicBudget\FrontendBundle\Form\Data;

class MunicipalityCompareData
{
    private $budgetItemExpense;

    private $budgetItemLevel = 2;

    private $budgetItemParent;

    private $perInhabitant = 1;

    private $year = '2013';

    public function getBudgetItemExpense()
    {
        return $this->budgetItemExpense;
    }

    public function setBudgetItemExpense($budgetItemExpense)
    {
        $this->budgetItemExpense = $budgetItemExpense;
    }

    public function getBudgetItemLevel()
    {
        return $this->budgetItemLevel;
    }

    public function setBudgetItemLevel($budgetItemLevel)
    {
        $this->budgetItemLevel = $budgetItemLevel;
    }

    public function getBudgetItemParent()
    {
        return $this->budgetItemParent;
    }

    public function setBudgetItemParent($budgetItemParent)
    {
        $this->budgetItemParent = $budgetItemParent;
    }

    public function getPerInhabitant() {
        return $this->perInhabitant;
    }

    public function setPerInhabitant($perInhabitant) {
        $this->perInhabitant = $perInhabitant;
    }

    public function getYear()
    {
        return $this->year;
    }

    public function setYear($year)
    {
        $this->year = $year;
    }
}
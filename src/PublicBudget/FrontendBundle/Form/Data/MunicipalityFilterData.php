<?php

namespace PublicBudget\FrontendBundle\Form\Data;

class MunicipalityFilterData
{
    private $kraj;

    private $okres;

    private $year = '2013';

    private $populationRange;

    private $budgetItem;

    private $budgetItemIncome;

    private $budgetItemExpense;

    private $budgetItemPurpose;

    private $sort = 'desc';

    private $perInhabitant = 1;

    private $municipality;

    public function getKraj()
    {
        return $this->kraj;
    }

    public function setKraj($kraj)
    {
        $this->kraj = $kraj;
    }

    public function getOkres()
    {
        return $this->okres;
    }

    public function setOkres($okres)
    {
        $this->okres = $okres;
    }

    public function getYear()
    {
        return $this->year;
    }

    public function setYear($year)
    {
        $this->year = $year;
    }

    public function getPopulationRange()
    {
        return $this->populationRange;
    }

    public function setPopulationRange($populationRange)
    {
        $this->populationRange = $populationRange;
    }

    public function getBudgetItem()
    {
        return $this->budgetItem;
    }

    public function setBudgetItem($budgetItem)
    {
        $this->budgetItem = $budgetItem;
    }

    public function getBudgetItemIncome()
    {
        return $this->budgetItemIncome;
    }

    public function setBudgetItemIncome($budgetItemIncome)
    {
        $this->budgetItemIncome = $budgetItemIncome;
    }

    public function getBudgetItemExpense()
    {
        return $this->budgetItemExpense;
    }

    public function setBudgetItemExpense($budgetItemExpense)
    {
        $this->budgetItemExpense = $budgetItemExpense;
    }

    public function getBudgetItemPurpose()
    {
        return $this->budgetItemPurpose;
    }

    public function setBudgetItemPurpose($budgetItemPurpose)
    {
        $this->budgetItemPurpose = $budgetItemPurpose;
    }

    public function getSort() {
        return $this->sort;
    }

    public function setSort($sort) {
        $this->sort = $sort;
    }

    public function getPerInhabitant() {
        return $this->perInhabitant;
    }

    public function setPerInhabitant($perInhabitant) {
        $this->perInhabitant = $perInhabitant;
    }

    public function getMunicipality() {
        return $this->municipality;
    }

    public function setMunicipality($municipality) {
        $this->municipality = $municipality;
    }
}




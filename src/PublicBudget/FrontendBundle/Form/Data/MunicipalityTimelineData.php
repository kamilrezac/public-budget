<?php

namespace PublicBudget\FrontendBundle\Form\Data;

class MunicipalityTimelineData
{
    private $percentSignificance = 0.25;

    public function getPercentSignificance()
    {
        return $this->percentSignificance;
    }

    public function setPercentSignificance($percentSignificance)
    {
        $this->percentSignificance = $percentSignificance;
    }
}
<?php

namespace PublicBudget\FrontendBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\FixtureInterface;
use PublicBudget\FrontendBundle\Entity\BudgetItemSummaryConfiguration;

class LoadBudgetItemSummaryConfiguration implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $cmd = $manager->getClassMetadata('PublicBudgetFrontendBundle:BudgetItemSummaryConfiguration');
        $connection = $manager->getConnection();
        $dbPlatform = $connection->getDatabasePlatform();
        $connection->beginTransaction();
        try {
            $connection->query('SET NAMES utf8');
            $connection->query('SET FOREIGN_KEY_CHECKS=0');
            $q = $dbPlatform->getTruncateTableSql($cmd->getTableName());
            $connection->executeUpdate($q);
            $connection->query('SET FOREIGN_KEY_CHECKS=1');
            $connection->commit();
        }
        catch (\Exception $e) {
            $connection->rollback();
        }

        $typeClassification = array();
        $sectorClassification = array();
        $summaryExpression = array();

        $typeClassification['1,2,3,4'] = array('Příjmy', 'class_code', null);
        $typeClassification['1'] = array('Daňové', 'class_code','1,2,3,4');
        $typeClassification['2'] = array('Nedaňové', 'class_code','1,2,3,4');
        $typeClassification['3'] = array('Kapitálové', 'class_code','1,2,3,4');
        $typeClassification['4'] = array('Dotace', 'class_code','1,2,3,4');
        $typeClassification['5,6'] = array('Výdaje', 'class_code',null);
        $typeClassification['5'] = array('Provozní', 'class_code','5,6');
        $typeClassification['6'] = array('Investiční', 'class_code','5,6');
        $sectorClassification['1'] = array('Zemědělství a lesy', 'category_code',null);
        $sectorClassification['2'] = array('Průmysl', 'category_code',null);
        // $sectorClassification['3'] = array('Služby', 'category_code',null);
        $sectorClassification['31,32'] = array('Vzdělávání', 'section_code',null);
        $sectorClassification['33'] = array('Kultura, církve a sdělovací prostředky','section_code',null);
        $sectorClassification['34'] = array('Tělovýchova a zájmová činnost','section_code',null);
        $sectorClassification['35'] = array('Zdravotnictví','section_code',null);
        $sectorClassification['36'] = array('Bydlení, komunální služby a územní rozvoj','section_code',null);
        $sectorClassification['37'] = array('Ochrana životního prostředí','section_code',null);
        $sectorClassification['38'] = array('Ostatní výzkum a vývoj','section_code',null);
        $sectorClassification['39'] = array('Ostatní služby','section_code',null);
        $sectorClassification['4'] = array('Sociální věci','category_code',null);
        $sectorClassification['5'] = array('Bezpečnost','category_code',null);
        $sectorClassification['6'] = array('Veřejná správa','category_code',null);
        $summaryExpression['1-282'] = array('Přebytek/Schodek',null,null);

        $typeClassificationResult = $this->getTypeClassificationResult($typeClassification, $manager);
        $sectorClassificationResult = $this->getSectorClassificationResult($sectorClassification, $manager);
        $summaryExpressionResult = $this->getSummaryExpressionResult($summaryExpression);

        $this->savebudgetItemSummaryConfiguration($typeClassificationResult, $manager);
        $this->savebudgetItemSummaryConfiguration($sectorClassificationResult, $manager);
        $this->savebudgetItemSummaryConfiguration($summaryExpressionResult, $manager);
    }

    private function getTypeClassificationResult($data, $manager)
    {
        $result = array();
        foreach ($data as $id => $row) {
            if (!isset($row[2])) {
                $onlyIds = $manager->getConnection()->fetchAll("SELECT c.id FROM type_classification c WHERE c.".$row[1]." IN (". $id .")");
                $result[$id] = array();
                $result[$id][0] = 'type_classification'; //classification type
                $result[$id][1] = implode(',', array_map('current', $onlyIds)); //ids
                $result[$id][2] = null; //parent
                $result[$id][3] = $row[0]; //name
                $result[$id][4] = 1; //level
                continue;
            }

            $classifications = $manager->getConnection()->fetchAll("SELECT c.id, c.subgroup_name AS level_2_name, c.subgroup_code AS level_2_code, c.item_name AS level_3_name, c.item_code AS level_3_code FROM type_classification c WHERE c.".$row[1]." IN (". $id .") ORDER BY c.subgroup_code,c.item_code ASC");
            $result = $result + $this->getClassificationResult($classifications, 'type_classification', $id, $row);
        }

        return $result;
    }

    private function getSectorClassificationResult($data, $manager)
    {
        $result = array();
        foreach ($data as $id => $row) {
            $classifications = $manager->getConnection()->fetchAll("SELECT c.id, c.subsection_name AS level_2_name, c.subsection_code AS level_2_code, c.paragraph_name AS level_3_name, c.paragraph_code AS level_3_code FROM sector_classification c WHERE c.".$row[1]." IN (". $id .") ORDER BY c.subsection_code,c.paragraph_code ASC");
            $result = $result + $this->getClassificationResult($classifications, 'sector_classification', $id, $row);
        }

        return $result;
    }

    private function getClassificationResult($classifications, $type, $id, $row)
    {
        $result[$id] = array();
        $result[$id][1] = "";
        foreach ($classifications as $classification) {
            $level_2_name = $classification['level_2_name'];
            $level_3_name = $classification['level_3_name'];
            $level_2_code = $classification['level_2_code'];
            $level_3_code = $classification['level_3_code'];

            if (!isset($result[$level_2_code])) {
                $result[$level_2_code] = array();
                $result[$level_2_code][0] = $type; //classification type
                $result[$level_2_code][1] = ""; //id
                $result[$level_2_code][2] = $id; //parent
                $result[$level_2_code][3] = $level_2_name; //name
                $result[$level_2_code][4] = 3; //level
            }

            if (!isset($result[$level_3_code])) {
                $result[$level_3_code] = array();
                $result[$level_3_code][0] = $type; //classification type
                $result[$level_3_code][1] = ""; //id
                $result[$level_3_code][2] = $level_2_code; //parent
                $result[$level_3_code][3] = $level_3_name; //name
                $result[$level_3_code][4] = 4; //level
            }

            $result[$id][1] .= $classification['id'].',';
            $result[$level_2_code][1] .= $result[$level_2_code][1] ? ','.$classification['id']:$classification['id'];
            $result[$level_3_code][1] = $classification['id'];
        }
        $result[$id][0] = $type; //classification type
        $result[$id][1] = rtrim($result[$id][1], ','); //ids
        $result[$id][2] = $row[2]; //parent
        $result[$id][3] = $row[0]; //name
        $result[$id][4] = 2; //level

        return $result;
    }

    private function getSummaryExpressionResult($data)
    {
        $result = array();
        foreach ($data as $id => $row) {
            $result[$id][0] = 'summary_expression'; //type classification
            $result[$id][1] = $id; //summary configuration id expression
            $result[$id][2] = null; //parent
            $result[$id][3] = $row[0]; //name
            $result[$id][4] = null; //level
        }

        return $result;
    }

    private function savebudgetItemSummaryConfiguration($result, $manager)
    {
        foreach ($result as $id => $row) {
            $budgetItemSummaryConfiguration = new BudgetItemSummaryConfiguration();
            $budgetItemSummaryConfiguration->setType($row[0]);
            $budgetItemSummaryConfiguration->setValue($row[1]);
            $budgetItemSummaryConfiguration->setName($row[3]);
            $budgetItemSummaryConfiguration->setLevel($row[4]);
            $budgetItemSummaryConfiguration->setCode($id);
            if ($row[2]) {
                $parentId = $manager->getConnection()->fetchColumn("SELECT id FROM budget_item_summary_configuration s WHERE s.code=:code AND s.type=:type", array('code' => $row[2], 'type' => $row[0]));
                $budgetItemSummaryConfiguration->setParent(
                    $manager->getRepository('PublicBudgetFrontendBundle:BudgetItemSummaryConfiguration')->find($parentId)
                );
            } else {
                $budgetItemSummaryConfiguration->setParent(null);
            }
            $manager->persist($budgetItemSummaryConfiguration);
            $manager->flush();
        }
    }
}

<?php

namespace PublicBudget\FrontendBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\FixtureInterface;
use PublicBudget\FrontendBundle\Entity\PopulationRange;

class LoadPopulationRanges implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $cmd = $manager->getClassMetadata('PublicBudgetFrontendBundle:PopulationRange');
        $connection = $manager->getConnection();
        $dbPlatform = $connection->getDatabasePlatform();
        $connection->beginTransaction();
        try {
            $connection->query('SET FOREIGN_KEY_CHECKS=0');
            $q = $dbPlatform->getTruncateTableSql($cmd->getTableName());
            $connection->executeUpdate($q);
            $connection->query('SET FOREIGN_KEY_CHECKS=1');
            $connection->commit();
        }
        catch (\Exception $e) {
            $connection->rollback();
        }

        $data = array();
        $data[] = array(1,100);
        $data[] = array(101,200);
        $data[] = array(201,500);
        $data[] = array(501,1000);
        $data[] = array(1001,2000);
        $data[] = array(2001,5000);
        $data[] = array(5001,10000);
        $data[] = array(10001,20000);
        $data[] = array(20001,50000);
        $data[] = array(50001,100000);
        $data[] = array(100001,2000000);

        foreach ($data as $row) {
            $populationRange = new PopulationRange();
            $populationRange->setMin($row[0]);
            $populationRange->setMax($row[1]);
            $manager->persist($populationRange);
        }
        $manager->flush();

        $populationRanges = $manager->getRepository('PublicBudgetFrontendBundle:PopulationRange')->findAll();

        $populations = $manager->getRepository('PublicBudgetFrontendBundle:Population')->findAll();

        foreach ($populations as $population) {
            foreach ($populationRanges as $populationRange) {
                if ($populationRange->getMin() <= $population->getCount() && $populationRange->getMax() >= $population->getCount()) {
                    $population->setPopulationRange($populationRange);
                    $manager->persist($population);
                }
            }
        }
        $manager->flush();
    }
}
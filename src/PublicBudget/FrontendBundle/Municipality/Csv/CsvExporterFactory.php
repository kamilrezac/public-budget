<?php

namespace PublicBudget\FrontendBundle\Municipality\Csv;
use PublicBudget\FrontendBundle\Municipality\Csv as Csv;

class CsvExporterFactory
{
    public function getCsvExporter($type)
    {
        switch ($type) {
            case 'us':
                return new Csv\UsCsvExporter();
            case 'cz':
                return new Csv\CzechCsvExporter();
        }
    }
}
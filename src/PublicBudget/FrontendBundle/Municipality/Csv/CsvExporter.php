<?php

namespace PublicBudget\FrontendBundle\Municipality\Csv;

abstract class CsvExporter
{
    protected $delimiter;

    protected $enclosure;

    protected $encoding;

    protected $decimalMark;

    public function __construct()
    {
        $this->setProperties();
    }

    public function process($headerData, $dataset)
    {
        $output = $this->transform($headerData);
        foreach ($dataset as $record) {
            $output .=  $this->transform($record);
        }

        return $this->encode($output);
    }

    private function transform($record)
    {
        array_walk($record, array($this, 'appendEnclosure'));
        return implode($this->delimiter, $record) . "\n";
    }

    public function smartProcess($headerData, $dataset, $title = array())
    {
        global $keys;
        $keys = array_flip(array_keys($headerData));

        $output = '';
        if ($title) {
            $output .= $this->transform($title);
        }

        $output .= $this->transform($headerData);
        foreach ($dataset as $record) {
            $output .= $this->smartTransform($record, $keys);
        }

        return $this->encode($output);
    }

    private function smartTransform($record, $keys)
    {
        $record = array_intersect_key($record, $keys);
        uksort($record, function($a, $b) {
            global $keys;
            return $keys[$a] < $keys[$b] ? -1:1;
        });
        array_walk($record, array($this, 'appendEnclosure'));
        return implode($this->delimiter, $record) . "\n";
    }

    private function appendEnclosure(&$item)
    {
        if (is_numeric($item)) {
            $item = str_replace(".", $this->decimalMark, $item);
        }
        $item = $this->enclosure . $item . $this->enclosure;
    }

    protected abstract function encode($output);

    protected abstract function setProperties();
}
<?php

namespace PublicBudget\FrontendBundle\Municipality\Csv;

class CzechCsvExporter extends CsvExporter
{
    protected function encode($output)
    {
        return iconv('utf-8', $this->encoding . '//TRANSLIT', $output);
    }

    protected function setProperties()
    {
        $this->delimiter = ';';
        $this->enclosure = "\"";
        $this->decimalMark = ',';
        $this->encoding = "windows-1250";
    }
}
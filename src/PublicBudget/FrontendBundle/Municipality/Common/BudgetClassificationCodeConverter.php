<?php

namespace PublicBudget\FrontendBundle\Municipality\Common;

class BudgetClassificationCodeConverter
{
    public static $toTypeName = array(
        '1' => 'Daňové',
        '2' => 'Nedaňové',
        '3' => 'Kapitálové',
        '4' => 'Dotace',
    );

    public static $toSectorName = array(
        '1' => 'Zemědělství a lesy',
        '2' => 'Průmysl',
        '3' => 'Služby',
        '31,32' => 'Vzdělávání',
        '33' => 'Kultura, církve a sdělovací prostředky',
        '34' => 'Tělovýchova a zájmová činnost',
        '35' => 'Zdravotnictví',
        '36' => 'Bydlení, komunální služby a územní rozvoj',
        '37' => 'Ochrana životního prostředí',
        '38' => 'Ostatní výzkum a vývoj',
        '39' => 'Ostatní služby', // '39' => 'Ostatní činnosti související se službami pro obyvatelstvo',
        '4' => 'Sociální věci',
        '5' => 'Bezpečnost',
        '6' => 'Veřejná správa',
    );
}
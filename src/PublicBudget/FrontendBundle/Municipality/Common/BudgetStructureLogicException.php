<?php

namespace PublicBudget\FrontendBundle\Municipality\Common;

class BudgetStructureLogicException extends BudgetStructureLogic
{
    public $incomeConsolidationById = array('108','138','169','175','176','179','192');
    public $incomeConsolidation = array('2226','2441','4121','4133','4134','4139','4221');
    public $expenseConsolidationById = array('295','309','311','312','314','321','411','443');
    public $expenseConsolidation = array('5321','5342','5344','5345','5349','5367','6341','6441');
    public $changeOfDebtById = array('461','462','463','464','468','469','475','476','477',
      '478','482','483','484','485');
    public $changeOfDebt = array('8111','8112','8113','8114','8121','8122','8211','8212','8213','8214',
      '8221','8222','8223','8224');
}
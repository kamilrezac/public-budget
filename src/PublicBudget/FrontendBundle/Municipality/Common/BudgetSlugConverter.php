<?php

namespace PublicBudget\FrontendBundle\Municipality\Common;

class BudgetSlugConverter
{
    public static $toSectionName = array(
        'danove' => 'Daňové',
        'nedanove' => 'Nedaňové',
        'kapitalove' => 'Kapitálové',
        'dotace' => 'Dotace',
        'zemedelstvi-a-lesy' => 'Zemědělství a lesy',
        'prumysl' => 'Průmysl',
        'sluzby' => 'Služby',
        'vzdelavani' => 'Vzdělávání',
        'kultura-cirkve-a-sdelovaci-prostredky' => 'Kultura, církve a sdělovací prostředky',
        'telovychova-a-zajmova-cinnost' => 'Tělovýchova a zájmová činnost',
        'zdravotnictvi' => 'Zdravotnictví',
        'bydleni-komunalni-sluzby-a-uzemni-rozvoj' => 'Bydlení, komunální služby a územní rozvoj',
        'ochrana-zivotniho-prostredi' => 'Ochrana životního prostředí',
        'ostatni-vyzkum-a-vyvoj' => 'Ostatní výzkum a vývoj',
        'ostatni-sluzby' => 'Ostatní služby',
        'socialni-veci' => 'Sociální věci',
        'bezpecnost' => 'Bezpečnost',
        'verejna-sprava' => 'Veřejná správa',
        'zmena-prostredku-na-uctech' => 'Změna prostředků na účtech',
        'zmena-zadluzeni' => 'Změna zadlužení',
        'prijmy' => 'Příjmy',
        'vydaje' => 'Výdaje',
    );

    public static $toTypeName = array(
        'prijmy' => 'Příjmy',
        'vydaje' => 'Výdaje',
        'financovani' => 'Financování',
        'presuny' => 'Přesuny'
    );

    public static $toSectionCode = array(
        'danove' => '1',
        'nedanove' => '2',
        'kapitalove' => '3',
        'dotace' => '4',
        'zemedelstvi-a-lesy' => '1',
        'prumysl' => '2',
        'sluzby' => '3',
        'vzdelavani' => '31,32',
        'kultura-cirkve-a-sdelovaci-prostredky' => '33',
        'telovychova-a-zajmova-cinnost' => '34',
        'zdravotnictvi' => '35',
        'bydleni-komunalni-sluzby-a-uzemni-rozvoj' => '36',
        'ochrana-zivotniho-prostredi' => '37',
        'ostatni-vyzkum-a-vyvoj' => '38',
        'ostatni-sluzby' => '39',
        'socialni-veci' => '4',
        'bezpecnost' => '5',
        'verejna-sprava' => '6',
    );
}
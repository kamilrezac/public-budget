<?php

namespace PublicBudget\FrontendBundle\Municipality\Common;
use PublicBudget\FrontendBundle\Entity\Municipality;

use PublicBudget\FrontendBundle\Municipality\Common\BudgetStructureLogic;
use PublicBudget\FrontendBundle\Municipality\Common\BudgetStructureLogicException;

class BudgetStructureLogicFactory
{
    private static $municipalitiesWithException = array('289');

    public static function getBudgetStructureLogicBy($administrationUnit)
    {
        if ($administrationUnit instanceof Municipality &&
            in_array($administrationUnit->getId(), self::$municipalitiesWithException)) {
            return new BudgetStructureLogicException();
        }

        return new BudgetStructureLogic();
    }

    public function getBy($administrationUnit)
    {
        if ($administrationUnit instanceof Municipality &&
            in_array($administrationUnit->getId(), self::$municipalitiesWithException)) {
            return $this->getBudgetStructureLogicException();
        }

        return $this->getBudgetStructureLogic();
    }

    public function getBudgetStructureLogic()
    {
    	return new BudgetStructureLogic();
    }

    public function getBudgetStructureLogicException()
    {
    	return new BudgetStructureLogicException();
    }

}
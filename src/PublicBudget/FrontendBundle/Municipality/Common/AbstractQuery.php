<?php

namespace PublicBudget\FrontendBundle\Municipality\Common;

use PublicBudget\FrontendBundle\Municipality\Common\BudgetStructureLogic;
use PublicBudget\FrontendBundle\Municipality\Common\BudgetStructureLogicException;

class AbstractQuery
{
    protected $entityManager;
    protected $sqlJoin = array();
    protected $sqlWhere = array();
    protected $sqlGroup = '';
    protected $sqlHaving = '';
    protected $sqlOrderValue = '';
    protected $sqlOrderSort = '';
    protected $sqlFrom = '';
    protected $sqlLimit;
    protected $sqlColumns;

    protected $consolidation = array();
    protected $consolidationException = array();
    protected $budgetStructureLogicFactory;

    public function __construct($entityManager, BudgetStructureLogicFactory $budgetStructureLogicFactory)
    {
        $budgetStructureLogic = $budgetStructureLogicFactory->getBudgetStructureLogic();
        $budgetStructureLogicException = $budgetStructureLogicFactory->getBudgetStructureLogicException();
        $this->budgetStructureLogicFactory = $budgetStructureLogicFactory;
        $this->entityManager = $entityManager;
        $this->consolidation = array_merge($budgetStructureLogic->incomeConsolidationById, $budgetStructureLogic->expenseConsolidationById);
        $this->consolidationException = array_merge(
          $budgetStructureLogicException->incomeConsolidationById,
          $budgetStructureLogicException->expenseConsolidationById
        );
    }

    public function setLimit($offset, $rowCount)
    {
        $this->sqlLimit = "LIMIT ". $offset .",".$rowCount;
    }

    public function execute()
    {
        return $this->entityManager->getConnection()->fetchAll($this->assembleQuery());
    }

    public function fetchAssoc()
    {
        return $this->entityManager->getConnection()->fetchAssoc($this->assembleQuery());
    }

    protected function assembleQuery()
    {
        $sql = array();
        $sql[] = "SELECT " . implode(',',$this->sqlColumns);
        $sql[] = "FROM " . $this->sqlFrom;
        $sql[] = implode(' ', $this->sqlJoin);
        if ($this->sqlWhere) {
            $sql[] = 'WHERE ' . implode(' AND ', $this->sqlWhere);
        }
        if ($this->sqlGroup) {
            $sql[] = 'GROUP BY ' . $this->sqlGroup;
        }
        if ($this->sqlHaving) {
            $sql[] = "HAVING " . $this->sqlHaving;
        }
        if ($this->sqlOrderValue) {
            $sql[] = "ORDER BY ". $this->sqlOrderValue . " " . $this->sqlOrderSort;
        }
        if ($this->sqlLimit) {
            $sql[] = $this->sqlLimit;
        }
        
        return implode(' ', $sql);
    }
}

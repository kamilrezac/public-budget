<?php

namespace PublicBudget\FrontendBundle\Municipality\Common;
use GeoIp2\Database\Reader;

class ClientIpLocationFinder
{
    public function getUserLocationData()
    {
        $reader = new Reader(__DIR__.'/../../GeoIP/GeoLite2-City.mmdb');

        try {
            $record = $reader->city($this->getClientIpAddress());
        } catch (\Exception $e) {
        }

        if (isset($record)) {
            return $record;
        }

        return false;
    }

    public function getClientIpAddress() {
        foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key) {
            if (array_key_exists($key, $_SERVER) === true) {
                foreach (explode(',', $_SERVER[$key]) as $ip) {
                    if (filter_var($ip, FILTER_VALIDATE_IP) !== false) {
                        return $ip;
                    }
                }
            }
        }

        return '';
    }
}
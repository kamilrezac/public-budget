<?php

namespace PublicBudget\FrontendBundle\Municipality\Timeline;

use PublicBudget\FrontendBundle\Municipality\Common\AbstractQuery;

class DeviationExpensesInAllYearsQuery extends AbstractQuery
{
	public function getResult($municipality, $percentSignificance)
	{
		$budgetStructureLogic = $this->budgetStructureLogicFactory->getBy($municipality);


		$sql = "SELECT s.paragraph_name AS name, b.value_final AS value
				FROM budget_item b
				INNER JOIN (SELECT AVG(bb.value_final) as avg_expenses
							FROM budget_item bb
							INNER JOIN type_classification tt
			                ON bb.type_classification_id=tt.id
			                WHERE tt.id IN (".implode(',', $budgetStructureLogic->expensesById).")
			                AND tt.id NOT IN (".implode(',', $budgetStructureLogic->expenseConsolidationById).")
			                AND bb.municipality_id = ".$municipality->getId().") r
				ON r.year=b.year
			    INNER JOIN type_classification t
			    ON b.type_classification_id=t.id
			    INNER JOIN sector_classification s
			    ON b.type_classification_id=s.id
			    WHERE (b.value_final/r.expenses) >= ".$percentSignificance."
			    AND t.id IN (".implode(',', $budgetStructureLogic->expensesById).")
			    AND t.id NOT IN (".implode(',', $budgetStructureLogic->expenseConsolidationById).")
			    AND b.municipality_id = ".$municipality->getId().";";

        return $this->entityManager->getConnection()->fetchAll($sql);
	}
}
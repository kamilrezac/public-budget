<?php

namespace PublicBudget\FrontendBundle\Municipality\Timeline;

use PublicBudget\FrontendBundle\Municipality\Common\AbstractQuery;

class SignificantExpensesForYearQuery extends AbstractQuery
{
	public function getResult($municipality, $percentSignificance)
	{
		$budgetStructureLogic = $this->budgetStructureLogicFactory->getBy($municipality);


		$sql = "SELECT s.category_code AS section_code, s.paragraph_name AS name, r.year, b.value_final AS value, ROUND((b.value_final/r.expenses) * 100, 1) as percent_of_total
				FROM budget_item b
				INNER JOIN (SELECT bb.year, SUM(bb.value_final) as expenses
							FROM budget_item bb
							INNER JOIN type_classification tt
			                ON bb.type_classification_id=tt.id
			                WHERE tt.id IN (".implode(',', $budgetStructureLogic->expensesById).")
			                AND tt.id NOT IN (".implode(',', $budgetStructureLogic->expenseConsolidationById).")
			                AND bb.municipality_id = ".$municipality->getId()."
							GROUP BY bb.year) r
				ON r.year=b.year
			    INNER JOIN type_classification t
			    ON b.type_classification_id=t.id
			    INNER JOIN sector_classification s
			    ON b.type_classification_id=s.id
			    WHERE (b.value_final/r.expenses) >= ".$percentSignificance."
			    AND t.id IN (".implode(',', $budgetStructureLogic->expensesById).")
			    AND t.id NOT IN (".implode(',', $budgetStructureLogic->expenseConsolidationById).")
			    AND b.municipality_id = ".$municipality->getId()." AND b.year >= 2000;";

        return $this->entityManager->getConnection()->fetchAll($sql);
	}
}
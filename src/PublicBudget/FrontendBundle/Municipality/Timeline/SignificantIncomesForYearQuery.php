<?php

namespace PublicBudget\FrontendBundle\Municipality\Timeline;

use PublicBudget\FrontendBundle\Municipality\Common\AbstractQuery;

class SignificantIncomesForYearQuery extends AbstractQuery
{
	public function getResult($municipality, $percentSignificance)
	{
		$budgetStructureLogic = $this->budgetStructureLogicFactory->getBy($municipality);


		$sql = "SELECT t.class_code AS section_code, t.item_name AS name, r.year, b.value_final AS value, ROUND((b.value_final/r.incomes) * 100, 1) AS percent_of_total
				FROM budget_item b
				INNER JOIN (SELECT bb.year, SUM(bb.value_final) as incomes
							FROM budget_item bb
							INNER JOIN type_classification tt
			                ON bb.type_classification_id=tt.id
			                WHERE tt.id IN (".implode(',', $budgetStructureLogic->incomesById).")
			                AND tt.id NOT IN (".implode(',', $budgetStructureLogic->incomeConsolidationById).")
			                AND bb.municipality_id = ".$municipality->getId()."
							GROUP BY bb.year) r
				ON r.year=b.year
			    INNER JOIN type_classification t
			    ON b.type_classification_id=t.id
			    WHERE (b.value_final/r.incomes) >= ".$percentSignificance."
			    AND t.id IN (".implode(',', $budgetStructureLogic->incomesById).")
			    AND t.id NOT IN (".implode(',', $budgetStructureLogic->incomeConsolidationById).")
			    AND b.municipality_id = ".$municipality->getId()." AND b.year >= 2000;";

        return $this->entityManager->getConnection()->fetchAll($sql);
	}
}
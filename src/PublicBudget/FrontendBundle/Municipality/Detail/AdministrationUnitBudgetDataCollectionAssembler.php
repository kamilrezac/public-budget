<?php

namespace PublicBudget\FrontendBundle\Municipality\Detail;

use PublicBudget\FrontendBundle\Entity as Entity;

use PublicBudget\FrontendBundle\Municipality\Common\BudgetSlugConverter;
use PublicBudget\FrontendBundle\Entity\Municipality;
use PublicBudget\FrontendBundle\Entity\Kraj;

class AdministrationUnitBudgetDataCollectionAssembler
{
    private $outputModifier;

    private $router;

    private $commonRepository;

    private $request;

    public function __construct($outputModifier, $router, $request, $commonRepository)
    {
        $this->outputModifier = $outputModifier;
        $this->router = $router;
        $this->request = $request;
        $this->commonRepository = $commonRepository;
    }

    public function getDetailPageData($activeYear, $administrationUnit)
    {
        $display = $this->request->query->get('zobrazeni');
        $expensesSectionFilter = $this->request->query->get('vydaje');

        $incomeExpenseGroupedByYear = $this->commonRepository->findIncomeAndExpenseGroupedByYearFor($administrationUnit);

        $totalBalance = 0;
        $totalBalanceActiveYear = 0;
        foreach ($incomeExpenseGroupedByYear as $incomeExpenseYear) {
            $totalBalance += $incomeExpenseYear['income'] - $incomeExpenseYear['expense'];
            if ($incomeExpenseYear['year'] == $activeYear) {
                $totalBalanceActiveYear = $incomeExpenseYear['income'] - $incomeExpenseYear['expense'];
            }
        }

        $populationActiveYear = $administrationUnit->getPopulationFor($activeYear);

        $budgetDataCollection = new \stdClass();

        //filter expenses by class code
        if ($expensesSectionFilter == 'provozni') {
            $budgetDataCollection->expenseClassCodeFilter = "5";
        } else if ($expensesSectionFilter == 'investicni') {
            $budgetDataCollection->expenseClassCodeFilter = "6";
        } else {
            $budgetDataCollection->expenseClassCodeFilter = "5,6";
        }

        // display per inhabitant
        if ($display == 'obyvatel') {
            $this->outputModifier->computeForInhabitant($populationActiveYear);
            $totalBalanceActiveYear = $totalBalanceActiveYear / $populationActiveYear;
            $totalBalance = $totalBalance / $populationActiveYear;
        }

        $budgetDataCollection->activeYear = $activeYear;
        $budgetDataCollection->shareMessages = $this->getSocialShareMessagesBasedOnTotalBalance(
            $totalBalance,
            $administrationUnit
        );

        $budgetDataCollection->display = $display;

        $budgetDataCollection->expensesSectionFilter = $expensesSectionFilter;
        $budgetDataCollection->populationActiveYear = $populationActiveYear;
        $budgetDataCollection->totalBalance = $totalBalance;
        $budgetDataCollection->totalBalanceActiveYear = $totalBalanceActiveYear;
        $budgetDataCollection->incomeExpenseGroupedByYear = $incomeExpenseGroupedByYear;

        $budgetDataCollection->financingSummary = $this->outputModifier->alterResultBasedOnOptions($this->commonRepository->findFinancingFor($activeYear), 'Financování');
        $budgetDataCollection->transferSummary = $this->outputModifier->alterResultBasedOnOptions($this->commonRepository->findTransfersFor($activeYear), 'Přesuny');
        $budgetDataCollection->incomeSummary = $this->outputModifier->alterResultBasedOnOptions($this->commonRepository->findIncomeSectionsFor($activeYear), 'Příjmy');
        $budgetDataCollection->expenseSummary = $this->outputModifier->alterResultBasedOnOptions($this->commonRepository->findExpenseByPurposeSectionsFor($activeYear, $expensesSectionFilter), 'Výdaje');

        return $budgetDataCollection;
    }

    public function getSectionDetailPageData($activeYear, $section, $type, $administrationUnit, $repository)
    {
        $display = $this->request->query->get('zobrazeni');
        $sort = $this->request->query->get('razeni');
        $expensesSectionFilter = $this->request->query->get('vydaje');

        $result = $repository->findSectionDetail($activeYear, $section, $expensesSectionFilter);

        // expenses are displayed by sector classification
        if ($type == 'vydaje') {
            $classificationTypeMark = '§';
        } else {
            $classificationTypeMark = 'Pol.';
        }

        // due to accounting logic, financing items has opposite sign
        if ($type == 'financovani') {
            $this->outputModifier->negateResult();
        }

        // display per inhabitant
        if ($display == 'obyvatel') {
            $populationActiveYear = $administrationUnit->getPopulationFor($activeYear);
            $this->outputModifier->computeForInhabitant($populationActiveYear);
        }

        // sort by size
        if ($sort != 'paragraf') {
            $this->outputModifier->sortBySize();
        }

        //filter expenses by class code
        if ($expensesSectionFilter == 'provozni') {
            $expenseClassCodeFilter = "5";
        } else if ($expensesSectionFilter == 'investicni') {
            $expenseClassCodeFilter = "6";
        } else {
            $expenseClassCodeFilter = "5,6";
        }

        $budgetDataCollection = new \stdClass();

        if ($type == 'vydaje' || $type == 'prijmy') {
            $budgetDataCollection->sectionCode = BudgetSlugConverter::$toSectionCode[$section];
        }

        $budgetDataCollection->display = $display;
        $budgetDataCollection->sort = $sort;
        $budgetDataCollection->expensesSectionFilter = $expensesSectionFilter;
        $budgetDataCollection->expenseClassCodeFilter = $expenseClassCodeFilter;

        $budgetDataCollection->groupCollection = $this->outputModifier->alterResultBasedOnOptions($result);
        $budgetDataCollection->totalBalance = $this->commonRepository->findTotalBalanceFor($administrationUnit);
        $budgetDataCollection->shareMessages = $this->getSocialShareMessagesBasedOnTotalBalance(
            $budgetDataCollection->totalBalance,
            $administrationUnit
        );

        $budgetDataCollection->classificationTypeMark = $classificationTypeMark;
        $budgetDataCollection->sectionName = BudgetSlugConverter::$toSectionName[$section];
        $budgetDataCollection->typeName = BudgetSlugConverter::$toTypeName[$type];
        $budgetDataCollection->activeYear = $activeYear;
        $budgetDataCollection->type = $type;

        return $budgetDataCollection;
    }

    private function getSocialShareMessagesBasedOnTotalBalance($totalBalance, $administrationUnit)
    {
        if ($administrationUnit instanceof Municipality) {
            $administrationUnitRoute = "PublicBudgetFrontendBundle_municipality_timeline";
            $message = "Obec " .$administrationUnit->getName() . " v letech 2000-2013 hospodařila";
        } else if ($administrationUnit instanceof Kraj) {
            $administrationUnitRoute = "PublicBudgetFrontendBundle_kraj_detail";
            $message = "Obce v kraji " . $administrationUnit->getName() . " v letech 2000-2013 celkově hospodařily";
        }

        $shareMessages = new \stdClass();
        if ($totalBalance > 0) {
            $shareMessages->facebook =  $message . " s přebytkem ". number_format($totalBalance, 0,  ',',  ' ') . " Kč";
        } else {
            $shareMessages->facebook =  $message . " se schodkem " . number_format($totalBalance, 0,  ',',  ' ') . " Kč";
        }

        $shareMessages->twitter = $shareMessages->facebook . " URL " . $this->router->generate($administrationUnitRoute, array('slug' => $administrationUnit->getSlug()), true) . " @rozpocetverejne";

        return $shareMessages;
    }
}
<?php

namespace PublicBudget\FrontendBundle\Municipality\Detail;

class AdministrationUnitHelperDataFactory
{
    public static function getBy($administrationUnit)
    {
        $administrationUnitHelperData = new \stdClass();
        if ($administrationUnit instanceof \PublicBudget\FrontendBundle\Entity\Kraj) {
            $administrationUnitHelperData->name = 'kraj';
            $administrationUnitHelperData->budgetItemClass = 'PublicBudget\FrontendBundle\Entity\BudgetItemKrajSummary';
            $administrationUnitHelperData->budgetItemTable = 'budget_item_kraj_summary';
        } else if ($administrationUnit instanceof \PublicBudget\FrontendBundle\Entity\Municipality) {
            $administrationUnitHelperData->name = 'municipality';
            $administrationUnitHelperData->budgetItemClass = 'PublicBudget\FrontendBundle\Entity\BudgetItem';
            $administrationUnitHelperData->budgetItemTable = 'budget_item';
        }

        return $administrationUnitHelperData;
    }
}
<?php

namespace PublicBudget\FrontendBundle\Municipality\Detail;

class AjaxOutputModifier
{
    public function alterResultBasedOnOptions($data, $administrationUnit)
    {
        foreach ($data as &$row) {
            $row['value'] = $row['value'] / $administrationUnit->getPopulationFor($row['year']);
        }

        return $data;
    }
}
<?php

namespace PublicBudget\FrontendBundle\Municipality\Detail;

use PublicBudget\FrontendBundle\Municipality\Detail\BudgetSummaryCollection;

class SectionDetailOutputModifier
{
    private $municipalityPopulation = 0;

    private $negateResult = false;

    private $sortBySize = false;

    private $computeForInhabitant = false;

    public function computeForInhabitant($municipalityPopulation)
    {
        $this->computeForInhabitant = true;
        $this->municipalityPopulation = $municipalityPopulation;
    }

    public function negateResult()
    {
        $this->negateResult = true;
    }

    public function sortBySize()
    {
        $this->sortBySize = true;
    }

    public function alterResultBasedOnOptions($result)
    {
        $budgetSummaryCollection = new BudgetSummaryCollection();
        foreach ($result as $row) {
            if ($this->negateResult) {
                $value = -1 * $row['value'];
            } else {
                $value = $row['value'];
            }

            if ($this->computeForInhabitant) {
                $value = $value / $this->municipalityPopulation;
            }

            $budgetSummaryCollectionValue = new BudgetSummaryCollection($row['itemName'], $row['itemCode'], $value);
            $budgetSummaryCollection->getSummaryFor($row)->addSummary($budgetSummaryCollectionValue);
        }

        if ($this->sortBySize) {
            $budgetSummaryCollection->sortBySize();
        }

        return $budgetSummaryCollection;
    }
}
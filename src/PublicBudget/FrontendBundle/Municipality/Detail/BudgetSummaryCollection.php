<?php

namespace PublicBudget\FrontendBundle\Municipality\Detail;

use PublicBudget\FrontendBundle\Municipality\Detail\BudgetSummaryCollection;

use Countable;

class BudgetSummaryCollection
{
    public $name;

    public $code;

    public $value;

    public $parent = false;

    public $summaries = array();

    public function __construct($name = '', $code = null, $value = 0)
    {
        $this->name = $name;
        $this->code = $code;
        $this->value = $value;
    }

    public function addSummary(BudgetSummaryCollection $summary)
    {
        $this->value += $summary->value;
        $summary->parent = $this;
        $this->propagateValue($summary->value);
        if (isset($this->summaries[$summary->name])) {
            $this->summaries[$summary->name]->value += $summary->value;
        } else {
            $this->summaries[$summary->name] = $summary;
        }
    }

    private function propagateValue($value) {
        if ($this->parent) {
            $this->parent->value += $value;
            $this->parent->propagateValue($value);
        }
    }

    public function getChildrenAsArray()
    {
        $summaries = array();
        foreach ($this->summaries as $summary) {
            $summaries[$summary->name] = array(
               'value' => $summary->value,
               'code' => $summary->code,
            );
        }

        return $summaries;
    }

    public function getSummaryFor($row)
    {
        if (!isset($this->summaries[$row['subgroupName']])) {
            $this->addSummary(new BudgetSummaryCollection($row['subgroupName'], $row['subgroupCode']));
        }

        return $this->summaries[$row['subgroupName']];
    }

    public function sortBySize()
    {
        foreach ($this->summaries as $summary) {
            $summary->sortBySize();
        }
        uasort($this->summaries, array($this, 'compareBySize'));
    }

    public function compareBySize($sum1, $sum2)
    {
        if ($sum1->value == $sum2->value) {
            return 0;
        }

        return $sum1->value > $sum2->value ? -1 : 1;
    }
}
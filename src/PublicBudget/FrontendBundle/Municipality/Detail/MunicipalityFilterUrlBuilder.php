<?php

namespace PublicBudget\FrontendBundle\Municipality\Detail;

class MunicipalityFilterUrlBuilder
{
    private $router;

    public function __construct($router)
    {
        $this->router = $router;
    }

    public function buildFrom($activeYear, $section, $type, $expenseType, $display, $populationRange)
    {
        $routeParams = array();

        if ($populationRange) {
            $routeParams['filtr_obci[population_range]'] = $populationRange;
        }

        $routeParams['filtr_obci[year]'] = $activeYear;
        if ($display == 'obyvatel') {
            $routeParams['filtr_obci[per_inhabitant]'] = 1;
        }

        $routeParams['filtr_obci[budget_item]'] = $type->getId();

        if ($type->getSlug() == 'prijmy') {
            $routeParams['filtr_obci[budget_item_income]'] = $section->getId();
        }

        if ($type->getSlug() == 'vydaje') {
            $routeParams['filtr_obci[budget_item_purpose]'] = $section->getId();

            if ($expenseType) {
                $routeParams['filtr_obci[budget_item_expense]'] = $expenseType->getId();
            }
        }

        return $this->router->generate('PublicBudgetFrontendBundle_municipality_filter', $routeParams);
    }

}


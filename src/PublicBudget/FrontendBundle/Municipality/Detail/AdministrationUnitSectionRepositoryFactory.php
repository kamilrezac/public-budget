<?php

namespace PublicBudget\FrontendBundle\Municipality\Detail;

use PublicBudget\FrontendBundle\Repository as Repository;

class AdministrationUnitSectionRepositoryFactory
{
    static function getSectionRepositoryByType($type, $entityManager, $administrationUnit, $budgetStructureLogic, $administrationUnitHelperData)
    {
        switch ($type) {
            case 'prijmy':
                $repository = new Repository\AdministrationUnitIncomeRepository($entityManager, $administrationUnit, $budgetStructureLogic, $administrationUnitHelperData);
                break;
            case 'vydaje':
                $repository = new Repository\AdministrationUnitExpenseRepository($entityManager, $administrationUnit, $budgetStructureLogic, $administrationUnitHelperData);
                break;
            case 'financovani':
                $repository = new Repository\AdministrationUnitFinancingRepository($entityManager, $administrationUnit, $budgetStructureLogic, $administrationUnitHelperData);
                break;
            case 'presuny':
                $repository = new Repository\AdministrationUnitTransfersRepository($entityManager, $administrationUnit, $budgetStructureLogic, $administrationUnitHelperData);
                break;
        }

        return $repository;
    }
}
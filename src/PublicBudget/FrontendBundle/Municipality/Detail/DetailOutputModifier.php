<?php

namespace PublicBudget\FrontendBundle\Municipality\Detail;

use PublicBudget\FrontendBundle\Municipality as Municipality;
use PublicBudget\FrontendBundle\Municipality\Detail\BudgetSummaryCollection;
use PublicBudget\FrontendBundle\Municipality\Common\BudgetClassificationCodeConverter;

class DetailOutputModifier
{
    private $computeForInhabitant = false;

    private $municipalityPopulation = 0;

    public function computeForInhabitant($municipalityPopulation)
    {
        $this->computeForInhabitant = true;
        $this->municipalityPopulation = $municipalityPopulation;
    }

    public function alterResultBasedOnOptions($result, $name)
    {
        $budgetSummaryCollection = new BudgetSummaryCollection($name);
        foreach ($result as $row) {
            $value = $row['value'];

            if ($this->computeForInhabitant) {
                $value = $value / $this->municipalityPopulation;
            }

            if (isset($row['code'])) {
                $sectionName = $name == 'Příjmy' ?
                BudgetClassificationCodeConverter::$toTypeName[$row['code']] :
                BudgetClassificationCodeConverter::$toSectorName[$row['code']];
                $budgetSummaryCollectionValue = new BudgetSummaryCollection($sectionName, $row['code'], $value);
            } else {
                $budgetSummaryCollectionValue = new BudgetSummaryCollection($row['budget_section'], null, $value);
            }

            $budgetSummaryCollection->addSummary($budgetSummaryCollectionValue);
        }

        return $budgetSummaryCollection;
    }

}
<?php

namespace PublicBudget\FrontendBundle\Municipality\Filter;

use PublicBudget\FrontendBundle\Municipality\Common\AbstractQuery;

class RowCountQuery extends AbstractQuery
{
	public function appendTo($result, $data)
    {
    	$result->rowCount = $this->entityManager->getConnection()->fetchColumn("SELECT COUNT(*) FROM budget_item_summary_value");
    }
}
<?php

namespace PublicBudget\FrontendBundle\Municipality\Filter;

class FilterResult
{
	public $rows = array();
	public $rowCount = 0;
	public $pageCount = 0;

	public $municipalityRouteData = array(
		'route' => '',
		'params' => array(),
		'fragment' => '',
	);

	public $template;

	public $median = 0;
	public $min = 0;
	public $max = 0;
	public $avg = 0;
	public $sum = 0;

	public $quantiles = array(
		'totalValue' => 0, 
		'rows' => array()
	);

	public $municipalitiesNotIncludedCount;

	public $selectedMunicipalitySummaryValue;

	public $mapData;
}
<?php

namespace PublicBudget\FrontendBundle\Municipality\Filter;

class Paginator
{
    const MUNICIPALITIES_PER_PAGE = 100;

    public function appendTo($result, $data)
    {
    	$filterQuery = $data['filterQuery'];
    	$page = $data['page'];
        $filterQuery->setLimit(($page - 1) * self::MUNICIPALITIES_PER_PAGE,self::MUNICIPALITIES_PER_PAGE);
        $result->rows = $filterQuery->execute();
        $result->pageCount = floor($result->rowCount/self::MUNICIPALITIES_PER_PAGE)+1;
    }
}
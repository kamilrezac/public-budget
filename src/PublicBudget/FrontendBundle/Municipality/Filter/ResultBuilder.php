<?php 

namespace PublicBudget\FrontendBundle\Municipality\Filter;
use PublicBudget\FrontendBundle\Municipality\Filter\FilterResult;

class ResultBuilder
{
	protected $buildClasses = array('all' => array());

	public function getResult($data, $tab)
	{
		$result = new FilterResult();
		$result->template = $this->template . '-' . $tab;
		foreach (array_merge($this->buildClasses['all'], $this->buildClasses[$tab]) as $class) {
			$class->appendTo($result, $data);
		}
		return $result;
	}
}
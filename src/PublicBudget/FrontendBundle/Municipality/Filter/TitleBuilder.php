<?php

namespace PublicBudget\FrontendBundle\Municipality\Filter;

class TitleBuilder
{
    public function buildFrom($filterData)
    {
        $message = '';
        $messageParts = array();

        if ($filterData->getBudgetItem()) {
            if ($filterData->getBudgetItem() == 'prijmy') {
                $typeName = 'Příjmy';
            } else if ($filterData->getBudgetItem() == 'vydaje') {
                $typeName = 'Výdaje';
            } else {
                $typeName = $filterData->getBudgetItem()->getName();
            }

            $level = $filterData->getBudgetItem()->getLevel();
            $level = $filterData->getBudgetItemIncome() ? $filterData->getBudgetItemIncome()->getLevel():$level;
            $level = $filterData->getBudgetItemExpense() ? $filterData->getBudgetItemExpense()->getLevel():$level;
            if ($filterData->getBudgetItemIncome() || $filterData->getBudgetItemExpense()) {         
                $summaryConfiguration = $filterData->getBudgetItemIncome() ? $filterData->getBudgetItemIncome():$filterData->getBudgetItemExpense();
                if ($level == 2) {
                    $messageParts[] = $summaryConfiguration->getName() . ' ' . strtolower($typeName);
                } else {
                    $messageParts[] = $typeName . ' na druh ' . strtolower($summaryConfiguration->getName());
                }
            } else {
                $messageParts[] = $filterData->getBudgetItem()->getName();
            }

            if ($filterData->getBudgetItemPurpose()) {
                $messageParts[] = 'na účel ' . strtolower($filterData->getBudgetItemPurpose()->getName());
            }

            if ($filterData->getPerInhabitant()) {
                $messageParts[] = 'přepočteno na obyvatele';
            }
        } else {
            $messageParts[] = "Výpis";
        }

        if ($filterData->getKraj()) {
            $messageParts[] = 'z kraje '. $filterData->getKraj()->getName();
        }

        if ($filterData->getOkres()) {
            $messageParts[] = 'z okresu '. $filterData->getOkres()->getName();
        }

        if (!$filterData->getKraj() && !$filterData->getOkres()) {
            $messageParts[] = 'pro celou ČR';
        }

        if ($filterData->getPopulationRange()) {
            $messageParts[] = 'obce velikosti '. $filterData->getPopulationRange()->getMin() . ' - '.$filterData->getPopulationRange()->getMax() . ' obyvatel';
        }

        if ($messageParts) {
            $message .= implode(' ', $messageParts);
        }

        if ($filterData->getYear()) {
            $message .= ' v roce '. $filterData->getYear();
        }

        return $message;
    }
}
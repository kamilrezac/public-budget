<?php

namespace PublicBudget\FrontendBundle\Municipality\Filter;

use PublicBudget\FrontendBundle\Municipality\Common\AbstractQuery;

class QuantileQuery extends AbstractQuery
{
    const QUANTILE_COUNT = 5;

    public function appendTo($result, $data)
    {
        $sql = "SELECT MIN(r.value) AS min, MAX(r.value) AS max, SUM(r.value) AS sum, 
        (@n - 1) DIV (@c DIV ".self::QUANTILE_COUNT.") AS quantile
FROM (SELECT municipality_id, value
        FROM budget_item_summary_value  
ORDER BY value DESC) r
INNER JOIN (
       SELECT @n:=0                           
       ,      @c:=COUNT(*)                    
       FROM (SELECT municipality_id, value
        FROM budget_item_summary_value) rr
      ) c
WHERE (@n := @n + 1)
GROUP BY quantile;";

        $quantiles = $this->entityManager->getConnection()->fetchAll($sql);
        if (isset($quantiles[self::QUANTILE_COUNT])) unset($quantiles[self::QUANTILE_COUNT]);

        $result->quantiles['totalValue'] = 0;

        foreach ($quantiles as $quantile) {
            $result->quantiles['totalValue'] += $quantile['sum'];
        }

        foreach ($quantiles as &$quantile) {
            $quantile['percent'] = ($quantile['sum'] / $result->quantiles['totalValue']) * 100;
        }

        $result->quantiles['rows'] = $quantiles;
    }
}
<?php

namespace PublicBudget\FrontendBundle\Municipality\Filter;
use PublicBudget\FrontendBundle\Municipality\Filter\ResultBuilder;

class BasicResultBuilder extends ResultBuilder
{
    public function __construct($paginator, $municipalityUrlBuilder, $rowCountQuery)
    {
    	$this->template = 'filter-basic';
    	$this->buildClasses['all'][] = $rowCountQuery;
        $this->buildClasses['list'][] = $paginator;
        $this->buildClasses['list'][] = $municipalityUrlBuilder;
    }
}
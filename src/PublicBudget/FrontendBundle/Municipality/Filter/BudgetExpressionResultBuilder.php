<?php

namespace PublicBudget\FrontendBundle\Municipality\Filter;
use PublicBudget\FrontendBundle\Municipality\Filter\ResultBuilder;

class BudgetExpressionResultBuilder extends ResultBuilder
{
    public function __construct($paginator, $municipalityUrlBuilder, $selectedMunicipalityQuery, $rowCountQuery)
    {
    	$this->template = 'filter-budget-expression';
    	$this->buildClasses['all'][] = $rowCountQuery;
        $this->buildClasses['list'][] = $paginator;
        $this->buildClasses['list'][] = $municipalityUrlBuilder;
        $this->buildClasses['list'][] = $selectedMunicipalityQuery;
    }
}
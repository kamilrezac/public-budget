<?php

namespace PublicBudget\FrontendBundle\Municipality\Filter;

class MunicipalityUrlBuilder
{
    private $router;

    public function __construct($router)
    {
        $this->router = $router;
    }

    public function appendTo($result, $data)
    {
        $routeParams = array();
        $fragment = false;
        $filterData = $data['filterData'];
        if ($filterData->getBudgetItem() && $filterData->getBudgetItem()->getSlug() == 'prijmy' && $filterData->getBudgetItemIncome()) {
            $route = 'PublicBudgetFrontendBundle_municipality_section_detail';
            $routeParams['activeYear'] = $filterData->getYear();
            if ($filterData->getBudgetItemIncome()->getLevel() == 4) {
                $section = $filterData->getBudgetItemIncome()->getParent()->getParent();
                $fragment = $filterData->getBudgetItemIncome()->getParent()->getCode();
            } else if ($filterData->getBudgetItemIncome()->getLevel() == 3) {
                $section = $filterData->getBudgetItemIncome()->getParent();
                $fragment = $filterData->getBudgetItemIncome()->getCode();
            } else {
                $section = $filterData->getBudgetItemIncome();
            }

            $routeParams['type'] = $filterData->getBudgetItem()->getSlug();
            $routeParams['section'] = $section->getSlug();

            if ($filterData->getPerInhabitant()) {
                $routeParams['zobrazeni'] = 'obyvatel';
            }
        } else if ($filterData->getBudgetItem() && $filterData->getBudgetItem()->getSlug() == 'vydaje' && $filterData->getBudgetItemPurpose()) {
            $route = 'PublicBudgetFrontendBundle_municipality_section_detail';
            $routeParams['activeYear'] = $filterData->getYear();
            if ($filterData->getBudgetItemExpense()) {
                $routeParams['vydaje'] = $filterData->getBudgetItemExpense()->getSlug();
            }
            
            if ($filterData->getBudgetItemPurpose()->getLevel() == 4) {
                $section = $filterData->getBudgetItemPurpose()->getParent()->getParent();
                $fragment = $filterData->getBudgetItemPurpose()->getParent()->getCode();
            } else if ($filterData->getBudgetItemPurpose()->getLevel() == 3) {
                $section = $filterData->getBudgetItemPurpose()->getParent();
                $fragment = $filterData->getBudgetItemPurpose()->getCode();
            } else {
                $section = $filterData->getBudgetItemPurpose();
            }

            $routeParams['type'] = $filterData->getBudgetItem()->getSlug();
            $routeParams['section'] = $section->getSlug();

            if ($filterData->getPerInhabitant()) {
                $routeParams['zobrazeni'] = 'obyvatel';
            }
        } else {
            $route = 'PublicBudgetFrontendBundle_municipality_detail';
            $routeParams['rok'] = $filterData->getYear();
        }

        $result->municipalityRouteData['route'] = $route;
        $result->municipalityRouteData['params'] = $routeParams;
        $result->municipalityRouteData['fragment'] = $fragment;
    }
}


<?php

namespace PublicBudget\FrontendBundle\Municipality\Filter;

use PublicBudget\FrontendBundle\Municipality\Common\AbstractQuery;

class MunicipalityCountQuery extends AbstractQuery
{
    public function appendTo($result, $data)
    {
    	$totalCount = $this->entityManager->getConnection()->fetchColumn("SELECT COUNT(*) FROM municipality");
    	$result->municipalitiesNotIncludedCount = $totalCount - $result->rowCount;
    }
}
<?php

namespace PublicBudget\FrontendBundle\Municipality\Filter;

use PublicBudget\FrontendBundle\Municipality\Common\AbstractQuery;

class MapQuery extends AbstractQuery
{
    const QUANTILE_COUNT = 5;

    public function appendTo($result, $data)
    {
        $sql = "SELECT m.name, m.longitude, m.latitude, 
        (@n - 1) DIV (@c DIV ".self::QUANTILE_COUNT.") AS quantile
FROM (SELECT municipality_id, value
        FROM budget_item_summary_value  
ORDER BY value DESC) r
INNER JOIN (
       SELECT @n:=0                           
       ,      @c:=COUNT(*)                    
       FROM (SELECT municipality_id, value
        FROM budget_item_summary_value) rr
      ) c
INNER JOIN municipality m
ON m.id=r.municipality_id
WHERE (@n := @n + 1);";

        $municipalities = $this->entityManager->getConnection()->fetchAll($sql);
        
        $result->mapData = $municipalities;
    }
}
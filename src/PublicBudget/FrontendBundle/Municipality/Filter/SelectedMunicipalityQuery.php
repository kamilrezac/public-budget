<?php

namespace PublicBudget\FrontendBundle\Municipality\Filter;

use PublicBudget\FrontendBundle\Municipality\Common\AbstractQuery;
use PublicBudget\FrontendBundle\Entity\BudgetItemSummaryValue;

class SelectedMunicipalityQuery extends AbstractQuery
{
	public function appendTo($result, $data)
    {
        $filterData = $data['filterData'];

        if ($filterData->getMunicipality()) {
            $result->selectedMunicipalitySummaryValue = $this->entityManager->getRepository('PublicBudgetFrontendBundle:BudgetItemSummaryValue')->findOneByMunicipality(
            	$filterData->getMunicipality()
            );

            if (!$result->selectedMunicipalitySummaryValue) {
            	$budgetItemSummaryValue = new BudgetItemSummaryValue();
            	$budgetItemSummaryValue->setMunicipality($filterData->getMunicipality());
            	$result->selectedMunicipalitySummaryValue = $budgetItemSummaryValue; 
            }
        }
    }
}
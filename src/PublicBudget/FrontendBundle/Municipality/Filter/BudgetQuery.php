<?php

namespace PublicBudget\FrontendBundle\Municipality\Filter;

use PublicBudget\FrontendBundle\Municipality\Common\AbstractQuery;

class BudgetQuery extends AbstractQuery
{
    public function createQueryFromFilterData(\PublicBudget\FrontendBundle\Form\Data\MunicipalityFilterData $filterData)
    {
        $this->initQuery($filterData);

        $this->filterByBudgetItem($filterData);

        if ($filterData->getSort()) {
            $this->sqlOrderSort = $filterData->getSort();
        }
    }

    private function initQuery($filterData)
    {
        $this->sqlColumns = array("m.id", "m.name", "m.slug", "k.name AS kraj_name", "o.name AS okres_name", "p.count AS population_count");

        $this->sqlFrom = "municipality m";

        $this->sqlJoin[] = "INNER JOIN kraj k ON k.id=m.kraj_id";
        $this->sqlJoin[] = "INNER JOIN okres o ON o.id=m.okres_id";
        $this->sqlJoin[] = "INNER JOIN population p ON p.municipality_id=m.id AND p.year=".$filterData->getYear();

        $this->sqlOrderValue = 'm.name';
        $this->sqlOrderSort = 'asc';
    }

    private function filterByBudgetItem($filterData)
    {
        $this->sqlColumns[] = 'r.value AS value';

        $this->sqlFrom = "(SELECT municipality_id, value
        FROM budget_item_summary_value) AS r";
        array_unshift($this->sqlJoin, "INNER JOIN municipality m ON m.id=r.municipality_id");
        $this->sqlOrderValue = "value";
    }
}


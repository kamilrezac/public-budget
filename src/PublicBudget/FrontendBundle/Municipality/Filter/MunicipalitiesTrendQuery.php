<?php

namespace PublicBudget\FrontendBundle\Municipality\Filter;

use PublicBudget\FrontendBundle\Municipality\Common\AbstractQuery;

class MunicipalitiesTrendQuery extends AbstractQuery
{
    public function getResult($filterData)
    {
    	if ($filterData->getPerInhabitant()) {
    		$valueSelect = "SUM(value_final)/p.count AS value";
    	} else {
    		$valueSelect = "SUM(value_final) AS value";
    	}
    	
        $this->sqlColumns = array("b.municipality_id", "b.year", $valueSelect);

        $this->sqlFrom = "budget_item b";

        if ($filterData->getPerInhabitant() || $filterData->getPopulationRange()) {
            $this->sqlJoin[] = "INNER JOIN population p ON p.municipality_id=b.municipality_id AND p.year=".$filterData->getYear();
            if ($filterData->getPopulationRange()) {
                $this->sqlWhere[] = "p.population_range_id=" . $filterData->getPopulationRange()->getId();
            }
        }

        if ($filterData->getKraj() || $filterData->getOkres()) {
        	$this->sqlJoin[] = "INNER JOIN municipality m ON b.municipality_id=m.id";
        }

        if ($filterData->getKraj()) {
            $this->sqlJoin[] = "INNER JOIN kraj k ON k.id=m.kraj_id";
            $this->sqlWhere[] = "k.id='".$filterData->getKraj()->getId()."'";
        }

        if ($filterData->getOkres()) {
            $this->sqlJoin[] = "INNER JOIN okres o ON o.id=m.okres_id";
            $this->sqlWhere[] = "o.id='".$filterData->getOkres()->getId()."'";
        }

        if ($filterData->getBudgetItemIncome()) {
            $budgetItemTypeClassification = $filterData->getBudgetItemIncome();
        } else if ($filterData->getBudgetItemExpense()) {
            $budgetItemTypeClassification = $filterData->getBudgetItemExpense();
        } else {
            $budgetItemTypeClassification = $filterData->getBudgetItem();
        }

        $this->sqlJoin[] = "INNER JOIN type_classification t ON b.type_classification_id=t.id";
        $this->sqlWhere[] = "t.id IN (".$budgetItemTypeClassification->getValue().")";

        if ($filterData->getBudgetItemPurpose()) {
            $this->sqlJoin[] = "INNER JOIN sector_classification s ON b.sector_classification_id=s.id";
            $this->sqlWhere[] = "s.id IN (".$filterData->getBudgetItemPurpose()->getValue().")";
        }

        $this->sqlWhere[] = "b.year >= 2000";
        $this->sqlWhere[] = "t.id NOT IN (".implode(',', $this->consolidation).")";
        $this->sqlWhere[] = "b.municipality_id != 289";

        $this->sqlGroup = "b.municipality_id, b.year";

        $municipalitiesWithoutException = $this->assembleQuery();
        array_pop($this->sqlWhere);
        array_pop($this->sqlWhere);
        $this->sqlWhere[] = "t.id NOT IN (".implode(',', $this->consolidationException).")";
        $this->sqlWhere[] = "b.municipality_id = 289";
        $municipalitiesWithException = $this->assembleQuery();
       	
        $sql =  "SELECT r.year, AVG(r.value) AS value FROM (" . $municipalitiesWithoutException . " UNION " . $municipalitiesWithException .") r GROUP BY r.year";

       	$data = $this->entityManager->getConnection()->fetchAll($sql);

        return $data;
    }
}
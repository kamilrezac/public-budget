<?php

namespace PublicBudget\FrontendBundle\Municipality\Filter;

use PublicBudget\FrontendBundle\Municipality\Common\AbstractQuery;

class BudgetExpressionSummaryValuesInsertQuery extends AbstractQuery
{
    private $innerSelectJoin = array();
    private $innerSelectWhere = array();
    private $innerSelectValue = '';

    private function filterByBudgetItem($filterData)
    {
        $this->assembleInnerQueryForExpressionValue($filterData);

        if ($filterData->getPerInhabitant() || $filterData->getPopulationRange()) {
            $this->innerSelectJoin[] = "INNER JOIN population p ON p.municipality_id=m.id AND p.year=".$filterData->getYear();
            if ($filterData->getPopulationRange()) {
                $this->innerSelectWhere[] = "p.population_range_id=" . $filterData->getPopulationRange()->getId();
            }
        }

        if ($filterData->getKraj()) {
            $this->innerSelectJoin[] = "INNER JOIN kraj k ON k.id=m.kraj_id";
            $this->innerSelectWhere[] = "k.id='".$filterData->getKraj()->getId()."'";
        }

        if ($filterData->getOkres()) {
            $this->innerSelectJoin[] = "INNER JOIN okres o ON o.id=m.okres_id";
            $this->innerSelectWhere[] = "o.id='".$filterData->getOkres()->getId()."'";
        }

        array_unshift($this->innerSelectJoin, "INNER JOIN budget_item b ON b.municipality_id=m.id");
        $this->innerSelectJoin[] = "INNER JOIN type_classification t ON b.type_classification_id=t.id";
        $this->innerSelectWhere[] = "b.year=".$filterData->getYear();
    }

    /**
     *  Selecting expression in subquery (Přebytek/Schodek = Příjmy - Výdaje)
     */
    private function assembleInnerQueryForExpressionValue($filterData)
    {
        $expression = $filterData->getBudgetItem()->getValue();
        preg_match_all('!\d+!', $expression, $matches);
        $budgetItemSummaryConfigurationIds = $matches[0];
        $caseStatement = array();
        $expressionValues = array();
        foreach ($budgetItemSummaryConfigurationIds as $budgetItemSummaryConfigurationId) {
            $classificationIds = $this->entityManager->getConnection()->fetchColumn("SELECT value FROM budget_item_summary_configuration WHERE id=".$budgetItemSummaryConfigurationId);
            if ($filterData->getPerInhabitant()) {
                $caseStatement[] = "SUM(CASE WHEN t.id IN (".$classificationIds.") THEN b.value_final ELSE 0 END)/p.count AS value".$budgetItemSummaryConfigurationId;
            } else {
                $caseStatement[] = "SUM(CASE WHEN t.id IN (".$classificationIds.") THEN b.value_final ELSE 0 END) AS value".$budgetItemSummaryConfigurationId;
            }
            $expressionValues[] = 'r.value'.$budgetItemSummaryConfigurationId;
        }
        $expression = str_replace($budgetItemSummaryConfigurationIds,$expressionValues,$expression);
        $this->innerSelectExpression = $expression;
        $this->innerSelectValue = implode(', ', $caseStatement);
    }

    public function insertBudgetItemSummaryValues($filterData)
    {
        $this->filterByBudgetItem($filterData);

        $sql = "TRUNCATE budget_item_summary_value; 
        INSERT INTO budget_item_summary_value 
        SELECT municipality_id, ".$this->innerSelectExpression." 
        FROM (
            SELECT m.id AS municipality_id, ".$this->innerSelectValue."
            FROM municipality m
            ".implode(' ', $this->innerSelectJoin)."
            WHERE ".implode(' AND ', $this->innerSelectWhere)." 
            AND t.id NOT IN (".implode(',',$this->consolidation).") AND m.id!=289
            GROUP BY m.id
            UNION 
            SELECT m.id AS municipality_id, ".$this->innerSelectValue."
            FROM municipality m
            ".implode(' ', $this->innerSelectJoin)."
            WHERE ".implode(' AND ', $this->innerSelectWhere)." 
            AND t.id NOT IN (".implode(',',$this->consolidationException).") AND m.id=289
            GROUP BY m.id
        ) r";

        $this->entityManager->getConnection()->executeQuery($sql);
    }
}


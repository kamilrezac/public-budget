<?php

namespace PublicBudget\FrontendBundle\Municipality\Filter;

use PublicBudget\FrontendBundle\Municipality\Common\AbstractQuery;

class BudgetItemSummaryValuesInsertQuery extends AbstractQuery
{
    private $innerSelectJoin = array();
    private $innerSelectWhere = array();
    private $innerSelectValue = '';

    private function filterByBudgetItem($filterData)
    {
        $this->assembleInnerQueryForClassificationValue($filterData);

        if ($filterData->getPerInhabitant() || $filterData->getPopulationRange()) {
            $this->innerSelectJoin[] = "INNER JOIN population p ON p.municipality_id=m.id AND p.year=".$filterData->getYear();
            if ($filterData->getPopulationRange()) {
                $this->innerSelectWhere[] = "p.population_range_id=" . $filterData->getPopulationRange()->getId();
            }
        }

        if ($filterData->getKraj()) {
            $this->innerSelectJoin[] = "INNER JOIN kraj k ON k.id=m.kraj_id";
            $this->innerSelectWhere[] = "k.id='".$filterData->getKraj()->getId()."'";
        }
        
        if ($filterData->getOkres()) {
            $this->innerSelectJoin[] = "INNER JOIN okres o ON o.id=m.okres_id";
            $this->innerSelectWhere[] = "o.id='".$filterData->getOkres()->getId()."'";
        }

        array_unshift($this->innerSelectJoin, "INNER JOIN budget_item b ON b.municipality_id=m.id");
        $this->innerSelectJoin[] = "INNER JOIN type_classification t ON b.type_classification_id=t.id";
        $this->innerSelectWhere[] = "b.year=".$filterData->getYear();
    }

    /**
     *  Selecting single budget classification value in subquery (Příjmy daňové)
     */
    private function assembleInnerQueryForClassificationValue($filterData)
    {
        if ($filterData->getBudgetItemIncome()) {
            $budgetItemTypeClassification = $filterData->getBudgetItemIncome();
        } else if ($filterData->getBudgetItemExpense()) {
            $budgetItemTypeClassification = $filterData->getBudgetItemExpense();
        } else {
            $budgetItemTypeClassification = $filterData->getBudgetItem();
        }

        $this->innerSelectWhere[] = "t.id IN (".$budgetItemTypeClassification->getValue().")";

        if ($filterData->getBudgetItemPurpose()) {
            $this->innerSelectJoin[] = "INNER JOIN sector_classification s ON b.sector_classification_id=s.id";
            $this->innerSelectWhere[] = "s.id IN (".$filterData->getBudgetItemPurpose()->getValue().")";
        }

        $this->innerSelectValue = 'SUM(b.value_final)';
        if ($filterData->getPerInhabitant()) {
            $this->innerSelectValue = $this->innerSelectValue.'/p.count';
        }
        $this->innerSelectValue = $this->innerSelectValue . ' AS value';
    }

    public function insertBudgetItemSummaryValues($filterData)
    {
        $this->filterByBudgetItem($filterData);

        $sql = "INSERT INTO budget_item_summary_value
        SELECT m.id AS municipality_id, ".$this->innerSelectValue."
        FROM municipality m
        ".implode(' ', $this->innerSelectJoin)."
        WHERE ".implode(' AND ', $this->innerSelectWhere)." 
        AND t.id NOT IN (".implode(',',$this->consolidation).") AND m.id!=289
        GROUP BY m.id
        HAVING value!=0
        UNION 
        SELECT m.id AS municipality_id, ".$this->innerSelectValue."
        FROM municipality m
        ".implode(' ', $this->innerSelectJoin)."
        WHERE ".implode(' AND ', $this->innerSelectWhere)." 
        AND t.id NOT IN (".implode(',',$this->consolidationException).") AND m.id=289
        GROUP BY m.id
        HAVING value!=0;";

        $this->entityManager->getConnection()->executeQuery("TRUNCATE budget_item_summary_value;");
        $this->entityManager->getConnection()->executeQuery($sql);
    }
}


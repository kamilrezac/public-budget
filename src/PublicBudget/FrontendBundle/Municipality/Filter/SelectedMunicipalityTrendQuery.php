<?php

namespace PublicBudget\FrontendBundle\Municipality\Filter;

use PublicBudget\FrontendBundle\Municipality\Common\AbstractQuery;

class SelectedMunicipalityTrendQuery extends AbstractQuery
{
    public function getResult($filterData)
    {
        $municipality = $filterData->getMunicipality();
        $budgetStructureLogic = $this->budgetStructureLogicFactory->getBy($municipality);
        $consolidation = array_merge($budgetStructureLogic->expenseConsolidationById,$budgetStructureLogic->incomeConsolidationById);

        if ($filterData->getPerInhabitant()) {
            $valueSelect = "SUM(value_final)/p.count AS value";
        } else {
            $valueSelect = "SUM(value_final) AS value";
        }

        $this->sqlColumns = array("b.year", $valueSelect);

        $this->sqlFrom = "budget_item b";

        if ($filterData->getPerInhabitant()) {
            $this->sqlJoin[] = "INNER JOIN population p ON p.municipality_id=b.municipality_id AND p.year=".$filterData->getYear();
        }

        if ($filterData->getBudgetItemIncome()) {
            $budgetItemTypeClassification = $filterData->getBudgetItemIncome();
        } else if ($filterData->getBudgetItemExpense()) {
            $budgetItemTypeClassification = $filterData->getBudgetItemExpense();
        } else {
            $budgetItemTypeClassification = $filterData->getBudgetItem();
        }

        $this->sqlJoin[] = "INNER JOIN type_classification t ON b.type_classification_id=t.id";
        $this->sqlWhere[] = "t.id IN (".$budgetItemTypeClassification->getValue().")";

        if ($filterData->getBudgetItemPurpose()) {
            $this->sqlJoin[] = "INNER JOIN sector_classification s ON b.sector_classification_id=s.id";
            $this->sqlWhere[] = "s.id IN (".$filterData->getBudgetItemPurpose()->getValue().")";
        }

        $this->sqlWhere[] = "b.municipality_id = ".$municipality->getId();
        $this->sqlWhere[] = "t.id NOT IN (".implode(',', $consolidation).")";
        $this->sqlWhere[] = "b.year >= 2000";

        $this->sqlGroup = "b.year";

        $data = $this->execute();

        return $data;
    }
}
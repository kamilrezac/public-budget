<?php

namespace PublicBudget\FrontendBundle\Municipality\Filter;
use PublicBudget\FrontendBundle\Municipality\Filter\ResultBuilder;

class BudgetItemResultBuilder extends ResultBuilder
{
    public function __construct(
    	$paginator, 
    	$quantileQuery, 
    	$municipalityUrlBuilder, 
    	$municipalityCountQuery, 
    	$medianQuery, 
    	$minMaxAvgQuery,
    	$selectedMunicipalityQuery,
        $rowCountQuery,
        $mapQuery
    ) {
        $this->template = 'filter-budget-item';
        $this->buildClasses['all'][] = $rowCountQuery;
        $this->buildClasses['list'][] = $paginator;
        $this->buildClasses['list'][] = $municipalityUrlBuilder;
        $this->buildClasses['list'][] = $selectedMunicipalityQuery;
        $this->buildClasses['summary'][] = $quantileQuery;
        $this->buildClasses['summary'][] = $municipalityCountQuery;
        $this->buildClasses['summary'][] = $medianQuery;
        $this->buildClasses['summary'][] = $minMaxAvgQuery;
        $this->buildClasses['map'][] = $mapQuery;
        
    }
}
<?php

namespace PublicBudget\FrontendBundle\Municipality\Filter;

use PublicBudget\FrontendBundle\Municipality\Common\AbstractQuery;

class MinMaxAvgSumQuery extends AbstractQuery
{
    public function appendTo($result, $data)
    {
        $sql = "SELECT AVG(value) AS avg
,          MIN(value) AS min 
,          MAX(value) AS max
,          SUM(value) AS sum
FROM budget_item_summary_value
";
        $data = $this->entityManager->getConnection()->fetchAssoc($sql);

        $result->min = $data['min'];
        $result->max = $data['max'];
        $result->avg = $data['avg'];
        $result->sum = $data['sum'];
    }
}
<?php

namespace PublicBudget\FrontendBundle\Municipality\Filter;

use PublicBudget\FrontendBundle\Municipality\Common\AbstractQuery;

class BasicQuery extends AbstractQuery
{
    public function createQueryFromFilterData(\PublicBudget\FrontendBundle\Form\Data\MunicipalityFilterData $filterData)
    {
        $this->initQuery($filterData);

        if ($filterData->getKraj()) {
            $this->sqlWhere[] = "k.id=" . $filterData->getKraj()->getId();
        }

        if ($filterData->getOkres()) {
            $this->sqlWhere[] = "o.id=" . $filterData->getOkres()->getId();
        }
    
        if ($filterData->getPopulationRange()) {
            $this->sqlWhere[] = "p.population_range_id=" . $filterData->getPopulationRange()->getId();
        }

        if ($filterData->getSort()) {
            $this->sqlOrderSort = $filterData->getSort();
        }
    }

    private function initQuery($filterData)
    {
        $this->sqlColumns = array("m.id", "m.name", "m.slug", "k.name AS kraj_name", "o.name AS okres_name", "p.count AS population_count");

        $this->sqlFrom = "municipality m";

        $this->sqlJoin[] = "INNER JOIN kraj k ON k.id=m.kraj_id";
        $this->sqlJoin[] = "INNER JOIN okres o ON o.id=m.okres_id";
        $this->sqlJoin[] = "INNER JOIN population p ON p.municipality_id=m.id AND p.year=".$filterData->getYear();

        $this->sqlOrderValue = 'm.name';
        $this->sqlOrderSort = 'asc';
    }
}
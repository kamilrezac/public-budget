<?php

namespace PublicBudget\FrontendBundle\Municipality\Filter;

use PublicBudget\FrontendBundle\Municipality\Common\AbstractQuery;

class MedianQuery extends AbstractQuery
{
    public function appendTo($result,$data)
    {
        $sql = "SELECT     AVG(value) AS median
,          MIN(value) AS left_median 
,          MAX(value) AS right_median
,          @l          AS left_median_position
,          @r          AS right_median_position
FROM (
     SELECT     @n, value
     FROM       budget_item_summary_value
     CROSS JOIN (
                SELECT @n:=0
                ,      @r := COUNT(*) DIV 2 + 1
                ,      @l := COUNT(*) DIV 2
                           + IF(
                               COUNT(*) % 2
                             , 1
                             , 0
                             )
                FROM budget_item_summary_value
                ) ``
WHERE      (@n:=@n+1)
BETWEEN    @l AND @r
ORDER BY   value
) ``";

        $result->median = $this->entityManager->getConnection()->fetchColumn($sql);
    }
}
<?php

namespace PublicBudget\FrontendBundle\Municipality\Compare;

use PublicBudget\FrontendBundle\Form\Data\MunicipalityCompareData;
use PublicBudget\FrontendBundle\Municipality\Common\AbstractQuery;

class ClassificationQuery
{
    private $entityManager;

    public function __construct($entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function createQueryFromData(MunicipalityCompareData $municipalityCompareData)
    {
        $andWhereParent = '';
        if ($municipalityCompareData->getBudgetItemParent()) {
            $whereClause = " AND parent_id=".$municipalityCompareData->getBudgetItemParent()->getId();
        } else {
            $whereClause = " AND level=".$municipalityCompareData->getBudgetItemLevel();
        }

        $query = "SELECT slug, value, name
            FROM budget_item_summary_configuration
            WHERE type='sector_classification'".
            $whereClause;

        return $this->entityManager->getConnection()->fetchAll($query);
    }
}
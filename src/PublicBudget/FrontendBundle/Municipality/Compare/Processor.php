<?php

namespace PublicBudget\FrontendBundle\Municipality\Compare;

class Processor
{
    public function compare($municipality1Name, $municipality2Name, $municipality1Result, $municipality2Result, $classificationIds)
    {
        $result = array();
        $result1 = array();
        $result2 = array();
        $maxValue1 = 0;
        $maxValue2 = 0;
        foreach ($classificationIds as $classificationId) {
            $slug = $classificationId['slug'];
            $value1 = round($municipality1Result[$slug],2);
            $value2 = round($municipality2Result[$slug],2);

            if ($value1 == 0 && $value2 == 0) {
                continue;
            }

            $difference = $value1 - $value2;

            $resultTemp = array();
            $resultTemp['difference'] = $difference > 0 ? $difference:-1*$difference;
            $resultTemp['municipality1Value'] = $value1;
            $resultTemp['municipality2Value'] = $value2;
            $resultTemp['name'] = $classificationId['name'];

            if ($difference > 0) {
                $maxValue1 = $difference > $maxValue1 ? $difference:$maxValue1;
                $resultTemp['percent'] = $this->computePercentDifference($difference, $value2);
                $result1[] = $resultTemp;
            } else {
                $difference = -1 * $difference;
                $maxValue2 = $difference > $maxValue2 ? $difference:$maxValue2;
                $resultTemp['percent'] = $this->computePercentDifference($difference, $value1);
                $result2[] = $resultTemp;
            }
        }
        $result['maxValue1'] = $maxValue1;
        $result['maxValue2'] = $maxValue2;
        usort($result1, array($this, 'sortResult'));
        usort($result2, array($this, 'sortResult'));
        $result['result1'] = $result1;
        $result['result2'] = $result2;
        return $result;
    }

    private function computePercentDifference($difference, $value)
    {
        $percentDifference = $value > 0 ? round(($difference/$value) * 100):round($difference*100);
        return $percentDifference <= 1000 ? $percentDifference:'1000+';
    }

    private function sortResult($a, $b)
    {
        if ($a['difference'] == $b['difference']) {
            return 0;
        }

        return $a['difference'] > $b['difference'] ? -1:1;
    }
}
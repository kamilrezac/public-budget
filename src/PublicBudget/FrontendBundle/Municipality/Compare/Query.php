<?php

namespace PublicBudget\FrontendBundle\Municipality\Compare;

use PublicBudget\FrontendBundle\Form\Data\MunicipalityCompareData;
use PublicBudget\FrontendBundle\Municipality\Common\AbstractQuery;

class Query extends AbstractQuery
{
    public function createQueryFromData($municipality, MunicipalityCompareData $municipalityCompareData, $classificationIds, $budgetStructureLogic)
    {

        foreach ($classificationIds as $classificationId) {
            if ($municipalityCompareData->getPerInhabitant()) {
                $this->sqlColumns[] = "SUM(IF(b.sector_classification_id IN (".$classificationId['value']."),b.value_final,0))/".$municipality->getPopulationFor($municipalityCompareData->getYear())." AS '" . $classificationId['slug'] ."'\n";
            } else {
                $this->sqlColumns[] = "SUM(IF(b.sector_classification_id IN (".$classificationId['value']."),b.value_final,0)) AS '" . $classificationId['slug'] ."'\n";
            }
        }

        $this->sqlFrom = "budget_item b";
        $this->sqlJoin[] = "INNER JOIN type_classification t ON b.type_classification_id=t.id";

        $this->sqlWhere[] = "t.id NOT IN (".implode(',', $budgetStructureLogic->expenseConsolidationById).")";

        if ($municipalityCompareData->getBudgetItemExpense()) {
            $this->sqlWhere[] = "t.id IN (".$municipalityCompareData->getBudgetItemExpense()->getValue().")";
        } else { // all expenses
            $this->sqlWhere[] = "t.id IN (200,201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216,217,218,219,220,221,222,223,224,225,226,227,228,229,230,231,232,233,234,235,236,237,238,239,240,241,242,243,244,245,246,247,248,249,250,251,252,253,254,255,256,257,258,259,260,261,262,263,264,265,266,267,268,269,270,271,272,273,274,275,276,277,278,279,280,281,282,283,284,285,286,287,288,289,290,291,292,293,294,295,296,297,298,299,300,301,302,303,304,305,306,307,308,309,310,311,312,313,314,315,316,317,318,319,320,321,322,323,324,325,326,327,328,329,330,331,332,333,334,335,336,337,338,339,340,341,342,343,344,345,346,347,348,349,350,351,352,353,354,355,356,357,358,359,360,361,362,363,364,365,366,367,368,369,370,371,372,373,374,375,376,377,378,379,380,381,382,383,384,385,386,387,388,389,390,391,392,393,394,395,396,397,398,399,400,401,402,403,404,405,406,407,408,409,410,411,412,413,414,415,416,417,418,419,420,421,422,423,424,425,426,427,428,429,430,431,432,433,434,435,436,437,438,439,440,441,442,443,444,445,446,447,448,449,450,451,452,453,454,455,456,457,458,459,460,511,512,513,514,515,516,517,518,519,520,521,522,523,524,525,526,527,528,529,530,531,532,533,534,573,574)";
        }

        $this->sqlWhere[] = "b.year=".$municipalityCompareData->getYear()." AND municipality_id = ".$municipality->getId();

        return $this;
    }
}
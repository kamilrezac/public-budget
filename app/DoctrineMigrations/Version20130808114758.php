<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20130808114758 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "mysql", "Migration can only be executed safely on 'mysql'.");

        $this->addSql("ALTER TABLE population ADD population_range_id INT DEFAULT NULL");
        $this->addSql("ALTER TABLE population ADD CONSTRAINT FK_B449A008451FB18C FOREIGN KEY (population_range_id) REFERENCES population_range (id)");
        $this->addSql("CREATE INDEX IDX_B449A008451FB18C ON population (population_range_id)");
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "mysql", "Migration can only be executed safely on 'mysql'.");

        $this->addSql("ALTER TABLE population DROP FOREIGN KEY FK_B449A008451FB18C");
        $this->addSql("DROP INDEX IDX_B449A008451FB18C ON population");
        $this->addSql("ALTER TABLE population DROP population_range_id");
    }
}

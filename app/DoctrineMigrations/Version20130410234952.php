<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20130410234952 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "mysql", "Migration can only be executed safely on 'mysql'.");

        $this->addSql("CREATE INDEX budget_item_id ON budget_item (id)");
        $this->addSql("ALTER TABLE budget_item DROP PRIMARY KEY");
        $this->addSql("ALTER TABLE budget_item CHANGE municipality_id municipality_id INT NOT NULL, CHANGE type_classification_id type_classification_id INT NOT NULL, CHANGE sector_classification_id sector_classification_id INT NOT NULL, CHANGE id id INT NOT NULL");
        $this->addSql("ALTER TABLE budget_item ADD PRIMARY KEY (municipality_id, year, type_classification_id, sector_classification_id)");
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "mysql", "Migration can only be executed safely on 'mysql'.");

        $this->addSql("ALTER TABLE budget_item DROP PRIMARY KEY");
        $this->addSql("ALTER TABLE budget_item CHANGE sector_classification_id sector_classification_id INT NOT NULL, CHANGE type_classification_id type_classification_id INT NOT NULL, CHANGE municipality_id municipality_id INT NOT NULL, CHANGE id id INT AUTO_INCREMENT NOT NULL");
        $this->addSql("ALTER TABLE budget_item ADD PRIMARY KEY (id)");
        $this->addSql("DROP INDEX budget_item_id ON budget_item");
    }
}

SET CHARACTER SET 'utf8';
SET foreign_key_checks=0;
SET unique_checks=0;
SET sql_log_bin=0;

LOAD DATA LOCAL INFILE 'population_2013_transformed.csv' INTO TABLE `population`
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
LINES TERMINATED BY '\n'
(municipality_id,year,count,population_range_id);

SET foreign_key_checks=1;
SET unique_checks=1;
SET sql_log_bin=1;
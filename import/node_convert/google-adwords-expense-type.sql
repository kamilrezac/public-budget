SELECT 'Headline','Description Line 1','Description Line 2','Display URL','Destination URL','Keyword'
UNION
(SELECT c.name,
		'Kolik za ně vydá',
		'vaše obec?',
		'http://www.rozpocetobce.cz/filtr-obci',
		CONCAT('http://www.rozpocetobce.cz/filtr-obci?filtr_obci%5Bbudget_item%5D=282&filtr_obci%5Bbudget_item_income%5D=&filtr_obci%5Bbudget_item_expense%5D=',c.id,'&filtr_obci%5Bbudget_item_purpose%5D=&filtr_obci%5Bper_inhabitant%5D=0&filtr_obci%5Bkraj%5D=&filtr_obci%5Bpopulation_range%5D=&filtr_obci%5Byear%5D=2012&filtr_obci%5Bsort%5D=desc&utm_source=Google&utm_medium=googlegrants&utm_content=VydajeDruhy&utm_campaign=VydajeDruhy'),
		CONCAT('výdaje obce ', LOWER(c.name))
INTO OUTFILE '/Users/kamilrezac/Work/public-budget/adwords/expense-type.csv'
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
ESCAPED BY '\\'
LINES TERMINATED BY '\n'
FROM budget_item_summary_configuration c
WHERE c.id > 282 && c.id < 647);
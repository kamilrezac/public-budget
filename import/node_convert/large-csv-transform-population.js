var csv = require('csv');
var fs = require('fs')

var municipalities = []
var skipCounter = 1;
var populationRange
var count

csv()
.from.path('../ruby_convert/municipality.csv')
.on('record',function(data,index){
    municipalities[data[2]] = data[0]
})
.on('end',function(count){
  csv()
  .from.stream(fs.createReadStream('population_2013.csv', {flags: 'r'}))
  .to.stream(fs.createWriteStream('population_2013_transformed.csv', {flags: 'w'}))
  .transform(function(data,index){
     count = data[3]

     if (between(count, 1, 100)) {
        populationRange = 1
     } else if (between(count, 101, 200)) {
        populationRange = 2
     } else if (between(count, 201,500)) {
        populationRange = 3
     } else if (between(count, 501,1000)) {
        populationRange = 4
     } else if (between(count, 1001,2000)) {
        populationRange = 5
     } else if (between(count, 2001,5000)) {
        populationRange = 6
     } else if (between(count, 5001,10000)) {
        populationRange = 7
     } else if (between(count, 10001,20000)) {
        populationRange = 8
     } else if (between(count, 20001,50000)) {
        populationRange = 9
     } else if (between(count, 50001,100000)) {
        populationRange = 10
     } else if (between(count, 100001,2000000)) {
        populationRange = 11
     }

     if (municipalities[data[1]]) {
         return [municipalities[data[1]], 2013, count, populationRange]
     } else {
         console.log(skipCounter, data[2])
         skipCounter++
     }
  })
})

function between(x, min, max) {
  return x >= min && x <= max;
}




SELECT 'Název obce','CSU kód','IČO organizace','Název okresu','Název kraje',
'Třída kód','Třída název','Skupina kód','Skupina název','Podskupina kód',
'Podskupina název','Položka kód','Položka název','Kategorie kód','Kategorie název',
'Sekce kód','Sekce název','Subsekce kód','Subsekce název','Paragraf kód',
'Paragraf název','Rok','Počet obyvatel','Hodnota'
UNION
(SELECT m.name,
m.csu_code,
m.organization_ico,
o.name AS okres_name,
k.name AS kraj_name,
tc.class_code,
tc.class_name,
tc.group_code,
tc.group_name,
tc.subgroup_code,
tc.subgroup_name,
tc.item_code,
tc.item_name,
sc.category_code,
sc.category_name,
sc.section_code,
sc.section_name,
sc.subsection_code,
sc.subsection_name,
sc.paragraph_code,
sc.paragraph_name,
b.year,
p.count,
b.value_final
INTO OUTFILE '/Users/kamilrezac/Work/public-budget/data/vsechna-data-2012.csv'
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
ESCAPED BY '\\'
LINES TERMINATED BY '\n'
FROM budget_item b
JOIN population p
ON p.year=b.year AND p.municipality_id=b.municipality_id
JOIN type_classification tc
ON tc.id=b.type_classification_id
JOIN sector_classification sc
ON sc.id=b.sector_classification_id
JOIN municipality m
ON m.id=b.municipality_id
JOIN okres o
ON o.id=m.okres_id
JOIN kraj k
ON k.id=m.kraj_id
WHERE b.year=2012
ORDER BY tc.item_code,b.year);
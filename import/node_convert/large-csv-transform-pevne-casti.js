var csv = require('csv');
var fs = require('fs')

var municipalities = []
var sectorClassification = []
var typeClassification = []
var skipCounter = 1;
var valueApproved;
var valueAfterChanges;
var valueFinal;

csv()
.from.path('../ruby_convert/municipality.csv')
.on('record',function(data,index){
    municipalities[data[9]] = data[0]
})
.on('end',function(count){
    csv()
    .from.path('../ruby_convert/sector_classification.csv')
    .on('record',function(data,index){
        sectorClassification[data[7]] = data[0]
    })
    .on('end',function(count){
        csv()
        .from.path('../ruby_convert/type_classification.csv')
        .on('record',function(data,index){
            typeClassification[data[7]] = data[0]
        })
        .on('end',function(count){
           csv()
           .from.stream(fs.createReadStream('FINM202.csv', {flags: 'r'}), {delimiter:";"})
           .to.stream(fs.createWriteStream('budget-item-transformed-pevne-casti.csv', {flags: 'w'}))
           .transform(function(data,index){
              if (index == 0) return;

              ico = data[4];

              ico.concat(Array(ico.length + 1).join("0"), ico)

              classificationExist = typeClassification[data[7]]
              var minus = new RegExp('-');

              if(minus.test(data[8])){
                  valueApproved = data[8].replace(' ', '').replace('-','')
                  valueApproved = -1 * Math.round(valueApproved)
              } else {
                  valueApproved = data[8].replace(' ', '')
                  valueApproved = Math.round(valueApproved)
              }

              if(minus.test(data[9])){
                  valueAfterChanges = data[9].replace(' ', '').replace('-','')
                  valueAfterChanges = -1 * Math.round(valueAfterChanges)
              } else {
                  valueAfterChanges = data[9].replace(' ', '')
                  valueAfterChanges = Math.round(valueAfterChanges)
              }

              if(minus.test(data[10])){
                  valueFinal = data[10].replace(' ', '').replace('-','')
                  valueFinal = -1 * Math.round(valueFinal)
              } else {
                  valueFinal = data[10].replace(' ', '')
                  valueFinal = Math.round(valueFinal)
              }

              if (!valueApproved && !valueAfterChanges && !valueFinal) {
                  return
              }


              if (municipalities[ico] && classificationExist) {
                  return [540,typeClassification[data[7]], valueApproved, valueAfterChanges, valueFinal, 3, municipalities[ico], 2013]
              } else {
                  console.log(skipCounter, ico, classificationExist)
                  skipCounter++
              }
           })
        })
    })
})





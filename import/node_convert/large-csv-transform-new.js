var csv = require('csv');
var fs = require('fs')

var municipalities = []
var sectorClassification = []
var typeClassification = []
var skipCounter = 1;
var sectorClassificationValue;
var type
var ico
var classificationExist;
var valueApproved;
var valueAfterChanges;
var valueFinal;

csv()
.from.path('../ruby_convert/municipality.csv')
.on('record',function(data,index){
    municipalities[data[9]] = data[0]
})
.on('end',function(count){
    csv()
    .from.path('../ruby_convert/sector_classification.csv')
    .on('record',function(data,index){
        sectorClassification[data[7]] = data[0]
    })
    .on('end',function(count){
        csv()
        .from.path('../ruby_convert/type_classification.csv')
        .on('record',function(data,index){
            typeClassification[data[7]] = data[0]
        })
        .on('end',function(count){
           csv()
           .from.stream(fs.createReadStream('FINM201.csv', {flags: 'r'}), {delimiter: ';'})
           .to.stream(fs.createWriteStream('budget-item-transformed.csv', {flags: 'w'}))
           .transform(function(data,index){
              if (index == 0) return;

              if (data[8] == "0000") {
                  sectorClassificationValue = 540
              } else {
                  sectorClassificationValue = sectorClassification[data[8]];
              }

              if (data[7] == "2") {
                  type = 1
              } else {
                  type = 2
              }
                   var minus = new RegExp('-');

           if(minus.test(data[10])){
               valueApproved = data[10].replace(' ', '').replace('-','')
               valueApproved = -1 * Math.round(valueApproved)
           } else {
               valueApproved = data[10].replace(' ', '')
               valueApproved = Math.round(valueApproved)
           }

           if(minus.test(data[11])){
               valueAfterChanges = data[11].replace(' ', '').replace('-','')
               valueAfterChanges = -1 * Math.round(valueAfterChanges)
           } else {
               valueAfterChanges = data[11].replace(' ', '')
               valueAfterChanges = Math.round(valueAfterChanges)
           }

           if(minus.test(data[12])){
               valueFinal = data[12].replace(' ', '').replace('-','')
               valueFinal = -1 * Math.round(valueFinal)
           } else {
               valueFinal = data[12].replace(' ', '')
               valueFinal = Math.round(valueFinal)
           }

              ico = data[4];

              ico.concat(Array(ico.length + 1).join("0"), ico)

              classificationExist = typeClassification[data[9]] && sectorClassificationValue != "undefined"
              if (municipalities[ico] && classificationExist) {
                  return [sectorClassificationValue,typeClassification[data[9]], valueApproved, valueAfterChanges, valueFinal, type, municipalities[ico], 2013]
              } else {
                  console.log(skipCounter, ico, classificationExist)
                  skipCounter++
              }
           })
        })
    })
})





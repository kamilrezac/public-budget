SELECT 'Název obce','CSU kód','IČO organizace','Název okresu','Název kraje','Počet obyvatel','Pásmo počtu obyvatel',
'Rok','Příjmy','Výdaje','Daňové příjmy','Nedaňové příjmy','Kapitálové příjmy','Dotace','Zemědělství','Průmysl',
'Služby','Sociální věci','Bezpečnost','Veřejná správa','Schodek/Přebytek','Přesuny-příjmy','Přesuny-výdaje'
UNION
(SELECT m.name,
m.csu_code,
m.organization_ico,
o.name AS okres_name,
k.name AS kraj_name,
p.count,
CASE
WHEN p.count BETWEEN 0 AND 100 then '01. 0 až 100 obyvatel'
WHEN p.count BETWEEN 101 AND 200 then '02. 101 až 200 obyvatel'
WHEN p.count BETWEEN 201 AND 500 then '03. 201 až 500 obyvatel'
WHEN p.count BETWEEN 501 AND 1000 then '04. 501 až 1000 obyvatel'
WHEN p.count BETWEEN 1001 AND 2000 then '05. 1001 až 2000 obyvatel'
WHEN p.count BETWEEN 2001 AND 5000 then '06. 2001 až 5000 obyvatel'
WHEN p.count BETWEEN 5001 AND 10000 then '07. 5001 až 10000 obyvatel'
WHEN p.count BETWEEN 10001 AND 20000 then '08. 10001 až 20000 obyvatel'
WHEN p.count BETWEEN 20001 AND 50000 then '09. 20001 až 50000 obyvatel'
WHEN p.count BETWEEN 50001 AND 100000 then '10. 50001 až 100000 obyvatel'
WHEN p.count BETWEEN 100001 AND 2000000000 then '11. 100001 až 2000000000 obyvatel'
END population_zone,
b.year,
SUM(IF(t.id = 1 AND tc.item_code NOT IN ('4133','4134','4139'), b.value_final, 0)) AS income,
SUM(IF(t.id = 2 AND tc.item_code NOT IN ('5342','5344','5345','5349'), b.value_final, 0)) AS expense,
SUM(IF(tc.class_code = 1, b.value_final, 0)) AS danoveprijmy,
SUM(IF(tc.class_code = 2, b.value_final, 0)) AS nedanoveprijmy,
SUM(IF(tc.class_code = 3, b.value_final, 0)) AS kapitaloveprijmy,
SUM(IF(tc.class_code = 4 AND tc.item_code NOT IN ('4133','4134','4139'), b.value_final, 0)) AS dotace,
SUM(IF(sc.category_code = 1 AND tc.class_code IN (5,6) AND tc.item_code NOT IN ('5342','5344','5345','5349'), b.value_final, 0)) AS zemedelstvi,
SUM(IF(sc.category_code = 2 AND tc.class_code IN (5,6) AND tc.item_code NOT IN ('5342','5344','5345','5349'), b.value_final, 0)) AS prumysl,
SUM(IF(sc.category_code = 3 AND tc.class_code IN (5,6) AND tc.item_code NOT IN ('5342','5344','5345','5349'), b.value_final, 0)) AS sluzby,
SUM(IF(sc.category_code = 4 AND tc.class_code IN (5,6) AND tc.item_code NOT IN ('5342','5344','5345','5349'), b.value_final, 0)) AS socialniveci,
SUM(IF(sc.category_code = 5 AND tc.class_code IN (5,6) AND tc.item_code NOT IN ('5342','5344','5345','5349'), b.value_final, 0)) AS bezpecnost,
SUM(IF(sc.category_code = 6 AND tc.class_code IN (5,6) AND tc.item_code NOT IN ('5342','5344','5345','5349'), b.value_final, 0)) AS verejnasprava,
SUM(IF(tc.item_code = 8000, b.value_final,0)) AS schodekprebytek,
SUM(IF(tc.item_code IN ('4133','4134','4139'), b.value_final, 0)) AS presunyprijmy,
SUM(IF(tc.item_code IN ('5342','5344','5345','5349'), b.value_final, 0)) AS presunyvydaje
INTO OUTFILE '/Users/kamilrezac/Work/public-budget/data/sumare-2012.csv'
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
ESCAPED BY '\\'
LINES TERMINATED BY '\n'
FROM budget_item b
JOIN budget_item_type t
ON t.id=b.budget_item_type_id
JOIN type_classification tc
ON tc.id=b.type_classification_id
JOIN sector_classification sc
ON sc.id=b.sector_classification_id
JOIN municipality m
ON m.id=b.municipality_id
JOIN okres o
ON o.id=m.okres_id
JOIN kraj k
ON k.id=m.kraj_id
JOIN population p
ON m.id=p.municipality_id AND p.year=2012
WHERE b.year=2012
GROUP BY b.year, m.name, m.csu_code, m.organization_ico, o.name, k.name
ORDER BY b.year);
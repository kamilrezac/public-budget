SET CHARACTER SET 'utf8';
SET foreign_key_checks=0;
SET unique_checks=0;
SET sql_log_bin=0;

LOAD DATA LOCAL INFILE 'budget-item-transformed-pevne-casti.csv' INTO TABLE `budget_item`
FIELDS TERMINATED BY ';' OPTIONALLY ENCLOSED BY '"'
LINES TERMINATED BY '\n'
(sector_classification_id,type_classification_id,value_approved,value_after_changes,value_final,budget_item_type_id,municipality_id,year);

LOAD DATA LOCAL INFILE 'budget-item-transformed.csv' INTO TABLE `budget_item`
FIELDS TERMINATED BY ';' OPTIONALLY ENCLOSED BY '"'
LINES TERMINATED BY '\n'
(sector_classification_id,type_classification_id,value_approved,value_after_changes,value_final,budget_item_type_id,municipality_id,year);

SET foreign_key_checks=1;
SET unique_checks=1;
SET sql_log_bin=1;
SET NAMES utf8;
INSERT INTO `kraj` (id,name,code) VALUES
('1','Hlavní město Praha','CZ010'),
('2','Středočeský kraj','CZ020'),
('3','Jihočeský kraj','CZ031'),
('4','Plzeňský kraj','CZ032'),
('5','Karlovarský kraj','CZ041'),
('6','Ústecký kraj','CZ042'),
('7','Liberecký kraj','CZ051'),
('8','Královéhradecký kraj','CZ052'),
('9','Pardubický kraj','CZ053'),
('10','Kraj Vysočina','CZ063'),
('11','Jihomoravský kraj','CZ064'),
('12','Olomoucký kraj','CZ071'),
('13','Zlínský kraj','CZ072'),
('14','Moravskoslezský kraj','CZ080');

require 'csv'
require 'nokogiri'
require 'open-uri'

class Float
  def round_to(x)
    (self * 10**x).round.to_f / 10**x
  end
end

def to_price string
  (string.gsub(/[^0-9,]/, "").gsub(',','.').to_f * 1000).round
end

def to_float string
  string.gsub(',','.').to_f
end

def to_integer string
  string.gsub(/[^0-9,]/, "").to_i
end

def to_percent string
  (string.gsub(',','.').to_f / 100).round_to(4)
end

def get_doc url
  begin
    doc = Nokogiri::HTML(open(url))
  rescue
    get_doc url
  end
end

csvFile = CSV.open("monitoring.csv", "w")
reader = CSV.open("../node_convert/Obce.csv", "rb")
header = reader.shift
year = 2010
reader.each_with_index do |row, index|
  # if index < 709
  #   next
  # end

  if (row[11].size == 8)
    ico = row[11]
  else
    ico = row[11].prepend("0" * (8 - row[11].size))
  end
  url = "http://wwwinfo.mfcr.cz/cgi-bin/ufis/iufismon/readDotaz.pl?obdobi=20101200&ico=" + ico
  doc = get_doc url
  population = to_integer doc.css('table tr:nth(3) td:nth(4)').text
  total_income = to_price doc.css('table tr:nth(4) td:nth(4)').text
  interests = to_price doc.css('table tr:nth(5) td:nth(4)').text
  paid_debts = to_price doc.css('table tr:nth(6) td:nth(4)').text
  total_debt_service = to_price doc.css('table tr:nth(7) td:nth(4)').text
  debt_service_pointer = to_percent doc.css('table tr:nth(8) td:nth(4)').text
  total_assets = to_price doc.css('table tr:nth(9) td:nth(4)').text
  liabilities = to_price doc.css('table tr:nth(10) td:nth(4)').text
  total_money_on_bank_accounts = to_price doc.css('table tr:nth(11) td:nth(4)').text
  loans_and_municipal_bonds = to_price doc.css('table tr:nth(12) td:nth(4)').text
  received_repayable_financial_aid_and_other_debts = to_price doc.css('table tr:nth(13) td:nth(4)').text
  total_debt = to_price doc.css('table tr:nth(14) td:nth(4)').text
  share_of_liabilities_to_total_assets = to_percent doc.css('table tr:nth(15) td:nth(4)').text
  share_of_debt_to_liabilities = to_percent doc.css('table tr:nth(16) td:nth(4)').text
  liabilities_per_inhabitant = to_price doc.css('table tr:nth(17) td:nth(4)').text
  current_assets = to_price doc.css('table tr:nth(18) td:nth(4)').text
  short_term_liabilities = to_price doc.css('table tr:nth(19) td:nth(4)').text
  total_liquidity = to_float doc.css('table tr:nth(20) td:nth(4)').text
  csvFile << [index+1,year,population,total_income,interests,paid_debts,total_debt_service, debt_service_pointer, total_assets, liabilities, total_money_on_bank_accounts, loans_and_municipal_bonds, received_repayable_financial_aid_and_other_debts, total_debt, share_of_liabilities_to_total_assets, share_of_debt_to_liabilities, liabilities_per_inhabitant, current_assets, short_term_liabilities, total_liquidity]
end

csvFile.close

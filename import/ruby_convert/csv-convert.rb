require 'csv'

class ImportData
   attr_accessor :csv, :table_name
   def initialize(csv, table_name)
     @csv = csv
     @table_name = table_name
   end
end

tables_data = [
    ImportData.new("kraje.csv", "kraj"),
    ImportData.new("okresy.csv", "okres"),
    ImportData.new("municipality.csv", "municipality"),
    ImportData.new("type_classification.csv", "type_classification"),
    ImportData.new("sector_classification.csv", "sector_classification")
]

tables_data.each do |data|
  reader = CSV.open(data.csv, "rb")
  header = reader.shift
  sql = "SET NAMES utf8;\n"
  sql << "INSERT INTO `#{data.table_name}` (" + header * "," + ") VALUES\n"
  reader.each do |row|
    sql << "(" + row.collect{ |x| "'#{x}'" } * "," + "),\n"
  end
  sql.chop!.chop! << ";"

  sqlFile = File.new("#{data.table_name}.sql", "w")
  sqlFile.puts sql
  sqlFile.close

  #con = Mysql.new('localhost', 'root', '', 'public_budget')
  #con.query(sql)
  #con.close
end



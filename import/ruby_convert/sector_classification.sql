SET NAMES utf8;
INSERT INTO `sector_classification` (id,category_code,category_name,section_code,section_name,subsection_code,subsection_name,paragraph_code,paragraph_name) VALUES
('1064','4','SOCIÁLNÍ VĚCI A POLITIKA ZAMĚSTNANOSTI','43','Sociální služby a společné činnosti v sociálním zabezpečení a politice zaměstnanosti.','435','Služby sociální péče','4350','Domovy pro seniory'),
('1065','6','VŠEOBECNÁ VEŘEJNÁ SPRÁVA A SLUŽBY','61','Státní moc, státní správa, územní samospráva a politické strany','611','Zastupitelské orgány','6118','Volba prezidenta republiky'),
('1066','6','VŠEOBECNÁ VEŘEJNÁ SPRÁVA A SLUŽBY','61','Státní moc, státní správa, územní samospráva a politické strany','611','Zastupitelské orgány','6119','Ostatní zastupitelské orgány a volby');
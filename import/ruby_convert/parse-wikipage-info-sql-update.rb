require 'csv'
require 'nokogiri'
require 'open-uri'

reader = CSV.open("Obce.csv", "rb")
header = reader.shift
sql = "SET NAMES utf8;\n"
reader.each_with_index do |row, index|
  doc = Nokogiri::HTML(open(row[0]))
  selector = doc.css('table:first a.external[href^="mailto"]')
  email = ''
  if selector[0]
      email = selector[0]['href'].sub(/mailto:/,"")
  end
  sql << "UPDATE `municipality` SET email='#{email}' WHERE id='#{index+1}';\n"
end

sqlFile = File.new("update_email_municipality.sql", "w")
sqlFile.puts sql
sqlFile.close

SET foreign_key_checks=0;
TRUNCATE TABLE `okres`;
TRUNCATE TABLE `kraj`;
TRUNCATE TABLE `municipality`;
TRUNCATE TABLE `sector_classification`;
TRUNCATE TABLE `type_classification`;
TRUNCATE TABLE `budget_item_type`;
SET foreign_key_checks=1;